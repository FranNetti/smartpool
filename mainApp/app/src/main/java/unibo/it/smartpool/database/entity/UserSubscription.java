package unibo.it.smartpool.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@Entity
public class UserSubscription extends UpdatableTable implements Serializable{

    public enum SubscriptionType {
        SINGLE_ENTRANCE(1),
        MONTHLY(2);

        private final int type;

        SubscriptionType(final int type){
            this.type = type;
        }

        public int getType() {
            return type;
        }
    }

    private static final String ID_FIELD = "id";
    private static final String NAME_FIELD = "corso";
    private static final String TYPE_FIELD = "tipoAbbonamento";
    private static final String DATE_START_FIELD = "dataInizio";
    private static final String DATE_END_FIELD = "scadenza";
    private static final String REM_ENTR_FIELD = "numIngressi";

    private static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat(DATE_PATTERN);

    @PrimaryKey
    private int id;

    @ColumnInfo
    @NonNull
    private String user;
    @ColumnInfo
    private int type;
    @ColumnInfo(name = "num_entr")
    @Nullable
    private int numRemainingEntrances;
    @ColumnInfo(name = "expiration_date")
    private String expirationDate;
    @ColumnInfo
    private String startDate;
    @ColumnInfo
    private String reference;
    @ColumnInfo
    private String lastUpdate;

    @Ignore
    private SubscriptionType subscriptionType;
    @Ignore
    private Date expiration;
    @Ignore
    private Date start;

    public UserSubscription(int id, @NonNull String user, int type, int numRemainingEntrances, String expirationDate, String startDate, String reference) {
        this.id = id;
        this.user = user;
        this.type = type;
        this.reference = reference;
        this.lastUpdate = FORMATTER.format(Calendar.getInstance().getTime());
        for(SubscriptionType subscriptionType : SubscriptionType.values()){
            if(subscriptionType.getType() == type){
                this.subscriptionType = subscriptionType;
            }
        }

        switch (subscriptionType){
            case SINGLE_ENTRANCE:
                this.numRemainingEntrances = numRemainingEntrances;
                Calendar expiration = Calendar.getInstance();
                expiration.set(Calendar.YEAR, expiration.getMaximum(Calendar.YEAR));
                expiration.set(Calendar.DAY_OF_YEAR, expiration.getMaximum(Calendar.DAY_OF_YEAR));
                this.expiration = expiration.getTime();
                this.expirationDate = FORMATTER.format(expiration.getTime());
                break;
            case MONTHLY:
                this.expirationDate = expirationDate;
                this.startDate = startDate;
                try {
                    this.expiration = FORMATTER.parse(expirationDate);
                    this.start = FORMATTER.parse(startDate);
                } catch (ParseException e){
                    this.expiration = Calendar.getInstance().getTime();
                    this.start = Calendar.getInstance().getTime();
                }
                break;
            default:
                break;
        }
    }

    public UserSubscription(final JSONObject object, String userEmail) throws JSONException {
        this.user = userEmail;
        this.lastUpdate = FORMATTER.format(Calendar.getInstance().getTime());

        this.id = object.getInt(ID_FIELD);
        this.reference = object.getString(NAME_FIELD);
        this.type = object.getInt(TYPE_FIELD);
        for(SubscriptionType subscriptionType : SubscriptionType.values()){
            if(subscriptionType.getType() == type){
                this.subscriptionType = subscriptionType;
            }
        }

        switch (subscriptionType){
            case SINGLE_ENTRANCE:
                this.numRemainingEntrances = object.getInt(REM_ENTR_FIELD);
                Calendar expiration = Calendar.getInstance();
                expiration.set(Calendar.YEAR, expiration.getMaximum(Calendar.YEAR));
                expiration.set(Calendar.DAY_OF_YEAR, expiration.getMaximum(Calendar.DAY_OF_YEAR));
                this.expiration = expiration.getTime();
                this.expirationDate = FORMATTER.format(expiration.getTime());
                break;
            case MONTHLY:
                this.expirationDate = object.getString(DATE_END_FIELD);
                this.startDate = object.getString(DATE_START_FIELD);
                try {
                    this.expiration = FORMATTER.parse(expirationDate);
                    this.start = FORMATTER.parse(startDate);
                } catch (ParseException e){
                    this.expiration = Calendar.getInstance().getTime();
                    this.start = Calendar.getInstance().getTime();
                }
                break;
            default:
                break;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getUser() {
        return user;
    }

    public void setUser(@NonNull String user) {
        this.user = user;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
        for(SubscriptionType subscriptionType : SubscriptionType.values()){
            if(subscriptionType.getType() == type){
                this.subscriptionType = subscriptionType;
            }
        }
    }

    @Nullable
    public int getNumRemainingEntrances() {
        return numRemainingEntrances;
    }

    public void setNumRemainingEntrances(@Nullable int numRemainingEntrances) {
        this.numRemainingEntrances = numRemainingEntrances;
    }

    @Nullable
    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(@Nullable String expirationDate) {
        this.expirationDate = expirationDate;
        try {
            this.expiration = FORMATTER.parse(expirationDate);
        } catch (ParseException e){
            this.expiration = Calendar.getInstance().getTime();
        }
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
        try {
            this.start = FORMATTER.parse(startDate);
        } catch (ParseException e){
            this.start = Calendar.getInstance().getTime();
        }
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
        this.type = subscriptionType.getType();
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
        this.expirationDate = FORMATTER.format(expiration);
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
        this.startDate = FORMATTER.format(start);
    }

    public boolean hasToUpdate(){
        return super.hasToUpdate(FORMATTER, lastUpdate);
    }

    public boolean isExpired(){
        switch (subscriptionType){
            case SINGLE_ENTRANCE:
                return numRemainingEntrances <= 0;
            case MONTHLY:
                Calendar today = Calendar.getInstance();
                Calendar exp = Calendar.getInstance();
                exp.setTime(expiration);
                return exp.before(today);
            default:
                return false;
        }
    }

    public boolean isStarted(){
        switch (subscriptionType){
            case MONTHLY:
                Calendar today = Calendar.getInstance();
                Calendar exp = Calendar.getInstance();
                exp.setTime(start);
                return exp.before(today);
            default:
                return false;
        }
    }

    public boolean updateUserSubscription(final UserSubscription newSub){
        boolean update = false;
        switch (newSub.subscriptionType){
            case SINGLE_ENTRANCE:
                int remainingEntrances = newSub.numRemainingEntrances;
                if(this.numRemainingEntrances != remainingEntrances){
                    this.numRemainingEntrances = remainingEntrances;
                    update = true;
                }
                break;
            case MONTHLY:
                if(!newSub.expiration.equals(this.expiration)){
                    this.setExpiration(newSub.expiration);
                    update = true;
                }
                break;
            default:
                break;
        }
        this.lastUpdate = FORMATTER.format(Calendar.getInstance().getTime());
        return update;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserSubscription that = (UserSubscription) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
