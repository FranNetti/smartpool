package unibo.it.smartpool.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;

@Entity
public class Instructor extends UpdatableTable implements Serializable {

    private static final String CF_FIELD = "cf";
    private static final String NAME_FIELD = "nome";
    private static final String SURNAME_FIELD = "cognome";
    private static final String ADDR_FIELD = "indirizzo";
    private static final String NUM_FIELD = "numTelefono";
    private static final String SEX_FIELD = "sesso";
    private static final String COURSES_FIELD = "corsi";
    private static final String COURSES_NAME = "nomeCorso";

    private static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat(DATE_PATTERN);

    @PrimaryKey
    @NonNull
    private String cf;
    @ColumnInfo
    private String name;
    @ColumnInfo
    private String surname;
    @ColumnInfo
    private String address;
    @ColumnInfo
    private String number;
    @ColumnInfo
    private String sex;
    @ColumnInfo
    private String courses;
    @ColumnInfo
    private String lastUpdate;

    /**
     * Constructor.
     * @param cf instructor's cf
     * @param name instructor's name
     * @param surname instructor's surname
     * @param number instructor's telephone number
     * @param courses instructor's courses
     */
    public Instructor(final String cf, final String name, final String surname, final String number, final String courses, final String lastUpdate) {
        this.cf = cf;
        this.name = name;
        this.surname = surname;
        this.number = number;
        this.courses = courses;
        this.lastUpdate = lastUpdate;
    }

    /**
     * Constructor.
     * @param obj the instructor's data in a json format
     * @throws JSONException if there is an error while getting data
     */
    public Instructor(final JSONObject obj) throws JSONException {
        this.cf = obj.getString(CF_FIELD);
        this.name = obj.getString(NAME_FIELD);
        this.surname = obj.getString(SURNAME_FIELD);
        this.address = obj.getString(ADDR_FIELD);
        this.number = obj.getString(NUM_FIELD);
        this.sex = obj.getString(SEX_FIELD);
        JSONArray courses = obj.getJSONArray(COURSES_FIELD);
        StringBuilder stringBuilder = new StringBuilder();
        for (int x = 0; x < courses.length(); x++){
            JSONObject courseObj = courses.getJSONObject(x);
            stringBuilder.append("- ");
            stringBuilder.append(courseObj.getString(COURSES_NAME));
            stringBuilder.append("\n");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        this.courses = stringBuilder.toString();
        this.lastUpdate = FORMATTER.format(Calendar.getInstance().getTime());
    }

    public String getCf() {
        return cf;
    }

    public void setCf(String cf) {
        this.cf = cf;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFullName(){
        return this.name + " " + this.surname;
    }

    public String getCourses() {
        return courses;
    }

    public void setCourses(String courses) {
        this.courses = courses;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isMale(){
        return sex.equals("M");
    }

    public boolean hasToUpdate(){
        return super.hasToUpdate(FORMATTER, this.lastUpdate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Instructor instructor = (Instructor) o;
        return Objects.equals(name, instructor.name) &&
                Objects.equals(surname, instructor.surname) &&
                Objects.equals(address, instructor.address) &&
                Objects.equals(number, instructor.number) &&
                Objects.equals(sex, instructor.sex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, address, number, sex);
    }
}
