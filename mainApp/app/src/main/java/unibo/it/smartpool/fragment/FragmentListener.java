package unibo.it.smartpool.fragment;

/**
 * Interface that regroups all the listeners of this package
 */
public interface FragmentListener extends
        EmptyFragment.EmptyFragmentListener,
        ProfileFragment.ProfileFragmentListener,
        ChangeInfoFragment.ChangeInfoFragmentListener,
        SubscriptionFragment.SubscriptionFragmentListener{
}
