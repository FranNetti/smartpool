package unibo.it.smartpool.fragment;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import unibo.it.smartpool.InformationManager;
import unibo.it.smartpool.R;
import unibo.it.smartpool.activity.MainActivity;
import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.connection.HttpException;
import unibo.it.smartpool.database.entity.AquaFitness;
import unibo.it.smartpool.database.entity.Subscription;
import unibo.it.smartpool.utils.ParallelExecutor;
import unibo.it.smartpool.view_elements.adapter.CourseAdapter;

public class AquaFitnessFragment  extends Fragment implements CourseAdapter.CurseAdapterListener{

    private InformationManager manager;
    private String userEmail;
    private static final String TAG = "Acq_fit_tag";
    private Context context;
    private CourseAdapter courseAdapter;

    public static AquaFitnessFragment newInstance() {
        return new AquaFitnessFragment();
    }

    public AquaFitnessFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.manager = ViewModelProviders.of(getActivity()).get(InformationManager.class);
        new ParallelExecutor().execute(new Runnable() {
            @Override
            public void run() {
                userEmail = manager.getUser().getEmail();
            }
        });
        this.context = getContext();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_aquafitness, container, false);
        final ListView layout = (ListView) view.findViewById(R.id.listView);
        final LiveData<List<AquaFitness>> courseList = this.manager.getAquaFitnessList();
        final List<AquaFitness> list = new ArrayList<>();
        this.courseAdapter = new CourseAdapter(
                getActivity(),
                android.R.layout.simple_list_item_1,
                this,
                list
        );
        layout.setAdapter(courseAdapter);

        final MainActivity main = (MainActivity)getActivity();

        courseList.observe(this, new Observer<List<AquaFitness>>() {
            @Override
            public void onChanged(@Nullable List<AquaFitness> course) {
            if (course != null) {
                if(course.isEmpty()){
                    main.noDataAvailable();
                } else {
                    list.clear();
                    list.addAll(course);
                    courseAdapter.notifyDataSetChanged();
                }
            }
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!ConnectionUtilities.isConnectionAvailable(getContext())){
            Toast.makeText(getContext(), getString(R.string.warning_text), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSubscriptionSelected(AquaFitness item,Subscription sub) {
        new AddSubscription(item, sub).execute();
    }

    private class AddSubscription extends AsyncTask<Void, Void, Integer> {
        private AquaFitness item;
        private Subscription sub;

        public AddSubscription(AquaFitness item,Subscription sub){
            this.item = item;
            this.sub = sub;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            final Map<String, String> map = ConnectionUtilities.getAcqFitAddSubscriptionMap(
                    String.valueOf(this.item.getId()),
                    String.valueOf(this.item.getIdActCourse()),
                    userEmail,
                    String.valueOf(this.sub.getId()),
                    this.sub.getLabel(),
                    this.sub.getType(),
                    this.sub.getPrice()
            );

            final String url = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.ADD_AQF_RECORD;
           try {
               if (ConnectionUtilities.isConnectionAvailable(context)) {
                   ConnectionUtilities.getDataFromUrl(url, map, true);
                   manager.updateSubscriptions();
                   manager.updateCard();
                   return HttpURLConnection.HTTP_OK;
               } else {
                   return HttpURLConnection.HTTP_GONE;
               }
           }catch (HttpException e){
               Log.e(TAG, "Error in the account balance, HttpCode: " + e.getCode(), e);
               return e.getCode();
           }
        }

        @Override
        protected void onPostExecute(final Integer success) {
            switch (success){
                case HttpURLConnection.HTTP_OK:
                    Toast.makeText(context,R.string.sub_add,Toast.LENGTH_LONG).show();
                    break;
                case HttpURLConnection.HTTP_GONE:
                    ConnectionUtilities.alertConnectionAbsence(context);
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    new AlertDialog.Builder(context)
                            .setMessage(R.string.balance)
                            .setPositiveButton("OK", null)
                            .show();
                    break;
                default:
                    ConnectionUtilities.alertConnectionError(context);
                    break;
            }
        }

    }
}

