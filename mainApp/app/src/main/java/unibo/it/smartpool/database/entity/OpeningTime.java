package unibo.it.smartpool.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;

@Entity(tableName = "opening")
public class OpeningTime extends UpdatableTable{

    private static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    @PrimaryKey
    @NonNull
    private String day;

    @ColumnInfo
    private String hours;

    @ColumnInfo
    private String lastUpdate;

    public OpeningTime(final String day, final String hours, final String lastUpdate){
        this.day = day;
        this.hours = hours;
        this.lastUpdate = lastUpdate;
    }

    @Ignore
    public OpeningTime(final String day, final String hours){
        this.day = day;
        this.hours = hours;
        this.lastUpdate = DATE_FORMATTER.format(Calendar.getInstance().getTime());
    }

    @NonNull
    public String getDay() {
        return day;
    }

    public void setDay(@NonNull String day) {
        this.day = day;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean hasToUpdate(){
        return super.hasToUpdate(DATE_FORMATTER, lastUpdate);
    }
}
