package unibo.it.smartpool.utils;

import unibo.it.smartpool.R;

/**
 * Enum for the menu state.
 */
public enum MenuState {
    FITNESS(R.id.nav_fit),
    COURSE(R.id.nav_course),
    FREE(R.id.nav_free),
    INSTRUCTOR(R.id.nav_instructor),
    INFO(R.id.nav_time),
    ENTRANCE(R.id.nav_entrance),
    CARD(R.id.nav_card),
    FARE(R.id.nav_abb),
    PROFILE(R.id.nav_profile),
    FAVOURITE(R.id.nav_favourite);

    private final int itemId;

    MenuState(int id) {
        this.itemId = id;
    }

    public int getItemId() {
        return itemId;
    }
}
