package unibo.it.smartpool.view_elements;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import unibo.it.smartpool.R;

public class LikeButton extends AppCompatImageButton {

    private boolean isChecked;

    public LikeButton(final Context context){
        super(context);
        this.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        this.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        this.isChecked = false;
    }

    public void toggleCheck() {
        if(!this.isChecked){
            this.setImageResource(R.drawable.ic_favorite_black_24dp);
            this.isChecked = true;
        } else{
            this.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            this.isChecked = false;
        }
    }

    public boolean isChecked() {
        return isChecked;
    }
}
