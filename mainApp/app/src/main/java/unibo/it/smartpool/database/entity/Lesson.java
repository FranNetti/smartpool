package unibo.it.smartpool.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Objects;

import unibo.it.smartpool.R;

@Entity(primaryKeys = {"day", "time", "pool"})
public class Lesson implements Serializable {

    private static final String DAY_FIELD = "giorno";
    private static final String TIME_FIELD = "ora";
    private static final String POOL_FIELD = "vasca";

    @ColumnInfo
    private int day;
    @ColumnInfo
    @NonNull
    private String time;
    @ColumnInfo
    @NonNull
    private String pool;

    public Lesson(final int day, @NonNull final String time, @NonNull final String pool) {
        this.day = day;
        this.time = time;
        this.pool = pool;
    }

    public Lesson(final JSONObject obj) throws JSONException {
        this.day = obj.getInt(DAY_FIELD);
        this.time = obj.getString(TIME_FIELD).substring(0,5);
        this.pool = obj.getString(POOL_FIELD);
    }

    public Lesson(final JSONObject obj, final String pool) throws JSONException {
        this.day = obj.getInt(DAY_FIELD);
        this.time = obj.getString(TIME_FIELD).substring(0,5);
        this.pool = pool;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @NonNull
    public String getTime() {
        return time;
    }

    public void setTime(@NonNull String time) {
        this.time = time;
    }

    @NonNull
    public String getPool() {
        return pool;
    }

    public void setPool(@NonNull String pool) {
        this.pool = pool;
    }

    public boolean update(final Lesson lesson){
        boolean update = false;
        if(this.day != lesson.day || !this.pool.equals(lesson.pool) || !this.time.equals(lesson.time)){
            update = true;
            this.day = lesson.day;
            this.pool = lesson.pool;
            this.time = lesson.time;
        }
        return update;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lesson lesson = (Lesson) o;
        return Objects.equals(day, lesson.day) &&
                Objects.equals(time, lesson.time) &&
                Objects.equals(pool, lesson.pool);
    }

    @Override
    public int hashCode() {

        return Objects.hash(day, time, pool);
    }

    public int getIntDay(final int day){
        int dayId;
        switch (day){
            case 1: dayId = R.string.d1; break;
            case 2: dayId = R.string.d2; break;
            case 3: dayId = R.string.d3; break;
            case 4: dayId = R.string.d4; break;
            case 5: dayId = R.string.d5; break;
            case 6: dayId = R.string.d6; break;
            case 7: dayId = R.string.d7; break;
            default: dayId = 0;
        }
        return dayId;
    }
}
