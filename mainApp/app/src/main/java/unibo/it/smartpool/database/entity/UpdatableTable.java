package unibo.it.smartpool.database.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class UpdatableTable {

    public boolean hasToUpdate(SimpleDateFormat formatter, String lastUp){
        Calendar today = Calendar.getInstance();
        Calendar lastUpdate = Calendar.getInstance();
        try{
            lastUpdate.setTime(formatter.parse(lastUp));
        } catch (ParseException e){
        }
        return !(lastUpdate.get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH) &&
                lastUpdate.get(Calendar.MONTH) == today.get(Calendar.MONTH) &&
                lastUpdate.get(Calendar.YEAR) == today.get(Calendar.YEAR));
    }
}
