package unibo.it.smartpool.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import unibo.it.smartpool.R;
import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.connection.HttpException;
import unibo.it.smartpool.database.AppDatabase;
import unibo.it.smartpool.database.entity.User;
import unibo.it.smartpool.database.dao.UserDAO;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * Class that manages an activity where it's possible to login
 **/
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to pass user data to the activity that started this one.
     */
    public static final String USER_DATA = "user_data";
    /**
     * Id to the activity that started this one that the user is a new user.
     */
    public static final String USER_CREATE = "user_create";

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * Id to save in shared preferences the last user email
     */
    private static final String USER_NAME = "user_name";

    /**
     * Tag to identify possible errors happened in this activity
     */
    private static final String TAG = "LoginActivity";

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private TextView warningTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        this.mPasswordView = (EditText) findViewById(R.id.password);
        this.warningTv = findViewById(R.id.warning_text);
        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);

        /* email field setup */
        // adding a listener to the x button in order to delete text when pressed
        mEmailView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (mEmailView.getRight() - mEmailView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        mEmailView.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        // getting from shared preferences the last user connected
        SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
        String text = pref.getString(USER_NAME, "");
        if (!text.isEmpty()) {
            mEmailView.setText(text);
            mEmailView.setSelection(text.length());
        }
        populateAutoComplete();

        //handling the enter button
        mEmailView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    mPasswordView.requestFocus();
                }
                return false;
            }
        });

        /* password field setup */
        //handling the enter button
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        /* btn login setup */
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean isOnline = ConnectionUtilities.isConnectionAvailable(this);
        this.warningTv.setVisibility(isOnline ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(LoginActivity.this)
                .setTitle(R.string.exit_alert_title)
                .setMessage(R.string.exit_alert_msg)
                .setPositiveButton(R.string.exit_affermative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_CANCELED, returnIntent);
                        finish();
                    }
                })
                .setNegativeButton(R.string.exit_negative, null)
                .show();
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }
        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute();
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    private class UserLoginTask extends AsyncTask<Void, Void, Integer> {

        private static final String USER_FIELD = "utente";

        private final String mEmail;
        private final String mPassword;
        private User user;
        private boolean addUser = false;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Integer doInBackground(Void... params) {

            UserDAO userTable = AppDatabase.getDatabase(LoginActivity.this).userDao();
            User userDb = userTable.getUser(mEmail);
            final boolean isOnline = ConnectionUtilities.isConnectionAvailable(LoginActivity.this);
            if(userDb == null || (userDb.hasToUpdate() && isOnline)){
                if(isOnline) {
                    final Map<String, String> map = ConnectionUtilities.getLoginMap(mEmail, mPassword);
                    final String url = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.LOGIN_PAGE;
                    try {
                        final String response = ConnectionUtilities.getDataFromUrl(url, map, true);
                        JSONObject userJS = new JSONObject(response).getJSONObject(USER_FIELD);
                        if (userJS != null) {
                            userJS.put(User.EMAIL_FIELD, mEmail);
                            userJS.put(User.PWD_FIELD, mPassword);
                            this.user = new User(userJS);
                            this.addUser = userDb == null;
                            return HttpURLConnection.HTTP_OK;
                        }
                    } catch (HttpException e) {
                        return e.getCode();
                    } catch (JSONException e) {
                        Log.e(TAG, e.toString());
                    }
                } else {
                    return HttpURLConnection.HTTP_GONE;
                }
            } else {
                this.user = userDb;
                if(this.user.getPwd().equals(mPassword)){
                    return HttpURLConnection.HTTP_OK;
                } else {
                    return HttpURLConnection.HTTP_BAD_REQUEST;
                }
            }

            return -2;
        }

        @Override
        protected void onPostExecute(final Integer result) {
            mAuthTask = null;

            switch (result) {
                // the password is incorrect
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    mPasswordView.setError(getString(R.string.error_incorrect_password));
                    mPasswordView.requestFocus();
                    break;
                // everything is good, login successful
                case HttpURLConnection.HTTP_OK:
                    SharedPreferences pref = LoginActivity.this.getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString(USER_NAME, this.mEmail);
                    editor.apply();
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(USER_DATA, this.user);
                    returnIntent.putExtra(USER_CREATE, this.addUser);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                    break;
                // the user doesn't exist
                case HttpURLConnection.HTTP_NOT_FOUND:
                    mEmailView.setError(getString(R.string.error_incorrect_email));
                    mEmailView.requestFocus();
                    break;
                case HttpURLConnection.HTTP_GONE:
                    ConnectionUtilities.alertConnectionAbsence(LoginActivity.this);
                    break;
                default:
                    ConnectionUtilities.alertConnectionError(LoginActivity.this);
                    break;

            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }

    }
}

