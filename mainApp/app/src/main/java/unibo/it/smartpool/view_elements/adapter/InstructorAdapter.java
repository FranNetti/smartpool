package unibo.it.smartpool.view_elements.adapter;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import unibo.it.smartpool.R;
import unibo.it.smartpool.database.AppDatabase;
import unibo.it.smartpool.database.dao.FavouriteDAO;
import unibo.it.smartpool.database.entity.Instructor;
import unibo.it.smartpool.utils.ParallelExecutor;
import unibo.it.smartpool.view_elements.LikeButton;

public class InstructorAdapter extends ArrayAdapter<Instructor> {

    public interface InstructorAdapterListener {
        void onAddressPressed(final String address);
        void onInstructorSelected(final Instructor instr);
        void onInstructorDeselected(final Instructor instr);
    }

    private InstructorAdapterListener listener;
    private String userEmail;
    private FavouriteDAO favouriteTable;

    public InstructorAdapter(Context context, int textViewResourceId, List<Instructor> objects, String userEmail, InstructorAdapterListener listener) {
        super(context, textViewResourceId, objects);
        this.listener = listener;
        this.userEmail = userEmail;
        this.favouriteTable = AppDatabase.getDatabase(context).favouriteDAO();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.instructor_item, null);

        final Instructor instr = getItem(position);

        final TextView instrName = view.findViewById(R.id.instr_name);
        final TextView instrSurname = view.findViewById(R.id.instr_surname);
        final TextView instrTel = view.findViewById(R.id.instr_tel);
        final TextView instrAddr = view.findViewById(R.id.instr_addr);
        final TextView instrCourses = view.findViewById(R.id.instr_courses);
        final ImageView profileImage = view.findViewById(R.id.instr_profile_pic);
        final LinearLayout likeLayout = view.findViewById(R.id.instr_like);
        final LikeButton like = new LikeButton(getContext());

        likeLayout.addView(like);

        instrName.setText(instr.getName());
        instrSurname.setText(instr.getSurname());
        instrTel.setText(instr.getNumber());
        instrCourses.setText(instr.getCourses());
        if(instr.isMale()) {
            profileImage.setImageResource(R.drawable.user_male);
        } else {
            profileImage.setImageResource(R.drawable.user_female);
        }

        /* to make addr underlined */
        SpannableString content = new SpannableString(instr.getAddress());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        instrAddr.setText(content);
        instrAddr.setTextColor(getContext().getResources().getColor(R.color.colorAccent));

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LikeButton btn = (LikeButton)v;
                btn.toggleCheck();
                if(btn.isChecked()){
                    listener.onInstructorSelected(instr);
                } else {
                    listener.onInstructorDeselected(instr);
                }
            }
        });
        instrAddr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onAddressPressed(instrAddr.getText().toString());
            }
        });

        ParallelExecutor executor = new ParallelExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                if(favouriteTable.isAFavourite(userEmail, instr.getCf()) > 0){
                    if(!like.isChecked()){
                        like.toggleCheck();
                    }
                }
            }
        });
        return view;
    }

}
