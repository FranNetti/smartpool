package unibo.it.smartpool.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import unibo.it.smartpool.R;
import unibo.it.smartpool.database.entity.AlarmNotification;
import unibo.it.smartpool.view_elements.AlarmAlertDialog;
import unibo.it.smartpool.view_elements.AlarmRadioButton;

/**
 * Class that manages an activity where it's possible to select when a user wants to be notified
 **/
public class AlarmChooserActivity extends AppCompatActivity implements AlarmAlertDialog.AlarmAlertDialogListener {

    /**
     * Code to identify a result from AlarmChooserActivity
     */
    public static final int ALARM_ACTIVITY_INTENT_CODE = 2;

    /**
     * String to pass a param that specifies the alarm type
     */
    public static final String ALARM_TYPE = "alarm_type";
    /**
     * String to get in intent extras the new alarm
     */
    public static final String ALARM_NOT = "alarm_not";
    /**
     * String to get in intent extras if it was chosen to delete the alarm
     */
    public static final String ALARM_DEL = "alarm_del";
    /**
     * String to use to inform the activity that you want to update an element
     */
    public static final String UPDATE_ACTIVITY = "update_act";

    private static final String DIALOG_TAG = "create_new_alert";

    private static final AlarmNotification[] ALARM_LIST = new AlarmNotification[]{
            new AlarmNotification(0, AlarmNotification.TimePeriod.DAY),
            new AlarmNotification(1, AlarmNotification.TimePeriod.DAY),
            new AlarmNotification(2, AlarmNotification.TimePeriod.DAY),
            new AlarmNotification(1, AlarmNotification.TimePeriod.WEEK),
            new AlarmNotification(1, AlarmNotification.TimePeriod.MONTH)
    };

    private final List<AlarmNotification> list = new ArrayList<>(Arrays.asList(ALARM_LIST));
    private RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_chooser);

        this.radioGroup = findViewById(R.id.alarm_radio_group);
        final Button btnSelect = findViewById(R.id.alarm_btn_select);
        final Button btnDelete = findViewById(R.id.alarm_btn_delete);

        /* radio group setup */
        for(AlarmNotification alarm : list){
            AlarmRadioButton button = new AlarmRadioButton(this, alarm);
            radioGroup.addView(button);
            radioGroup.addView(getHorizontalDivider());
        }
        AlarmRadioButton btnPersonalized = new AlarmRadioButton(this, getString(R.string.personalized));
        radioGroup.addView(btnPersonalized);
        radioGroup.addView(getHorizontalDivider());
        radioGroup.check(AlarmRadioButton.CURRENT_DAY_ELEMENT_ID);

        /* btn select setup */
        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlarmRadioButton radio = findViewById(radioGroup.getCheckedRadioButtonId());
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                returnIntent.putExtra(ALARM_NOT, radio.getAlarm());
                finish();
            }
        });

        /* btn delete setup - it is visible only if it's required by the previous activity */
        boolean show = getIntent().getBooleanExtra(UPDATE_ACTIVITY,false);
        btnDelete.setVisibility(show ? View.VISIBLE : View.GONE);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                returnIntent.putExtra(ALARM_DEL, true);
                finish();
            }
        });

        /* radio button for personalized alarm setup */
        btnPersonalized.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlarmAlertDialog alarm = new AlarmAlertDialog();
                alarm.addListener(AlarmChooserActivity.this);
                alarm.show(getFragmentManager(), DIALOG_TAG);
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public void onPositiveButtonPressed(AlarmNotification alarm) {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        returnIntent.putExtra(ALARM_NOT, alarm);
        finish();
    }

    private View getHorizontalDivider(){
        View v = new View(this);
        v.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                3
        ));
        v.setBackgroundColor(Color.LTGRAY);
        return v;
    }
}
