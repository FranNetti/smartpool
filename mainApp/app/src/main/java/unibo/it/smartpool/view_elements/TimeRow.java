package unibo.it.smartpool.view_elements;

import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class TimeRow extends TableRow {

    public TimeRow(Context context, String day, String time, String pool) {
        super(context);
        TextView dayView = new TextView(context);
        TextView timeView = new TextView(context);
        TextView poolView = new TextView(context);

        dayView.setLayoutParams(new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT,
                1f
        ));

        timeView.setLayoutParams(new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT,
                1f
        ));


        poolView.setLayoutParams(new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT,
                1f
        ));

        dayView.setText(day);
        dayView.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
        dayView.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);

        timeView.setText(time);
        timeView.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
        timeView.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
        poolView.setText(pool);
        poolView.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
        poolView.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
        this.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        this.setWeightSum(3);
        this.addView(dayView);
        this.addView(timeView);
        this.addView(poolView);
        TableLayout.LayoutParams lp = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(0,10,40,10);
        this.setLayoutParams(lp);

    }



}
