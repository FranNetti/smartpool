package unibo.it.smartpool.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

@Entity
public class SwimCourse extends UpdatableTable implements Serializable {

    private static final String ID_FIELD = "id";
    private static final String NAME_FIELD = "nome";
    private static final String POOL_FIELD = "vasca";
    private static final String ID_ATT_FIELD = "idCorsoAtt";
    private static final String LABEL_POOL_FIELD = "labelVasca";
    private static final String LABEL_LEVEL_FIELD = "labelLivello";
    private static final String INSTR_FIELD = "istruttori";
    private static final String INSTR_NAME = "nome";
    private static final String INSTR_SURNAME = "cognome";

    private static final String INSTR_DIV = ",";
    private static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat(DATE_PATTERN);


    @PrimaryKey
    private int id;
    @ColumnInfo
    private String name;
    @ColumnInfo
    private String pool;
    @ColumnInfo
    private String labelPool;
    @ColumnInfo
    private String labelLevel;
    @ColumnInfo
    private int idActCourse;
    @ColumnInfo
    private String instructorList;
    @ColumnInfo
    private String lastUpdate;

    @Ignore
    private List<Subscription> subscriptions;
    @Ignore
    private List<Lesson> lessons;


    public SwimCourse(final int id, final String name, final String pool, final String labelPool, final String labelLevel, final int idActCourse, final String instructorList, final String lastUpdate) {
        this.id = id;
        this.name = name;
        this.pool = pool;
        this.labelLevel = labelLevel;
        this.labelPool = labelPool;
        this.idActCourse = idActCourse;
        this.instructorList = instructorList;
        this.lastUpdate = lastUpdate;
        this.subscriptions = new ArrayList<>();
        this.lessons = new ArrayList<>();
    }

    public SwimCourse(final JSONObject obj) throws JSONException {
        this.id = obj.getInt(ID_FIELD);
        this.name = obj.getString(NAME_FIELD);
        this.pool = obj.getString(POOL_FIELD);
        this.labelLevel = obj.getString(LABEL_LEVEL_FIELD);
        this.labelPool = obj.getString(LABEL_POOL_FIELD);
        this.idActCourse = obj.getInt(ID_ATT_FIELD);
        StringBuilder builder = new StringBuilder();
        JSONArray instructor = obj.getJSONArray(INSTR_FIELD);
        for (int x = 0; x < instructor.length(); x++){
            JSONObject inst = instructor.getJSONObject(x);
            builder.append(inst.getString(INSTR_NAME));
            builder.append(" ");
            builder.append(inst.getString(INSTR_SURNAME));
            builder.append(INSTR_DIV);
        }
        builder.deleteCharAt(builder.length() - 1);
        this.instructorList = builder.toString();
        this.lastUpdate = FORMATTER.format(Calendar.getInstance().getTime());
        this.subscriptions = new ArrayList<>();
        this.lessons = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPool() {
        return pool;
    }

    public void setPool(String pool) {
        this.pool = pool;
    }

    public String getLabelPool() {
        return labelPool;
    }

    public void setLabelPool(String labelPool) {
        this.labelPool = labelPool;
    }

    public String getLabelLevel() {
        return labelLevel;
    }

    public void setLabelLevel(String labelLevel) {
        this.labelLevel = labelLevel;
    }

    public String getInstructorList() {
        return instructorList;
    }

    public void setInstructorList(String instructorList) {
        this.instructorList = instructorList;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public boolean hasToUpdate(){
        return super.hasToUpdate(FORMATTER, lastUpdate);
    }

    public int getIdActCourse() {
        return idActCourse;
    }

    public void setIdActCourse(int idActCourse) {
        this.idActCourse = idActCourse;
    }

    public boolean update(final SwimCourse swim){
        boolean update = false;
        this.lastUpdate = FORMATTER.format(Calendar.getInstance().getTime());
        if(!this.instructorList.equals(swim.instructorList) || !this.name.equals(swim.name) || !this.pool.equals(swim.pool)
                || !this.labelPool.equals(swim.labelPool) || !this.labelLevel.equals(swim.labelLevel)|| this.idActCourse !=swim.idActCourse){
            this.name = swim.name;
            this.instructorList = swim.instructorList;
            this.labelLevel = swim.labelLevel;
            this.labelPool = swim.labelPool;
            this.pool = swim.pool;
            this.idActCourse = swim.idActCourse;
            update = true;
        }
        return update;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SwimCourse that = (SwimCourse) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
