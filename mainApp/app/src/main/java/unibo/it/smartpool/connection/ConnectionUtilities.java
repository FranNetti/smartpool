package unibo.it.smartpool.connection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import unibo.it.smartpool.R;

// Utility class that handles the connection management
public final class ConnectionUtilities {

    /* server url */
    private static final String IP_ADDRESS = "93.66.254.244:6789";
    public static final String SERVER_HTTP_URL = "http://" + IP_ADDRESS + "/web/mobile/";

    /* web pages */
    public static final String LOGIN_PAGE = "utente.php";
    public static final String AQUAFITNESS_PAGE = "abbAcquaFitness.php";
    public static final String SWIMCOURSE_PAGE = "corsoNuoto.php";
    public static final String FREE_PAGE = "freeSwim.php";
    public static final String INSTRUCTOR_PAGE = "istruttore.php";
    public static final String INFO_PAGE = "info.php";
    public static final String CARD_PAGE = "tessera.php";
    public static final String SUB_PAGE = "abbonamenti.php";
    public static final String UPDATE_PAGE = "cambiaDati.php";
    public static final String ADD_AQF_RECORD = "AddAqfRecord.php";
    public static final String ADD_SWIM_RECORD = "AddSwimRecord.php";
    public static final String ADD_FREE_RECORD = "AddFreeRecord.php";

    /* data requested by the login page */
    private static final String LOGIN_USER = "user";
    private static final String LOGIN_PWD = "pwd";

    /* data requested by the update user info page */
    private static final String UPDATE_NAME = "nome";
    private static final String UPDATE_SURNAME = "cognome";
    private static final String UPDATE_ADDR = "ind";
    private static final String UPDATE_PHONE_NUMBER = "tel";
    private static final String UPDATE_EMAIL = "email";
    private static final String UPDATE_PWD = "pwd";

    /* data requested by the update user info page */
    private static final String ADD_EMAIL = "email";
    private static final String ADD_ABBONAMENTO = "codAbbonamento";
    private static final String ADD_LABEL = "label";
    private static final String ADD_VALUE = "value";
    private static final String ADD_CORSO_ID = "corsoId";
    private static final String ADD_TIPO_ID = "tipoId";
    private static final String ADD_PRICE = "price";

    /* data requested by the card page */
    private static final String CARD_USER = "email";

    /* data requested by the subscriptions page */
    private static final String SUB_USER = "user";


    private ConnectionUtilities(){}

    /**
     * Creates a dialog that informs about an error
     * @param context
     */
    public static void alertConnectionError(final Context context) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.error_sending_data_title)
                .setMessage(R.string.error_sending_data)
                .setPositiveButton("ok", null)
                .show();
    }

    /**
     * Creates a dialog that informs about the connection absence
     * @param context
     */
    public static void alertConnectionAbsence(final Context context) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.connection_absence_title)
                .setMessage(R.string.connection_absence_data)
                .setPositiveButton("ok", null)
                .show();
    }

    /**
     * Retrieve data from an url
     * @param urlToConnect the url to connect
     * @param params post params to send
     * @param isPost if it's a post or a get
     * @return a string with the content of the response
     * @throws HttpException if a http error occurs
     */
    public static String getDataFromUrl(final String urlToConnect, final Map<String, String> params, final boolean isPost) throws HttpException {
        HttpURLConnection httpURLConnection = null;
        StringBuilder response = new StringBuilder();
        BufferedReader rd = null;

        try {
            URL url = new URL(urlToConnect);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(3 * 1000);
            httpURLConnection.setReadTimeout(20 * 1000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod(isPost ? "POST" : "GET");
            httpURLConnection.setDoInput(true);

            if(isPost) {
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setChunkedStreamingMode(0);
                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(params));
                writer.flush();
                writer.close();
                os.close();
            }

            int responseCode = httpURLConnection.getResponseCode();
            switch (responseCode) {
                case HttpURLConnection.HTTP_OK:
                    InputStream inputStream = httpURLConnection.getInputStream();
                    rd = new BufferedReader(new InputStreamReader(inputStream));
                    String line = "";
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                    }
                    break;
                default:
                    throw new HttpException(responseCode);
            }
        } catch (IOException e) {
            e.printStackTrace();
            if(e instanceof SocketTimeoutException){
                throw new HttpTimeoutException();
            }
        } finally {
            if(rd != null) {
                try {
                    rd.close();
                } catch (Exception e) {}
            }
            if(httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
        return response.toString();
    }

    public static boolean isConnectionAvailable(final Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnected() ||
           (networkInfo.getType() != ConnectivityManager.TYPE_WIFI && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)){
            return false;
        }
        return true;
    }

    /**
     * Get a string with the post parameters
     * @param params a map with the data to send
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String getPostDataString(final Map<String, String> params) throws UnsupportedEncodingException {
        StringBuilder res = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if(first) {
                first = false;
            } else {
                res.append("&");
            }
            res.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            res.append("=");
            res.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return res.toString();
    }

    /**
     * Creates a map with the post data to send to the login page
     * @param user the user email
     * @param password the user password
     * @return a map with the data
     */
    public static Map<String, String> getLoginMap(final String user, final String password) {
        HashMap<String, String> map = new HashMap<>();
        map.put(LOGIN_USER, user);
        map.put(LOGIN_PWD, password);
        return map;
    }

    /**
     * Creates a map with the post data to send to the user update page
     * @param name user's name
     * @param surname user's surname
     * @param add user's address
     * @param phone user's phone number
     * @param mail user's email
     * @param pwd user password
     * @return a map with the data
     */
    public static Map<String, String> getUserUpdateMap(final String name, final String surname, final String add, final String phone, final String mail, final String pwd) {
        HashMap<String, String> map = new HashMap<>();
        map.put(UPDATE_NAME, name);
        map.put(UPDATE_SURNAME, surname);
        map.put(UPDATE_EMAIL, mail);
        map.put(UPDATE_PHONE_NUMBER, phone);
        map.put(UPDATE_ADDR, add);
        map.put(UPDATE_PWD, pwd);
        return map;
    }

    /**
     *
     * @param codAbbonamento id subscription
     * @param email user email
     * @param label subscription label
     * @param value subscription value
     * @param corsoId actual course id
     * @param idCourseType course typology
     * @return map of new subsciption data
     */
    public static Map<String, String> getAcqFitAddSubscriptionMap(final String idCourseType, final String corsoId, final String email,
                                                                  final String codAbbonamento, final String label, final String value, final String price) {
        HashMap<String, String> map = new HashMap<>();
        map.put(ADD_ABBONAMENTO, codAbbonamento);
        map.put(ADD_CORSO_ID, corsoId);
        map.put(ADD_EMAIL, email);
        map.put(ADD_LABEL, label);
        map.put(ADD_TIPO_ID, idCourseType);
        map.put(ADD_VALUE, value);
        map.put(ADD_PRICE,price);
        return map;
    }

    /**
     *
     * @param codAbbonamento id subscription
     * @param email user email
     * @param value subscription value
     * @param corsoId actual course id
     * @param idCourseType course typology
     * @return map of new subsciption data
     */
    public static Map<String,String> getSwimCourseAddSubscriptionMap (final String idCourseType,final String corsoId, final String email,
                                                                      final String codAbbonamento,final String value, final String price){
        HashMap<String, String> map = new HashMap<>();
        map.put(ADD_ABBONAMENTO, codAbbonamento);
        map.put(ADD_CORSO_ID, corsoId);
        map.put(ADD_EMAIL, email);
        map.put(ADD_TIPO_ID, idCourseType);
        map.put(ADD_VALUE, value);
        map.put(ADD_PRICE,price);
        return map;
    }

    /**
     *
     * @param codAbbonamento id subscription
     * @param email user email
     * @param label label for the name
     * @param value subscription value
     * @return map of new subsciption data
     */
    public static Map<String, String> getFreeSwimAddSubscriptionMap( final String email,final String codAbbonamento, final String label,
                                                                      final String value, final String price){
        HashMap<String, String> map = new HashMap<>();
        map.put(ADD_ABBONAMENTO, codAbbonamento);
        map.put(ADD_EMAIL, email);
        map.put(ADD_LABEL, label);
        map.put(ADD_VALUE, value);
        map.put(ADD_PRICE,price);
        return map;
    }
    /**
     * Creates a map with the post data to send to the card page
     * @param user the user's email
     * @return a map with the data
     */
    public static Map<String, String> getCardMap(final String user) {
        HashMap<String, String> map = new HashMap<>();
        map.put(CARD_USER, user);
        return map;
    }

    /**
     * Creates a map with the post data to send to the subscriptions page
     * @param user the user's email
     * @return a map with the data
     */
    public static Map<String, String> getSubscriptionsMap(final String user) {
        HashMap<String, String> map = new HashMap<>();
        map.put(SUB_USER, user);
        return map;
    }
}
