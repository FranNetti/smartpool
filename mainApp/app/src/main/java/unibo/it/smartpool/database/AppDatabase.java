package unibo.it.smartpool.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import unibo.it.smartpool.database.dao.AbbonamentoDAO;
import unibo.it.smartpool.database.dao.AlarmNotDAO;
import unibo.it.smartpool.database.dao.AquaFitnessDAO;
import unibo.it.smartpool.database.dao.CardDAO;
import unibo.it.smartpool.database.dao.FavouriteDAO;
import unibo.it.smartpool.database.dao.InstructorDAO;
import unibo.it.smartpool.database.dao.LessonDAO;
import unibo.it.smartpool.database.dao.NotificationDAO;
import unibo.it.smartpool.database.dao.OpeningTimeDAO;
import unibo.it.smartpool.database.dao.RelationDAO;
import unibo.it.smartpool.database.dao.SubAlarmNotificationDAO;
import unibo.it.smartpool.database.dao.SwimCourseDAO;
import unibo.it.smartpool.database.dao.UserDAO;
import unibo.it.smartpool.database.dao.UserSubscriptionDAO;
import unibo.it.smartpool.database.entity.AlarmNotification;
import unibo.it.smartpool.database.entity.AquaFitness;
import unibo.it.smartpool.database.entity.Card;
import unibo.it.smartpool.database.entity.Favourite;
import unibo.it.smartpool.database.entity.Instructor;
import unibo.it.smartpool.database.entity.Lesson;
import unibo.it.smartpool.database.entity.NotificationMsg;
import unibo.it.smartpool.database.entity.OpeningTime;
import unibo.it.smartpool.database.entity.SubAlarmNotification;
import unibo.it.smartpool.database.entity.Subscription;
import unibo.it.smartpool.database.entity.SwimCourse;
import unibo.it.smartpool.database.entity.User;
import unibo.it.smartpool.database.entity.UserSubscription;
import unibo.it.smartpool.database.relation_entity.RelationCourseLess;
import unibo.it.smartpool.database.relation_entity.RelationCourseSub;
import unibo.it.smartpool.database.relation_entity.RelationFitnLess;
import unibo.it.smartpool.database.relation_entity.RelationFitnSub;
import unibo.it.smartpool.database.relation_entity.RelationFreeSwimSub;

/**
 * Class to easily handle the database with Room
 */
@Database(
        entities = {
                AlarmNotification.class,
                AquaFitness.class,
                Card.class,
                Favourite.class,
                Instructor.class,
                Lesson.class,
                NotificationMsg.class,
                OpeningTime.class,
                RelationCourseLess.class,
                RelationCourseSub.class,
                RelationFitnLess.class,
                RelationFitnSub.class,
                RelationFreeSwimSub.class,
                SubAlarmNotification.class,
                Subscription.class,
                SwimCourse.class,
                User.class,
                UserSubscription.class,
        }, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract AlarmNotDAO alarmNotDAO();
    public abstract AquaFitnessDAO aquaFitnessDAO();
    public abstract CardDAO cardDAO();
    public abstract FavouriteDAO favouriteDAO();
    public abstract InstructorDAO instructorDAO();
    public abstract LessonDAO lessonDAO();
    public abstract NotificationDAO notificationDAO();
    public abstract OpeningTimeDAO openingTimeDAO();
    public abstract RelationDAO relationDAO();
    public abstract SubAlarmNotificationDAO subAlarmNotificationDAO();
    public abstract AbbonamentoDAO abbonamentoDAO();
    public abstract SwimCourseDAO swimCourseDAO();
    public abstract UserDAO userDao();
    public abstract UserSubscriptionDAO userSubscriptionDAO();


    public enum DbOperation{
        ADD, UPDATE, REMOVE, GET;
    }

    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                            context.getApplicationContext(),
                            AppDatabase.class,
                            "smart_pool_database"
                    )
                    .build();

                }
            }
        }
        return INSTANCE;
    }
}
