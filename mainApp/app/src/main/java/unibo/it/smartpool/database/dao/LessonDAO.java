package unibo.it.smartpool.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import unibo.it.smartpool.database.entity.Lesson;

@Dao
public interface LessonDAO {

    @Insert
    void addLesson(final Lesson lesson);

    @Insert
    void addLessons(final List<Lesson> lessons);

    @Query("SELECT * FROM lesson")
    List<Lesson> getAll();

    @Update
    void updateLessons(final List<Lesson> lessons);

}
