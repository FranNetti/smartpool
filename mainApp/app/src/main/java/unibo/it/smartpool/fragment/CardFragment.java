package unibo.it.smartpool.fragment;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import unibo.it.smartpool.InformationManager;
import unibo.it.smartpool.R;
import unibo.it.smartpool.activity.AlarmChooserActivity;
import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.database.entity.AlarmNotification;
import unibo.it.smartpool.database.AppDatabase;
import unibo.it.smartpool.database.entity.Card;
import unibo.it.smartpool.view_elements.adapter.ClockAdapter;
import unibo.it.smartpool.view_elements.InfoTableRow;

/**
 * Class for a fragment that shows the card info
 */
public class CardFragment extends AlarmDealerFragment {

    public static CardFragment newInstance() {
        return new CardFragment();
    }

    public CardFragment() {
    }

    private SimpleDateFormat dateFormatView;
    private TableLayout table;
    private Date expirationDate;
    private TextView warningTv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card, container, false);

        final InformationManager manager = ViewModelProviders.of(getActivity()).get(InformationManager.class);
        LiveData<Card> userCard = manager.getUserCard();

        this.table = view.findViewById(R.id.card_info_table);
        this.dateFormatView = new SimpleDateFormat("dd MMMM YYYY", Locale.getDefault());
        this.clock = view.findViewById(R.id.card_clock_list);
        this.save = view.findViewById(R.id.card_save);
        TextView add = view.findViewById(R.id.card_clock_add);
        this.warningTv = view.findViewById(R.id.conn_warning);
        this.noAlarm = view.findViewById(R.id.no_alarm);

        /* button save setup */
        this.changeButtonStatus(this.save, false);
        this.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar today = Calendar.getInstance();
                Calendar lastUpdate = Calendar.getInstance();
                lastUpdate.setTime(expirationDate);
                lastUpdate.set(Calendar.HOUR_OF_DAY, 10);
                if (lastUpdate.before(today)) {
                    for (AlarmNotification alarm : updateAlarm) {
                        alarm.setOn(false);
                    }
                    for (AlarmNotification alarm : addAlarm) {
                        alarm.setOn(false);
                    }
                    manageAlarms(manager);
                    resetStatus();
                    Toast.makeText(getContext(), R.string.card_expired_text, Toast.LENGTH_LONG).show();
                } else {
                    manageAlarms(manager);
                    Toast.makeText(getContext(), R.string.change_succ, Toast.LENGTH_SHORT).show();
                    resetStatus();
                }
            }
        });

        /* clock adapter and listview setup */
        this.adp = new ClockAdapter(
                getContext(),
                android.R.layout.simple_list_item_1,
                alarmList
        );
        this.clock.setAdapter(adp);
        this.adp.setUpdateClockListener(this);

        /* button add setup */
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AlarmChooserActivity.class);
                getActivity().startActivityForResult(intent, AlarmChooserActivity.ALARM_ACTIVITY_INTENT_CODE);
            }
        });

        /* start async task to get info */
        userCard.observe(this, new Observer<Card>() {
            @Override
            public void onChanged(@Nullable Card card) {
                if (card != null) {
                    table.removeAllViews();
                    Context context = getContext();
                    String date = dateFormatView.format(card.getExpirationDate());
                    table.addView(new InfoTableRow(context, "Numero", card.getNumber()));
                    table.addView(new InfoTableRow(context, "Scadenza", date));
                    table.addView(new InfoTableRow(context, "credito", card.getCredit() + "€"));
                    expirationDate = card.getExpirationDate();
                }
            }
        });

        /* fill alarm notifications from the db */
        manager.getAllCardAlarms().observe(this, new Observer<List<AlarmNotification>>() {
            @Override
            public void onChanged(@Nullable List<AlarmNotification> alarmNotifications) {
                if (alarmNotifications != null) {
                    if (alarmNotifications.isEmpty()) {
                        noAlarm.setVisibility(View.VISIBLE);
                        clock.setVisibility(View.GONE);
                    } else {
                        alarmList.clear();
                        alarmList.addAll(alarmNotifications);
                        updateAdpHeight();
                    }
                }
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean isOnline = ConnectionUtilities.isConnectionAvailable(getContext());
        this.warningTv.setVisibility(isOnline ? View.GONE : View.VISIBLE);
    }

    private void manageAlarms(final InformationManager manager) {
        if (!deleteAlarm.isEmpty()) {
            manager.manageCardAlarmOperation(AppDatabase.DbOperation.REMOVE, deleteAlarm, expirationDate);
        }
        if (!updateAlarm.isEmpty()) {
            manager.manageCardAlarmOperation(AppDatabase.DbOperation.UPDATE, updateAlarm, expirationDate);
        }
        if (!addAlarm.isEmpty()) {
            manager.manageCardAlarmOperation(AppDatabase.DbOperation.ADD, addAlarm,expirationDate);
        }
    }

    private void resetStatus(){
        changeButtonStatus(save, false);
        addAlarm.clear();
        updateAlarm.clear();
        deleteAlarm.clear();
    }
}
