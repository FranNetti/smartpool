package unibo.it.smartpool.view_elements.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import unibo.it.smartpool.R;
import unibo.it.smartpool.database.entity.AlarmNotification;

/**
 * Class that implements a personalized version of an adapter for a list view
 */
public class ClockAdapter extends ArrayAdapter<AlarmNotification> {

    public interface UpdateClockListener {
        void onRequestUpdate(AlarmNotification alarm);

        void onStateChanged(AlarmNotification alarm, boolean on);

    }

    private UpdateClockListener listener;

    public ClockAdapter(Context context, int textViewResourceId, List<AlarmNotification> objects) {
        super(context, textViewResourceId, objects);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.notification_item, null);
        final AlarmNotification item = getItem(position);
        final TextView name = (TextView) view.findViewById(R.id.not_text);
        final Switch enable = view.findViewById(R.id.not_switch);
        final ImageButton update = view.findViewById(R.id.not_update);

        /* text view setup */
        int quantity = item.getQuantity();
        if(quantity == 0){
            name.setText(getContext().getString(R.string.current_day));
        } else {
            if( quantity > 1) {
                quantity = item.getPeriod().getIdMessagePlural();
            } else {
                quantity = item.getPeriod().getIdMessage();
            }
            name.setText(item.getQuantity() + " " + getContext().getString(quantity) + " " + getContext().getString(R.string.before));
        }
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onRequestUpdate(item);
                }
            }
        });

        /* switch setup */
        enable.setChecked(item.isOn());
        enable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(listener != null){
                    listener.onStateChanged(item, isChecked);
                }
            }
        });

        /* image button setup */
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onRequestUpdate(item);
                }
            }
        });
        return view;
    }

    public int getTotalHeight() {
        return this.getCount() * this.getSingleElementHeight();
    }

    public int getSingleElementHeight() {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 50, getContext().getResources().getDisplayMetrics());
    }

    public void setUpdateClockListener(final UpdateClockListener elem){
        this.listener = elem;
    }
}
