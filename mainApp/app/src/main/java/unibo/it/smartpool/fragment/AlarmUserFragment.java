package unibo.it.smartpool.fragment;

import android.content.Intent;

import unibo.it.smartpool.view_elements.adapter.ClockAdapter;

public interface AlarmUserFragment extends ClockAdapter.UpdateClockListener{

    void onAlarmCreation(final Intent data);
}
