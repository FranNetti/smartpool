package unibo.it.smartpool.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;

import java.util.Objects;

/**
 * Class that implements the table for subscriptions' alarm notifications
 */
@Entity
public class SubAlarmNotification extends AlarmNotification {

    @ColumnInfo
    private int subReference;

    /**
     * Constructor.
     * @param quantity how much period before
     * @param dateType the time period
     * @param on if it's on
     * @param subReference the subscription this alarm refers to
     */
    public SubAlarmNotification(final int quantity, final String dateType, final boolean on, final int subReference){
        super(quantity, dateType, on);
        this.subReference = subReference;
    }

    /**
     * Constructor.
     * @param quantity how much period before
     * @param period the time period
     * @param on if it's on
     * @param subReference the subscription this alarm refers to
     */
    public SubAlarmNotification(final int quantity, final TimePeriod period, final boolean on, final int subReference){
        super(quantity,period,on);
        this.subReference = subReference;
    }

    /**
     * Constructor. It's supposed to be on.
     * @param quantity how much period before
     * @param period the time period
     * @param subReference the subscription this alarm refers to
     */
    public SubAlarmNotification(final int quantity, final TimePeriod period, final int subReference){
        this(quantity, period, true, subReference);
    }

    public SubAlarmNotification(final AlarmNotification alarm, final int subReference){
        super(alarm.getQuantity(), alarm.getPeriod(), alarm.isOn());
        this.subReference = subReference;
    }

    public void setSubReference(int subReference) {
        this.subReference = subReference;
    }

    public int getSubReference() {
        return subReference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SubAlarmNotification that = (SubAlarmNotification) o;
        return subReference == that.subReference;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), subReference);
    }
}
