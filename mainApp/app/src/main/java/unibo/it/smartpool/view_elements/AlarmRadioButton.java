package unibo.it.smartpool.view_elements;


import android.content.Context;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.TypedValue;
import android.widget.LinearLayout;

import unibo.it.smartpool.R;
import unibo.it.smartpool.database.entity.AlarmNotification;

/**
 * Class for a radio button personalized.
 */
public class AlarmRadioButton extends AppCompatRadioButton {

    public static final int CURRENT_DAY_ELEMENT_ID = 133;
    private static final int TEXT_SIZE = 18;
    private static final int PD_LEFT = 40;
    private static final int PD_RIGHT = 40;
    private static final int PD_TOP = 40;
    private static final int PD_BOTTOM = 40;

    private final AlarmNotification alarm;

    public AlarmRadioButton(final Context context, final AlarmNotification alarm){
        super(context);
        this.alarm = alarm;
        this.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        ));
        final int quant = alarm.getQuantity();
        final String message;
        if(quant > 0) {
            final int msg = quant > 1 ? alarm.getPeriod().getIdMessagePlural() : alarm.getPeriod().getIdMessage();
            message = quant + " " + context.getString(msg) + " " + context.getString(R.string.before);
        } else {
            message = context.getString(R.string.current_day);
            this.setId(CURRENT_DAY_ELEMENT_ID);
        }
        this.setText(message);
        this.setTextSize(TypedValue.COMPLEX_UNIT_SP,TEXT_SIZE);
        this.setPadding(PD_LEFT,PD_TOP,PD_RIGHT,PD_BOTTOM);
    }

    public AlarmRadioButton(final Context context, final String text){
        super(context);
        this.alarm = null;
        this.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        ));
        this.setText(text);
        this.setTextSize(TypedValue.COMPLEX_UNIT_SP,TEXT_SIZE);
        this.setPadding(PD_LEFT,PD_TOP,PD_RIGHT,PD_BOTTOM);
    }

    public AlarmNotification getAlarm() {
        return alarm;
    }
}
