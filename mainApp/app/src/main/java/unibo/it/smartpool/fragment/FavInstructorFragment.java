package unibo.it.smartpool.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import unibo.it.smartpool.InformationManager;
import unibo.it.smartpool.R;
import unibo.it.smartpool.database.entity.Instructor;
import unibo.it.smartpool.database.entity.User;
import unibo.it.smartpool.utils.ParallelExecutor;
import unibo.it.smartpool.view_elements.adapter.InstructorAdapter;

public class FavInstructorFragment extends Fragment implements InstructorAdapter.InstructorAdapterListener {

    public static FavInstructorFragment newInstance() {
        return new FavInstructorFragment();
    }


    public FavInstructorFragment() {
    }

    private InformationManager manager;
    private InstructorAdapter adp;
    private TextView emptyList;
    private User user;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager = ViewModelProviders.of(getActivity()).get(InformationManager.class);
        ParallelExecutor executor = new ParallelExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                user = manager.getUser();
            }
        });
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState){
        final View view = inflater.inflate(R.layout.fragment_instructor, container, false);
        final ListView listView = view.findViewById(R.id.instructor_list);
        final TextView emptyList = view.findViewById(R.id.empty_text);

        final List<Instructor> instructorList = new ArrayList<>();
        manager.getAllFavouritesOf(user.getEmail()).observe(this, new Observer<List<Instructor>>() {
            @Override
            public void onChanged(@Nullable List<Instructor> favourites) {
                if(favourites != null){
                    instructorList.clear();
                    instructorList.addAll(favourites);
                    adp.notifyDataSetChanged();
                    if(favourites.isEmpty()){
                        listView.setVisibility(View.GONE);
                        emptyList.setVisibility(View.VISIBLE);
                    } else {
                        listView.setVisibility(View.VISIBLE);
                        emptyList.setVisibility(View.GONE);
                    }
                }
            }
        });

        /* clock adapter and listview setup */
        this.adp = new InstructorAdapter(
                getContext(),
                android.R.layout.simple_list_item_1,
                instructorList,
                user.getEmail(),
                this
        );
        listView.setAdapter(adp);

        return view;
    }

    @Override
    public void onAddressPressed(final String addr) {
        //query got from google apis
        String addressEncoded = "0,0?q=" + addr.replaceAll(" ", "+");
        Uri address = Uri.parse("geo:" + addressEncoded);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, address);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    @Override
    public void onInstructorSelected(Instructor instr) {
        // never called
    }

    @Override
    public void onInstructorDeselected(Instructor instr) {
        if(user != null){
            manager.deleteFavourite(instr, user.getEmail());
            Toast.makeText(getContext(), getString(R.string.not_favorite), Toast.LENGTH_SHORT).show();
        }
    }
}
