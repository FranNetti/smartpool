package unibo.it.smartpool.fragment;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;

import unibo.it.smartpool.InformationManager;
import unibo.it.smartpool.R;
import unibo.it.smartpool.activity.MainActivity;
import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.connection.HttpException;
import unibo.it.smartpool.database.entity.AquaFitness;
import unibo.it.smartpool.database.entity.Subscription;
import unibo.it.smartpool.database.entity.FreeSwim;
import unibo.it.smartpool.utils.ParallelExecutor;
import unibo.it.smartpool.view_elements.MyButton;
import unibo.it.smartpool.view_elements.PriceTableRow;

public class FreeSwimFragment extends Fragment{

    private InformationManager manager;
    private String userEmail;
    private static final String TAG = "Swim_free_tag";
    private Context context;

    public static FreeSwimFragment newInstance() {
        return new FreeSwimFragment();
    }

    public FreeSwimFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.manager = ViewModelProviders.of(getActivity()).get(InformationManager.class);
        new ParallelExecutor().execute(new Runnable() {
            @Override
            public void run() {
                userEmail = manager.getUser().getEmail();
            }
        });
        this.context = getContext();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_free_swim, container, false);
        final RelativeLayout layout = view.findViewById(R.id.freeSwim);

        /* course title setup */
        final TextView courseTitle = layout.findViewById(R.id.coursetitle);
        courseTitle.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        courseTitle.setTextSize(22);
        courseTitle.setTypeface(null, Typeface.BOLD);

        /* course picture setup */
        ImageView coursePic = new ImageView(getContext());
        coursePic.setImageResource(R.drawable.freeswim);
        LinearLayout img = layout.findViewById(R.id.imgCorso);
        img.addView(coursePic);

        final TableLayout table = (TableLayout) layout.findViewById(R.id.table);
        final TableRow rowTitle = layout.findViewById(R.id.row_title);
        final TableRow rowDesc = layout.findViewById(R.id.row_description);
        final TableRow rowSubTitle = layout.findViewById(R.id.row_secondTitle);

        final LiveData<FreeSwim> courseList = this.manager.getFreeSwimCourse();

        final MainActivity main = (MainActivity)getActivity();

        courseList.observe(this, new Observer<FreeSwim>() {
            @Override
            public void onChanged(@Nullable FreeSwim course) {
                if (course != null) {
                    courseTitle.setText(course.getName());
                    List<Subscription> list = course.getSubscriptions();
                    int id = 0;
                    table.removeAllViews();
                    table.addView(rowTitle);
                    table.addView(rowSubTitle);
                    for(final Subscription abb : list){
                        MyButton button = new MyButton(getContext());
                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new AddSubscription(abb).execute();
                            }
                        });
                        table.addView(new PriceTableRow(getContext(), abb.getPrice(), abb.getType()+" "+abb.getLabel(), button));
                    }
                } else {
                    main.noDataAvailable();
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!ConnectionUtilities.isConnectionAvailable(getContext())){
            Toast.makeText(getContext(), getString(R.string.warning_text), Toast.LENGTH_LONG).show();
        }
    }

    private class AddSubscription extends AsyncTask<Void, Void, Integer> {
        private Subscription sub;

        public AddSubscription(Subscription sub){
            this.sub = sub;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            final Map<String, String> map = ConnectionUtilities.getFreeSwimAddSubscriptionMap(
                    userEmail,
                    String.valueOf(this.sub.getId()),
                    this.sub.getLabel(),
                    this.sub.getType(),
                    this.sub.getPrice()
            );

            final String url = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.ADD_FREE_RECORD;
            try {
                if (ConnectionUtilities.isConnectionAvailable(context)) {
                    ConnectionUtilities.getDataFromUrl(url, map, true);
                    manager.updateSubscriptions();
                    manager.updateCard();
                    return HttpURLConnection.HTTP_OK;
                } else {
                    return HttpURLConnection.HTTP_GONE;
                }
            }catch (HttpException e){
                Log.e(TAG, "Error in the account balance, HttpCode: " + e.getCode(), e);
                return e.getCode();
            }
        }

        @Override
        protected void onPostExecute(final Integer success) {
            switch (success){
                case HttpURLConnection.HTTP_OK:
                    Toast.makeText(context,R.string.sub_add,Toast.LENGTH_LONG).show();
                    break;
                case HttpURLConnection.HTTP_GONE:
                    ConnectionUtilities.alertConnectionAbsence(context);
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    new AlertDialog.Builder(context)
                            .setMessage(R.string.balance)
                            .setPositiveButton("OK", null)
                            .show();
                    break;
                default:
                    ConnectionUtilities.alertConnectionError(context);
                    break;
            }
        }

    }


}
