package unibo.it.smartpool.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

@Entity(primaryKeys = {"user", "instructorFavourite"})
public class Favourite {

    @ColumnInfo
    @NonNull
    private String user;

    @ColumnInfo
    @NonNull
    private String instructorFavourite;

    public Favourite(final String user, final String instructorFavourite){
        this.user = user;
        this.instructorFavourite = instructorFavourite;
    }

    @NonNull
    public String getInstructorFavourite() {
        return instructorFavourite;
    }

    @NonNull
    public String getUser() {
        return user;
    }

    public void setInstructorFavourite(@NonNull String instructorFavourite) {
        this.instructorFavourite = instructorFavourite;
    }

    public void setUser(@NonNull String user) {
        this.user = user;
    }
}
