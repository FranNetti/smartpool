package unibo.it.smartpool.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import unibo.it.smartpool.database.entity.NotificationMsg;

/**
 * Interface to handle NotificationMsg table in the database
 */
@Dao
public interface NotificationDAO {

    @Query("SELECT * FROM notificationMsg")
    LiveData<List<NotificationMsg>> getAll();

    @Query("SELECT * FROM notificationMsg WHERE date LIKE :date")
    List<NotificationMsg> getAllNotificationOfDay(final String date);

    @Query("SELECT * FROM notificationMsg WHERE type = :type ORDER BY date")
    List<NotificationMsg> getAllNotificationOfType(final int type);

    @Query("DELETE FROM notificationMsg WHERE alarm_ref IN (:alarmsUid)")
    void deleteAllNotificationOfAlarms(final List<Integer> alarmsUid);

    @Query("SELECT * FROM notificationMsg WHERE alarm_ref IN (:alarmsUid) ORDER BY date")
    List<NotificationMsg> getAllNotificationOfAlarms(final List<Integer> alarmsUid);

    @Query("UPDATE notificationMsg SET date = :newDate WHERE type = :type AND date LIKE :oldDate")
    void updateNotification(final String oldDate, final String newDate, final int type);

    @Query("DELETE FROM notificationMsg WHERE date LIKE :date")
    void deleteAllOfDay(final String date);

    @Query("DELETE FROM notificationMsg WHERE type = :type")
    void deleteAllOfType(final int type);

    @Query("DELETE FROM notificationMsg WHERE alarm_ref IN (:alarmsUid)")
    void deleteAllNotificationsOfAlarms(final List<Integer> alarmsUid);

    @Query("DELETE FROM notificationMsg WHERE date LIKE :date AND type = :type")
    void delete(final int type, final String date);

    @Delete
    void delete(final NotificationMsg notification);

    @Delete
    void deleteAll(final List<NotificationMsg> notifications);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(final List<NotificationMsg> notifications);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(final NotificationMsg notification);

    @Update
    void updateNotifications(final List<NotificationMsg> notifications);
}
