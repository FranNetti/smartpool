package unibo.it.smartpool.database.relation_entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;

import unibo.it.smartpool.database.entity.Subscription;
import unibo.it.smartpool.database.entity.SwimCourse;

@Entity(
        primaryKeys = {"courseRef", "subscriptionRef"},
        foreignKeys = {
                @ForeignKey(
                        entity = SwimCourse.class,
                        parentColumns = "id",
                        childColumns = "courseRef",
                        onDelete = ForeignKey.CASCADE
                ),
                @ForeignKey(
                        entity = Subscription.class,
                        parentColumns = {"id"},
                        childColumns = {"subscriptionRef"},
                        onDelete = ForeignKey.CASCADE
                )
        },
        indices = @Index(
                value = {"subscriptionRef"},
                name = "course_sub_index"
        )
)
public class RelationCourseSub {

    @ColumnInfo
    private int courseRef;
    @ColumnInfo
    private int subscriptionRef;

    public RelationCourseSub(final int courseRef, final int subscriptionRef){
        this.courseRef = courseRef;
        this.subscriptionRef = subscriptionRef;
    }

    public int getCourseRef() {
        return courseRef;
    }

    public void setCourseRef(int courseRef) {
        this.courseRef = courseRef;
    }

    public int getSubscriptionRef() {
        return subscriptionRef;
    }

    public void setSubscriptionRef(int subscriptionRef) {
        this.subscriptionRef = subscriptionRef;
    }
}
