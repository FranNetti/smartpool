package unibo.it.smartpool.connection;

import java.net.HttpURLConnection;

/**
 * Class that handles and http timeout event
 */
public class HttpTimeoutException extends HttpException {

    public HttpTimeoutException() {
        super(HttpURLConnection.HTTP_CLIENT_TIMEOUT);
    }
}
