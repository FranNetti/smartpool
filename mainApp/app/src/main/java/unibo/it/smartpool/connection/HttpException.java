package unibo.it.smartpool.connection;

/**
 * Class that handles an exception created by an http event
 */
public class HttpException extends Exception {

    private final int code;

    public HttpException(final int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }
}
