package unibo.it.smartpool.activity;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Pair;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Toast;

import unibo.it.smartpool.InformationManager;
import unibo.it.smartpool.database.entity.User;
import unibo.it.smartpool.database.entity.UserSubscription;
import unibo.it.smartpool.fragment.AlarmUserFragment;
import unibo.it.smartpool.fragment.AquaFitnessFragment;
import unibo.it.smartpool.fragment.CardFragment;
import unibo.it.smartpool.fragment.EntranceFragment;
import unibo.it.smartpool.fragment.FavInstructorFragment;
import unibo.it.smartpool.fragment.FreeSwimFragment;
import unibo.it.smartpool.fragment.InfoFragment;
import unibo.it.smartpool.fragment.InstructorsFragment;
import unibo.it.smartpool.fragment.SubscriptionFragment;
import unibo.it.smartpool.fragment.SwimCourseFragment;
import unibo.it.smartpool.fragment.SubscriptionNotificationFragment;
import unibo.it.smartpool.utils.MenuState;
import unibo.it.smartpool.view_elements.NavigationPers;
import unibo.it.smartpool.R;
import unibo.it.smartpool.fragment.ChangeInfoFragment;
import unibo.it.smartpool.fragment.EmptyFragment;
import unibo.it.smartpool.fragment.FragmentListener;
import unibo.it.smartpool.fragment.ProfileFragment;

/**
 * App main activity
 */
public class MainActivity extends LoggedActivity implements NavigationPers.NavigationPersListener, FragmentListener {

    private NavigationPers navigation;
    private boolean onChangeInfo = false;
    private boolean onSubAlarmView = false;
    private boolean onSubEnabled = false;
    private InformationManager manager;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.manager = ViewModelProviders.of(this).get(InformationManager.class);

        final NavigationView navigationView = findViewById(R.id.nav_view);
        final Toolbar toolbar = findViewById(R.id.toolbar);
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);

        setSupportActionBar(toolbar);

        /* navigation drawer setup */
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        /* navigation menu handler setup */
        this.navigation = new NavigationPers(navigationView, drawer, this, MenuState.INFO);
        onStateChanged(this.navigation.getCurrentMenuState());
    }

    @Override
    public void onBackPressed() {
        /*
         if the drawer is open, the close it
          or if there is a particular situation handle it
          or if the navigation menu has to return back let it handle it
          or let the class above handle it
         */
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(this.onChangeInfo) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.exit_alert_title)
                    .setMessage(R.string.change_back)
                    .setPositiveButton(R.string.exit_affermative, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            onChangeInfo = false;
                            getSupportFragmentManager().popBackStack();
                        }
                    })
                    .setNegativeButton(R.string.exit_negative, null)
                    .show();
        } else if (this.onSubAlarmView){
            onSubAlarmView = false;
            getSupportFragmentManager().popBackStack();
        } else if (this.onSubEnabled){
            onSubEnabled = false;
            SubscriptionFragment fragment = (SubscriptionFragment) getSupportFragmentManager().findFragmentByTag(MenuState.FARE.toString());
            fragment.onDischargeEvent();
        } else if(this.navigation.canReturnBack()) {
            this.navigation.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode){
            case AlarmChooserActivity.ALARM_ACTIVITY_INTENT_CODE:
                if(resultCode == Activity.RESULT_OK){
                    AlarmUserFragment fragment = (AlarmUserFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
                    fragment.onAlarmCreation(data);
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Override
    public void onLoginCompleted(final User user, final boolean newUser) {
        if(newUser){
            this.manager.addUser(user);
        } else {
            this.manager.setUser(user);
        }
    }

    /**
     * Method to use when there is no data available; it creates a default fragment.
     */
    public void noDataAvailable() {
        final Fragment fr = EmptyFragment.newInstance();
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_layout, fr);
        transaction.commit();
    }

    @Override
    public void onStateChanged(final MenuState state) {
        this.onChangeInfo = false;
        this.onSubAlarmView = false;
        Pair<Fragment, String> param = getFragment(state);
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_layout, param.first, param.second);
        transaction.commit();
    }

    @Override
    public void refresh() {
        onStateChanged(navigation.getCurrentMenuState());
    }

    @Override
    public void onButtonChangeInfoPressed() {
        this.onChangeInfo = true;
        final Fragment fr = ChangeInfoFragment.newInstance();
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_layout, fr);
        // add to the fragment stack the new fragment
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onChangesCompleted() {
        getSupportFragmentManager().popBackStack();
        this.onChangeInfo = false;
    }

    @Override
    public void onSelectedSubscription(final UserSubscription sub) {
        this.onSubAlarmView = true;
        final Fragment fr = SubscriptionNotificationFragment.newInstance(sub);
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_layout, fr);
        // add to the fragment stack the new fragment
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onSubscriptionSelection(final boolean enabled) {
        this.onSubEnabled = enabled;
    }

    /**
     * Method to get the new fragment to add to the screen
     * @param state the current state from which will be recovered the fragment
     * @return a pair containing the fragment and a tag to be assigned
     */
    private Pair<Fragment, String> getFragment(final MenuState state) {
        Fragment fr;
        String tag;
        switch (state){
            case FITNESS:
                fr = AquaFitnessFragment.newInstance();
                tag = MenuState.FITNESS.toString();
                break;
            case COURSE:
                fr = SwimCourseFragment.newInstance();
                tag = MenuState.COURSE.toString();
                break;
            case FREE:
                fr = FreeSwimFragment.newInstance();
                tag = MenuState.FREE.toString();
                break;
            case INSTRUCTOR:
                fr = InstructorsFragment.newInstance();
                tag = MenuState.INSTRUCTOR.toString();
                break;
            case INFO:
                fr = InfoFragment.newInstance();
                tag = MenuState.INFO.toString();
                break;
            case ENTRANCE:
                fr = EntranceFragment.newInstance();
                tag = MenuState.ENTRANCE.toString();
                break;
            case CARD:
                fr = CardFragment.newInstance();
                tag = MenuState.CARD.toString();
                break;
            case FARE:
                fr = SubscriptionFragment.newInstance();
                tag = MenuState.FARE.toString();
                break;
            case PROFILE:
                fr = ProfileFragment.newInstance();
                tag = MenuState.PROFILE.toString();
                break;
            case FAVOURITE:
                fr = FavInstructorFragment.newInstance();
                tag = MenuState.FAVOURITE.toString();
                break;
            default:
                fr = EmptyFragment.newInstance();
                tag = "empty";
                break;
        }
        return Pair.create(fr, tag);
    }
}
