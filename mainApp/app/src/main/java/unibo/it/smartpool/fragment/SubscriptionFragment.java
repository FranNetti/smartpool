package unibo.it.smartpool.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import unibo.it.smartpool.InformationManager;
import unibo.it.smartpool.R;
import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.database.entity.UserSubscription;
import unibo.it.smartpool.database.entity.UserSubscription.SubscriptionType;
import unibo.it.smartpool.view_elements.adapter.SubscriptionAdapter;

public class SubscriptionFragment extends Fragment{

    public static SubscriptionFragment newInstance() {
        return new SubscriptionFragment();
    }

    private SubscriptionFragmentListener mListener;

    private TextView warningTv;
    private SubscriptionAdapter adp;
    private Button btnDelete;
    private Button btnDischarge;
    private boolean anyExpired = false;

    public SubscriptionFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subscription, container, false);
        final InformationManager manager = ViewModelProviders.of(getActivity()).get(InformationManager.class);

        this.warningTv = view.findViewById(R.id.conn_warning);
        this.btnDelete = view.findViewById(R.id.sub_btn_delete);
        this.btnDischarge = view.findViewById(R.id.sub_btn_discharge);
        final ListView listView = view.findViewById(R.id.sub_list);
        final TextView emptyListMsg = view.findViewById(R.id.sub_no_sub);

        /* listview setup */
        final List<UserSubscription> userSubList = new ArrayList<>();
        this.adp = new SubscriptionAdapter(
                getContext(),
                android.R.layout.simple_list_item_1,
                userSubList
        );
        listView.setAdapter(adp);

        manager.getAllSubscription().observe(this, new Observer<List<UserSubscription>>() {
            @Override
            public void onChanged(@Nullable List<UserSubscription> subscriptions) {
                if(subscriptions != null){
                    if(subscriptions.isEmpty()){
                        emptyListMsg.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                    } else {
                        emptyListMsg.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                        userSubList.clear();
                        userSubList.addAll(subscriptions);
                        adp.notifyDataSetChanged();
                        listView.setLayoutParams(new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                adp.getTotalHeight()
                        ));
                        for(UserSubscription sub : subscriptions){
                            if(sub.isExpired()){
                                anyExpired = true;
                                break;
                            }
                        }
                    }
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserSubscription sub = (UserSubscription) parent.getItemAtPosition(position);
                if(sub.getSubscriptionType() != SubscriptionType.SINGLE_ENTRANCE){
                    if(!sub.isExpired() && mListener != null){
                        mListener.onSelectedSubscription(sub);
                    }
                }

            }
        });

        listView.setLongClickable(true);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                if(anyExpired) {
                    setChange(true);
                    if (mListener != null) {
                        mListener.onSubscriptionSelection(true);
                    }
                    return true;
                }
                return false;
            }
        });

        /* buttons setup */
        btnDelete.setVisibility(View.GONE);
        btnDischarge.setVisibility(View.GONE);

        btnDischarge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setChange(false);
                if(mListener != null){
                    mListener.onSubscriptionSelection(false);
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<UserSubscription> deleteList = new ArrayList<>();
                for(int x = 0; x < listView.getCount(); x++){
                    View item = listView.getChildAt(x);
                    CheckBox checkBox = item.findViewById(R.id.sub_checkbox);
                    if(checkBox.isChecked()){
                        deleteList.add(adp.getItem(x));
                    }
                }
                manager.deleteUserSubscriptions(deleteList);
                setChange(false);
                if(mListener != null){
                    mListener.onSubscriptionSelection(false);
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean isOnline = ConnectionUtilities.isConnectionAvailable(getContext());
        this.warningTv.setVisibility(isOnline ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SubscriptionFragmentListener) {
            mListener = (SubscriptionFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement SubscriptionFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onDischargeEvent(){
        setChange(false);
    }

    private void setChange(final boolean change){
        this.adp.setEditable(change);
        this.adp.notifyDataSetChanged();
        this.btnDelete.setVisibility(change ? View.VISIBLE : View.GONE);
        this.btnDischarge.setVisibility(change ? View.VISIBLE : View.GONE);
    }

    public interface SubscriptionFragmentListener {
        void onSelectedSubscription(UserSubscription sub);
        void onSubscriptionSelection(boolean enabled);
    }
}
