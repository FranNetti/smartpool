package unibo.it.smartpool.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.connection.HttpException;
import unibo.it.smartpool.database.AppDatabase;
import unibo.it.smartpool.database.entity.OpeningTime;
import unibo.it.smartpool.database.dao.OpeningTimeDAO;
import unibo.it.smartpool.utils.ParallelExecutor;

public class OpeningTimesRepository {

    private static final String TAG = "op_time_tag";

    private LiveData<List<OpeningTime>> opList;
    private OpeningTimeDAO table;
    private ParallelExecutor executor = new ParallelExecutor();

    public OpeningTimesRepository(final Context context){
        this.table = AppDatabase.getDatabase(context).openingTimeDAO();
        this.opList = table.getAllObservableOpTimes();
        updateOpeningTimes();
    }

    public LiveData<List<OpeningTime>> getOpeningTimes() {
        updateOpeningTimes();
        return opList;
    }

    private void updateOpeningTimes(){
        executor.execute(new OpTimesCheckUpdate());
    }

    private class OpTimesCheckUpdate implements Runnable {

        @Override
        public void run() {
            /*
            if there is at least a day of difference between lastUpdate and today,
            or the table is empty, download data from the server
             */
            final List<OpeningTime> oldOpList = table.getAllOpTimes();
            boolean hasToUpdate = false;
            for(OpeningTime op : oldOpList){
                hasToUpdate |= op.hasToUpdate();
            }
            if(hasToUpdate || oldOpList.isEmpty()){
                boolean ok = true;
                final List<OpeningTime> list = new ArrayList<>();
                final String url = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.INFO_PAGE;
                try{
                    final String data = ConnectionUtilities.getDataFromUrl(url, null, false);
                    final JSONObject schedule = new JSONObject(data).getJSONObject("orari");
                    final Iterator<String> schedule_keys = schedule.keys();
                    while (schedule_keys.hasNext()) {
                        String key = schedule_keys.next();
                        String single_schedule = schedule.getString(key);
                        list.add(new OpeningTime(key, single_schedule));
                    }
                } catch (HttpException e) {
                    ok = false;
                    Log.e(TAG, "Error in openingRepository updateOpeningTimes, HttpCode: " + e.getCode(), e);
                } catch (JSONException e){
                    ok = false;
                    Log.e(TAG, "Error in openingRepository updateOpeningTimes", e);
                }
                if(ok){
                    if(oldOpList .isEmpty()) {
                        table.insertOpTimes(list);
                    } else {
                        table.updateOpTimes(list);
                    }
                }
            }
        }

    }
}
