package unibo.it.smartpool.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Objects;

@Entity
public class Subscription implements Serializable{

    private static final String ID_FIELD = "id";
    private static final String TYPE_FIELD = "tipo";
    private static final String LABEL_FIELD = "label";
    private static final String PRICE_FIELD = "prezzo";

    @PrimaryKey
    private int id;
    @ColumnInfo
    private String price;
    @ColumnInfo
    private String type;
    @ColumnInfo
    private String label;

    public Subscription(final int id, final String price, final String type, final String label) {
        this.price = price;
        this.type = type;
        this.label = label;
        this.id = id;
    }

    public Subscription(final JSONObject obj) throws JSONException {
        this.id = obj.getInt(ID_FIELD);
        this.price = obj.getString(PRICE_FIELD);
        this.type = obj.getString(TYPE_FIELD);
        this.label = obj.getString(LABEL_FIELD);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean update(final Subscription abb){
        boolean update = false;
        if(!this.price.equals(abb.price) || !this.type.equals(abb.type) || !this.label.equals(abb.label)){
            update = true;
            this.price = abb.price;
            this.type = abb.type;
            this.label = abb.label;
        }
        return update;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subscription that = (Subscription) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
