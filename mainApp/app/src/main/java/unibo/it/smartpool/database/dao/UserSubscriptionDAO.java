package unibo.it.smartpool.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import unibo.it.smartpool.database.entity.UserSubscription;

@Dao
public interface UserSubscriptionDAO {

    @Query("SELECT * FROM usersubscription WHERE user LIKE :user ORDER BY expiration_date DESC, num_entr DESC")
    List<UserSubscription> getAllSubscriptionOf(final String user);

    @Query("SELECT * FROM usersubscription WHERE user LIKE :user ORDER BY expiration_date DESC, num_entr DESC")
    LiveData<List<UserSubscription>> getAllObservableSubscriptionOf(final String user);

    @Query("SELECT * FROM usersubscription WHERE id = :id")
    UserSubscription getSubscriptionFromId(final int id);

    @Query("SELECT * FROM usersubscription WHERE user LIKE :user AND lastUpdate <= (SELECT MIN(lastUpdate) from usersubscription) LIMIT 1")
    UserSubscription getLeastUpdatedSubscriptionOf(final String user);

    @Query("SELECT reference FROM usersubscription WHERE id = :subReference")
    String getReferenceOfSubscription(final int subReference);

    @Query("UPDATE usersubscription SET user = :newEmail WHERE user LIKE :oldEmail")
    void updateUserSubscriptionsEmail(final String oldEmail, final String newEmail);

    @Query("DELETE FROM usersubscription WHERE user LIKE :user")
    void deleteAllSubscriptionsOf(final String user);

    @Insert
    void addSubscription(UserSubscription subscription);

    @Insert
    void addSubscriptions(List<UserSubscription> subscriptions);

    @Update
    void updateSubscription(UserSubscription subscription);

    @Update
    void updateSubscriptions(List<UserSubscription> subscriptions);

    @Delete
    void deleteSubscription(UserSubscription subscription);

    @Delete
    void deleteSubscriptions(List<UserSubscription> subscriptions);
}
