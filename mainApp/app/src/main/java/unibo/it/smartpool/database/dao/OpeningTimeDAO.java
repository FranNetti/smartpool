package unibo.it.smartpool.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import unibo.it.smartpool.database.entity.OpeningTime;

@Dao
public interface OpeningTimeDAO {

    @Query("SELECT * FROM opening")
    LiveData<List<OpeningTime>> getAllObservableOpTimes();

    @Query("SELECT * FROM opening")
    List<OpeningTime> getAllOpTimes();

    @Query("SELECT MIN(lastUpdate) FROM opening")
    String getLastUpdateDay();

    @Query("DELETE FROM opening")
    void deleteAllOpTimes();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOpTimes(List<OpeningTime> openings);

    @Update
    void updateOpTimes(List<OpeningTime> openings);
}
