package unibo.it.smartpool.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import unibo.it.smartpool.database.entity.AlarmNotification;
import unibo.it.smartpool.database.entity.SubAlarmNotification;

@Dao
public interface SubAlarmNotificationDAO {

    @Query("SELECT * FROM subalarmnotification WHERE subReference = :subId")
    LiveData<List<SubAlarmNotification>> getAllObservableAlarmsOfSubscription(final int subId);

    @Query("SELECT * FROM subalarmnotification WHERE subReference = :subId")
    List<SubAlarmNotification> getAllAlarmsOfSubscription(final int subId);

    @Query("SELECT * FROM subalarmnotification WHERE `on` = 1 AND subReference = :subId ORDER BY quant DESC")
    List<AlarmNotification> getAllSwitchedOnAlarmNotificationOfSubscription(final int subId);

    @Query("UPDATE subalarmnotification SET `on` = :on WHERE uid = :id")
    void updateAlarmStatus(final int id, final boolean on);

    @Query("DELETE FROM subalarmnotification WHERE subReference = :subscription")
    void deleteAlarm(final int subscription);

    @Update
    void updateAlarm(SubAlarmNotification alarm);

    @Update
    void updateAll(SubAlarmNotification... alarms);

    @Update
    void updateAll(List<SubAlarmNotification> alarms);

    @Insert
    void insertAll(SubAlarmNotification... alarms);

    @Insert
    List<Long> insertAll(List<SubAlarmNotification> alarms);

    @Insert
    void insert(SubAlarmNotification alarm);

    @Delete
    void delete(SubAlarmNotification alarm);

    @Delete
    void deleteAll(SubAlarmNotification... alarms);

    @Delete
    void deleteAll(List<SubAlarmNotification> alarms);

}
