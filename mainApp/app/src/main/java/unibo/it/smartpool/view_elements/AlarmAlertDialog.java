package unibo.it.smartpool.view_elements;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import unibo.it.smartpool.R;
import unibo.it.smartpool.database.entity.AlarmNotification;

/**
 * Class for a dialog that can create a personalized alarm
 */
public class AlarmAlertDialog extends DialogFragment {

    public interface AlarmAlertDialogListener {
        void onPositiveButtonPressed(AlarmNotification alarm);
    }

    private AlarmAlertDialogListener listener;

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        ConstraintLayout view = (ConstraintLayout) inflater.inflate(R.layout.alarm_dialog_layout, null);

        final EditText quant = view.findViewById(R.id.dialog_quant_text);
        final Spinner options = view.findViewById(R.id.dialog_spinner);
        MySpinnerAdapter adp = new MySpinnerAdapter(
                getActivity(),
                android.R.layout.simple_spinner_item,
                AlarmNotification.TimePeriod.values()
        );
        options.setAdapter(adp);

        builder.setView(view)
                .setTitle(R.string.alarm_dialog_title)
                .setPositiveButton(R.string.create_pers, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if(listener != null){
                            int number = Integer.parseInt(quant.getText().toString());
                                AlarmNotification.TimePeriod period = (AlarmNotification.TimePeriod)options.getSelectedItem();
                                AlarmNotification alarm = new AlarmNotification(
                                        number,
                                        period
                                );
                                listener.onPositiveButtonPressed(alarm);
                        }
                    }
                })
                .setNegativeButton(R.string.undone_pers, null);
        return builder.create();
    }

    public void addListener(final AlarmAlertDialogListener listener){
        this.listener = listener;
    }

    private class MySpinnerAdapter extends ArrayAdapter<AlarmNotification.TimePeriod>{

        private static final int LEFT_PADDING = 50;
        private static final int RIGHT_PADDING = 50;
        private static final int TOP_PADDING = 40;
        private static final int BOTTOM_PADDING = 40;
        private static final int TEXT_SIZE = 16;

        public MySpinnerAdapter(final Context context, final int resourceId, final AlarmNotification.TimePeriod[] objects) {
            super(context, resourceId, objects);
        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
            return getPersonalizedView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(final int position, final View convertView, final ViewGroup parent) {
            return getPersonalizedView(position, convertView, parent);
        }

        private View getPersonalizedView(final int position, final View convertView, final ViewGroup parent){
            LinearLayout view = new LinearLayout(getContext());
            TextView text = new TextView(getContext());
            String msg = getContext().getString(getItem(position).getIdMessagePlural()) + " " + getContext().getString(R.string.before);
            text.setText(msg);
            text.setTextColor(Color.BLACK);
            text.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
            ));
            text.setPadding(LEFT_PADDING,TOP_PADDING,RIGHT_PADDING,BOTTOM_PADDING);
            text.setTextSize(TypedValue.COMPLEX_UNIT_SP,TEXT_SIZE);
            view.addView(text);
            return view;
        }
    }
}
