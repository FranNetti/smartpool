package unibo.it.smartpool.database.relation_entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import unibo.it.smartpool.database.entity.Subscription;
import unibo.it.smartpool.database.entity.UpdatableTable;

@Entity(foreignKeys = @ForeignKey(
        entity = Subscription.class,
        parentColumns = "id",
        childColumns = "subReference",
        onDelete = ForeignKey.CASCADE
))
public class RelationFreeSwimSub extends UpdatableTable {

    private static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat(DATE_PATTERN);

    @PrimaryKey
    private int subReference;
    @ColumnInfo
    private String lastUpdate;

    public RelationFreeSwimSub(final int subReference, final String lastUpdate){
        this.subReference = subReference;
        this.lastUpdate = lastUpdate;
    }

    @Ignore
    public RelationFreeSwimSub(final int subReference){
        this.subReference = subReference;
        this.lastUpdate = FORMATTER.format(Calendar.getInstance().getTime());
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public int getSubReference() {
        return subReference;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public void setSubReference(int subReference) {
        this.subReference = subReference;
    }

    public boolean hasToUpdate(){
        return super.hasToUpdate(FORMATTER, lastUpdate);
    }
}
