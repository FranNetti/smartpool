package unibo.it.smartpool.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

@Entity
public class AquaFitness extends UpdatableTable implements Serializable {

    private static final String ID_FIELD = "id";
    private static final String ID_ATT_FIELD = "idCorsoAtt";
    private static final String NAME_FIELD = "nome";
    private static final String DESC_FIELD = "desc";
    private static final String INSTR_FIELD = "istruttori";
    private static final String INSTR_NAME = "nome";
    private static final String INSTR_SURNAME = "cognome";

    private static final String INSTR_DIV = ",";
    private static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat(DATE_PATTERN);

    @PrimaryKey
    private int id;
    @ColumnInfo
    private String name;
    @ColumnInfo(name = "desc")
    private String description;
    @ColumnInfo
    private int idActCourse;
    @ColumnInfo
    private String instrList;
    @ColumnInfo
    private String lastUpdate;

    @Ignore
    private List<Subscription> subscriptions;
    @Ignore
    private List<Lesson> lessonList;

    public AquaFitness(final int id, final String name, final String description, final int idActCourse, final String instrList, final String lastUpdate){
        this.id = id;
        this.name = name;
        this.description = description;
        this.idActCourse = idActCourse;
        this.instrList = instrList;
        this.lastUpdate = FORMATTER.format(Calendar.getInstance().getTime());
        this.subscriptions = new ArrayList<>();
        this.lessonList = new ArrayList<>();
        this.lastUpdate = lastUpdate;
    }

    public AquaFitness(final JSONObject obj) throws JSONException {
        this.id = obj.getInt(ID_FIELD);
        this.name = obj.getString(NAME_FIELD);
        this.description = obj.getString(DESC_FIELD);
        this.idActCourse = obj.getInt(ID_ATT_FIELD);
        StringBuilder builder = new StringBuilder();
        JSONArray instructor = obj.getJSONArray(INSTR_FIELD);
        for (int x = 0; x < instructor.length(); x++){
            JSONObject inst = instructor.getJSONObject(x);
            builder.append(inst.getString(INSTR_NAME));
            builder.append(" ");
            builder.append(inst.getString(INSTR_SURNAME));
            builder.append(INSTR_DIV);
        }
        builder.deleteCharAt(builder.length() - 1);
        this.instrList = builder.toString();
        this.lastUpdate = FORMATTER.format(Calendar.getInstance().getTime());
        this.subscriptions = new ArrayList<>();
        this.lessonList = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String desc) {
        this.description = desc;
    }

    public int getIdActCourse() {
        return idActCourse;
    }

    public void setIdActCourse(int idActCourse) {
        this.idActCourse = idActCourse;
    }

    public String getInstrList() {
        return instrList;
    }

    public void setInstrList(String instrList) {
        this.instrList = instrList;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions.clear();
        this.subscriptions.addAll(subscriptions);
    }


    public List<Lesson> getLessons() {
        return lessonList;
    }

    public void setLessons(List<Lesson> lessonList) {
        this.lessonList.clear();
        this.lessonList.addAll(lessonList);
    }

    public boolean hasToUpdate() {
        return super.hasToUpdate(FORMATTER, lastUpdate);
    }

    public boolean update(final AquaFitness fitness){
        boolean update = false;
        this.lastUpdate = FORMATTER.format(Calendar.getInstance().getTime());
        if(!this.instrList.equals(fitness.instrList) || !this.name.equals(fitness.name) || !this.description.equals(fitness.description)
                || this.idActCourse!=fitness.idActCourse){
            this.instrList = fitness.instrList;
            this.name = fitness.name;
            this.description = fitness.description;
            this.idActCourse = fitness.idActCourse;
            update = true;
        }
        return update;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AquaFitness fitness = (AquaFitness) o;
        return id == fitness.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
