package unibo.it.smartpool.utils;

import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

public class ParallelExecutor implements Executor {
    @Override
    public void execute(@NonNull Runnable command) {
        new Thread(command).start();
    }
}
