package unibo.it.smartpool.database.entity;

import android.content.Context;
import java.util.List;

import unibo.it.smartpool.R;

public class FreeSwim {

    private final String name;
    private List<Subscription> subscriptions;

    public FreeSwim(final Context context, final List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
        this.name = context.getString(R.string.free_swim);
    }

    public String getName() {
        return name;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }
}