package unibo.it.smartpool.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.connection.HttpException;
import unibo.it.smartpool.database.entity.Subscription;
import unibo.it.smartpool.database.dao.AbbonamentoDAO;
import unibo.it.smartpool.database.AppDatabase;
import unibo.it.smartpool.database.entity.FreeSwim;
import unibo.it.smartpool.database.dao.RelationDAO;
import unibo.it.smartpool.database.relation_entity.RelationFreeSwimSub;

public class FreeSwimRepository {

    private static final String TAG = "free_swim_repo_tag";

    private final Context context;
    private final RelationDAO relationTable;
    private final AbbonamentoDAO subscriptionTable;
    private MutableLiveData<FreeSwim> swimCourse = new MutableLiveData<>();

    public FreeSwimRepository(final Context context){
        this.context = context;
        this.relationTable = AppDatabase.getDatabase(context).relationDAO();
        this.subscriptionTable = AppDatabase.getDatabase(context).abbonamentoDAO();
        new FreeSwimCheckUpdate().execute();
    }

    public LiveData<FreeSwim> getFreeSwimCourse() {
        new FreeSwimCheckUpdate().execute();
        return swimCourse;
    }

    private class FreeSwimCheckUpdate extends AsyncTask<Void,Void, FreeSwim> {
        @Override
        protected FreeSwim doInBackground(Void... voids) {
            RelationFreeSwimSub oldRel = relationTable.getLeastUpdateRelationFreeSwimSub();
            if(oldRel == null || oldRel.hasToUpdate()){
                relationTable.deleteAllFreeSwimSubscriptions();
                try{
                    final String url = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.FREE_PAGE;
                    String data = ConnectionUtilities.getDataFromUrl(url,null,false);
                    if(data != null && !data.isEmpty()){
                        boolean subUpdate = false;
                        List<Subscription> oldSubscriptions = subscriptionTable.getAll();
                        List<Subscription> newSubscriptions = new ArrayList<>();
                        List<RelationFreeSwimSub> freeSwimSubs = new ArrayList<>();

                        /* update/create subscriptions */
                        JSONArray subscriptions = new JSONObject(data).getJSONArray("fare");
                        List<Subscription> subList = new ArrayList<>();
                        for (int x = 0; x < subscriptions.length(); x++){
                            Subscription abb = new Subscription(subscriptions.getJSONObject(x));
                            if(oldSubscriptions.contains(abb)){
                                subUpdate |= oldSubscriptions.get(oldSubscriptions.indexOf(abb)).update(abb);
                            } else if(!newSubscriptions.contains(abb)){
                                newSubscriptions.add(abb);
                            }
                            freeSwimSubs.add(new RelationFreeSwimSub(abb.getId()));
                            subList.add(abb);
                        }

                        if(subUpdate){
                            subscriptionTable.updateSubscriptions(oldSubscriptions);
                        }
                        subscriptionTable.addSubscriptions(newSubscriptions);
                        relationTable.addFreeSwimSubRelations(freeSwimSubs);

                        return new FreeSwim(context, subList);
                    }
                } catch(HttpException e){
                    Log.e(TAG,"HttpException in FreeSwimCheckUpdate", e);
                } catch(JSONException e){
                    Log.e(TAG,"JsonException in FreeSwimCheckUpdate", e);
                }
            } else {
                return new FreeSwim(context, relationTable.getAllFreeSwimSubscriptions());
            }
            return null;
        }

        @Override
        protected void onPostExecute(FreeSwim course) {
            super.onPostExecute(course);
            if(course != null){
                swimCourse.setValue(course);
            }
        }

    }
}
