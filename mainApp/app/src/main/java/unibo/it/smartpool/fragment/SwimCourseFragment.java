package unibo.it.smartpool.fragment;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import unibo.it.smartpool.InformationManager;
import unibo.it.smartpool.R;
import unibo.it.smartpool.activity.MainActivity;
import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.connection.HttpException;
import unibo.it.smartpool.database.entity.AquaFitness;
import unibo.it.smartpool.database.entity.Subscription;
import unibo.it.smartpool.database.entity.SwimCourse;
import unibo.it.smartpool.utils.ParallelExecutor;
import unibo.it.smartpool.view_elements.adapter.SwimCourseAdapter;

public class SwimCourseFragment  extends android.support.v4.app.Fragment implements SwimCourseAdapter.SwimCurseAdapterListener{

    private InformationManager manager;
    private String userEmail;
    private static final String TAG = "Swim_Course_tag";
    private Context context;
    private SwimCourseAdapter courseAdapter;

    public static SwimCourseFragment newInstance() {
            return new SwimCourseFragment();
            }

    public SwimCourseFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.manager = ViewModelProviders.of(getActivity()).get(InformationManager.class);
        new ParallelExecutor().execute(new Runnable() {
            @Override
            public void run() {
                userEmail = manager.getUser().getEmail();
            }
        });
        this.context = getContext();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_swim_class, container, false);
        final ListView layout = (ListView) view.findViewById(R.id.listView1);
        final LiveData<List<SwimCourse>> courseList = this.manager.getSwimCourseList();
        final List<SwimCourse> list = new ArrayList<>();
        this.courseAdapter = new SwimCourseAdapter(
                getActivity(),
                android.R.layout.simple_list_item_1,
                this,
                list);
        layout.setAdapter(courseAdapter);

        final MainActivity main = (MainActivity)getActivity();

        courseList.observe(this, new Observer<List<SwimCourse>>() {
            @Override
            public void onChanged(@Nullable List<SwimCourse> course) {
                if (course != null) {
                    if(course.isEmpty()){
                        main.noDataAvailable();
                    } else {
                        list.clear();
                        list.addAll(course);
                        courseAdapter.notifyDataSetChanged();
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!ConnectionUtilities.isConnectionAvailable(getContext())){
            Toast.makeText(getContext(), getString(R.string.warning_text), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSubscriptionSelected(SwimCourse item,Subscription sub) {
        new AddSubscription(item, sub).execute();
    }

    private class AddSubscription extends AsyncTask<Void, Void, Integer> {
        private SwimCourse item;
        private Subscription sub;

        public AddSubscription(SwimCourse item,Subscription sub){
            this.item = item;
            this.sub = sub;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            final Map<String, String> map = ConnectionUtilities.getSwimCourseAddSubscriptionMap(
                    String.valueOf(this.item.getId()),
                    String.valueOf(this.item.getIdActCourse()),
                    userEmail,
                    String.valueOf(this.sub.getId()),
                    this.sub.getType(),
                    this.sub.getPrice()
            );

            final String url = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.ADD_SWIM_RECORD;
            try {
                if (ConnectionUtilities.isConnectionAvailable(context)) {
                    ConnectionUtilities.getDataFromUrl(url, map, true);
                    manager.updateSubscriptions();
                    manager.updateCard();
                    return HttpURLConnection.HTTP_OK;
                } else {
                    return HttpURLConnection.HTTP_GONE;
                }
            }catch (HttpException e){
                Log.e(TAG, "Error in the account balance, HttpCode: " + e.getCode(), e);
                return e.getCode();
            }
        }

        @Override
        protected void onPostExecute(final Integer success) {
            switch (success){
                case HttpURLConnection.HTTP_OK:
                    Toast.makeText(context,R.string.sub_add,Toast.LENGTH_LONG).show();
                    break;
                case HttpURLConnection.HTTP_GONE:
                    ConnectionUtilities.alertConnectionAbsence(context);
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    new AlertDialog.Builder(context)
                            .setMessage(R.string.balance)
                            .setPositiveButton("OK", null)
                            .show();
                    break;
                default:
                    ConnectionUtilities.alertConnectionError(context);
                    break;
            }
        }

    }
}
