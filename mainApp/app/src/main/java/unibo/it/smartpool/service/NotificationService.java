package unibo.it.smartpool.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

import unibo.it.smartpool.R;
import unibo.it.smartpool.database.dao.AlarmNotDAO;
import unibo.it.smartpool.database.AppDatabase;
import unibo.it.smartpool.database.dao.NotificationDAO;
import unibo.it.smartpool.database.entity.NotificationMsg;
import unibo.it.smartpool.database.dao.SubAlarmNotificationDAO;

/**
 * An IntentService that handles the notifications of expired fares or subscriptions.
 */
public class NotificationService extends IntentService {

    private static final String CHANNEL_ID =    "notification_ch";
    private static final String  GROUP_NOT = "notification_group";

    public NotificationService() {
        super("NotificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        createNotificationChannel();
        NotificationDAO notTable = AppDatabase.getDatabase(this).notificationDAO();
        AlarmNotDAO cardAlarmTable = AppDatabase.getDatabase(this).alarmNotDAO();
        SubAlarmNotificationDAO subAlarmTable = AppDatabase.getDatabase(this).subAlarmNotificationDAO();
        Calendar calendar = Calendar.getInstance();
        String currentDay = NotificationMsg.FORMATTER.format(calendar.getTime());
        List<NotificationMsg> notificationList = notTable.getAllNotificationOfDay(currentDay);
        if(!notificationList.isEmpty()){
            int id = new Random().nextInt();
            for (NotificationMsg notification : notificationList){
                String title;
                switch(notification.getType()){
                    case NotificationMsg.CARD_TYPE:
                        title = getString(R.string.not_card_title);
                        cardAlarmTable.updateAlarmStatus(notification.getAlarmRef(), false);
                        break;
                    case NotificationMsg.SUB_TYPE:
                        title = getString(R.string.not_sub_title);
                        subAlarmTable.updateAlarmStatus(notification.getAlarmRef(), false);
                        break;
                    default:
                        title = getString(R.string.not_default_title);
                        break;
                }
                createNotification(title, notification.getMsg(), null, ++id);
            }
            notTable.deleteAll(notificationList);
        }
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.not_channel_name);
            String description = getString(R.string.not_channel_desc);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void createNotification(final String title, final String msg, final PendingIntent tapPendingIntent, final int notificationId){
        NotificationCompat.Builder build = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(msg)
                .setStyle(new NotificationCompat.BigTextStyle()
                                                 .bigText(msg)
                )
                .setSmallIcon(R.mipmap.ic_smart_pool_f)
                .setAutoCancel(true)
                .setGroup(GROUP_NOT)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setCategory(NotificationCompat.CATEGORY_REMINDER)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        if(tapPendingIntent != null){
            build = build.setContentIntent(tapPendingIntent);
        }
        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        manager.notify(notificationId, build.build());
    }
}
