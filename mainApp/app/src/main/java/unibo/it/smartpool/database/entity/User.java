package unibo.it.smartpool.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * Class for the user data
 */
@Entity
public class User extends UpdatableTable implements Serializable {

    private static final String CF_FIELD = "cf";
    private static final String NAME_FIELD = "nome";
    private static final String SURNAME_FIELD = "cognome";
    private static final String SEX_FIELD = "sesso";
    private static final String ADDR_FIELD = "indirizzo";
    private static final String NUM_FIELD = "numTelefono";
    private static final String BIRTH_FIELD = "dataNascita";
    public static final String EMAIL_FIELD = "email";
    public static final String PWD_FIELD = "pwd";

    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private static final String MALE_PATTERN = "M";

    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat(DATE_PATTERN);

    @PrimaryKey @NonNull
    private String cf;
    @ColumnInfo
    private String email;
    @ColumnInfo
    private String pwd;
    @ColumnInfo
    private String name;
    @ColumnInfo
    private String surname;
    @ColumnInfo
    private boolean male;
    @ColumnInfo(name = "birthDate")
    private String birth;
    @ColumnInfo
    private String address;
    @ColumnInfo
    private String number;
    @ColumnInfo
    private String lastUpdate;

    @Ignore
    private Date birthDate;

    /**
     * Constructor.
     * @param name user's name
     * @param surname user's surname
     * @param male if the user is a male
     * @param address user's address
     * @param number user's telephone number
     * @param birth user's birth date
     * @param email user's email
     * @param pwd user's password
     * @param lastUpdate last time the user was updated
     */
    public User(final String cf, final String name, final String surname, final boolean male, final String address, final String number,
                final String birth, final String email, final String pwd, final String lastUpdate) {
        this.cf = cf;
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.number = number;
        this.birth = birth;
        try{
            this.birthDate = FORMATTER.parse(birth);
        } catch(ParseException e){
            this.birthDate = Calendar.getInstance().getTime();
        }
        this.email = email;
        this.pwd = pwd;
        this.male = male;
        this.lastUpdate = lastUpdate;
    }

    /**
     * Constructor.
     * @param user the user's data in a json format
     * @throws JSONException if there is an error while getting data
     */
    public User(final JSONObject user) throws JSONException {
        this.cf = user.getString(CF_FIELD);
        this.name = user.getString(NAME_FIELD);
        this.surname = user.getString(SURNAME_FIELD);
        this.address = user.getString(ADDR_FIELD);
        this.number = user.getString(NUM_FIELD);
        try {
            this.birth = user.getString(BIRTH_FIELD);
            this.birthDate = FORMATTER.parse(birth);
        } catch (ParseException e){
            this.birthDate = Calendar.getInstance().getTime();
        }
        this.email = user.getString(EMAIL_FIELD);
        this.pwd = user.getString(PWD_FIELD);
        this.male = user.getString(SEX_FIELD).equals(MALE_PATTERN);
        this.lastUpdate = FORMATTER.format(Calendar.getInstance().getTime());
    }

    public User(final User user){
        this.cf = user.cf;
        this.name = user.name;
        this.surname = user.surname;
        this.address = user.address;
        this.number = user.number;
        this.birth = user.birth;
        this.birthDate = user.birthDate;
        this.email = user.email;
        this.pwd = user.pwd;
        this.male = user.male;
        this.lastUpdate = FORMATTER.format(Calendar.getInstance().getTime());
    }

    @NonNull
    public String getCf() {
        return cf;
    }

    public void setCf(@NonNull String cf) {
        this.cf = cf;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public boolean isMale() {
        return male;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
        try {
            this.birthDate = FORMATTER.parse(birth);
        } catch (ParseException e) {
           this.birthDate = Calendar.getInstance().getTime();
        }
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        this.birth = FORMATTER.format(birthDate);
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Date getFormatLastUpdate(){
        try {
            return FORMATTER.parse(this.lastUpdate);
        } catch (ParseException e){
            return Calendar.getInstance().getTime();
        }
    }

    public void setFormatLastUpdate(final Date date){
        this.lastUpdate = FORMATTER.format(date);
    }

    public String getFullName(){
        return this.name + " " + this.surname;
    }

    public boolean hasToUpdate(){
        return super.hasToUpdate(FORMATTER, this.lastUpdate);
    }

    public boolean isUpdated(final User user){
        if(this.cf.equals(user.cf)) {
            return !this.address.equals(user.address) || !this.number.equals(user.number) || !this.email.equals(user.email) || !this.pwd.equals(user.pwd);
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(cf, user.cf) && Objects.equals(lastUpdate, user.lastUpdate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(cf);
    }
}
