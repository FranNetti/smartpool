package unibo.it.smartpool.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.connection.HttpException;
import unibo.it.smartpool.database.entity.Subscription;

import unibo.it.smartpool.database.dao.AbbonamentoDAO;
import unibo.it.smartpool.database.AppDatabase;
import unibo.it.smartpool.database.dao.LessonDAO;
import unibo.it.smartpool.database.relation_entity.RelationCourseLess;
import unibo.it.smartpool.database.relation_entity.RelationCourseSub;
import unibo.it.smartpool.database.dao.RelationDAO;
import unibo.it.smartpool.database.dao.SwimCourseDAO;
import unibo.it.smartpool.database.entity.Lesson;
import unibo.it.smartpool.database.entity.SwimCourse;

public class SwimCourseRepository {

    private static final String TAG = "swim_course_repo_tag";

    private SwimCourseDAO swimTable;
    private RelationDAO relationTable;
    private AbbonamentoDAO subscriptionTable;
    private LessonDAO lessonTable;

    private MutableLiveData<List<SwimCourse>> swimCourseList = new MutableLiveData<>();

    public SwimCourseRepository(final Context context){
        this.swimTable = AppDatabase.getDatabase(context).swimCourseDAO();
        this.relationTable = AppDatabase.getDatabase(context).relationDAO();
        this.subscriptionTable = AppDatabase.getDatabase(context).abbonamentoDAO();
        this.lessonTable = AppDatabase.getDatabase(context).lessonDAO();
    }

    public LiveData<List<SwimCourse>> getSwimCourseList() {
        new SwimCourseCheckUpdate().execute();
        return swimCourseList;
    }

    private class SwimCourseCheckUpdate extends AsyncTask<Void, Void, List<SwimCourse>> {
        @Override
        protected List<SwimCourse> doInBackground(Void... voids) {
            SwimCourse old = swimTable.getLeastUpdatedSwimCourse();
            if(old == null || old.hasToUpdate()){
                boolean subUpdate = false;
                boolean lessonUpdate = false;
                List<SwimCourse> oldSwimList = swimTable.getAllSwimCourses();
                List<Subscription> oldSubscriptions = subscriptionTable.getAll();
                List<Lesson> oldLessons = lessonTable.getAll();

                final String url = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.SWIMCOURSE_PAGE;
                try{
                    String data = ConnectionUtilities.getDataFromUrl(url,null,false);
                    if(data != null && !data.isEmpty()){
                        List<RelationCourseSub> swimAbb = new ArrayList<>();
                        List<RelationCourseLess> swimLess = new ArrayList<>();
                        List<SwimCourse> newSwimList = new ArrayList<>();
                        List<Subscription> newSubscriptionList = new ArrayList<>();
                        List<Lesson> newLessonList = new ArrayList<>();
                        JSONArray array = new JSONArray(data);
                        for(int i = 0;i < array.length();i++){
                            JSONObject obj = array.getJSONObject(i);

                            /* update/create swim course obj */
                            SwimCourse swim = new SwimCourse(obj);
                            if(oldSwimList.contains(swim)){
                                swim = oldSwimList.get(oldSwimList.indexOf(swim));
                                swim.update(swim);
                                relationTable.deleteAllLessonsOfSwimCourse(swim.getId());
                                relationTable.deleteAllSubscriptionsOfSwimCourse(swim.getId());
                            } else {
                                newSwimList.add(swim);
                            }

                            /* update/create subscriptions */
                            JSONArray subscriptions = obj.getJSONArray("fare");
                            List<Subscription> subList = new ArrayList<>();
                            for (int x = 0; x < subscriptions.length(); x++){
                                Subscription abb = new Subscription(subscriptions.getJSONObject(x));
                                if(oldSubscriptions.contains(abb)){
                                    subUpdate |= oldSubscriptions.get(oldSubscriptions.indexOf(abb)).update(abb);
                                } else if(!newSubscriptionList.contains(abb)){
                                    newSubscriptionList.add(abb);
                                }
                                swimAbb.add(new RelationCourseSub(swim.getId(), abb.getId()));
                                subList.add(abb);
                            }
                            swim.setSubscriptions(subList);

                            /* create lessons */
                            JSONArray lessons = obj.getJSONArray("lezioni");
                            List<Lesson> lessList = new ArrayList<>();
                            for (int x = 0; x < lessons.length(); x++){
                                Lesson less = new Lesson(lessons.getJSONObject(x), swim.getPool());
                                if(oldLessons.contains(less)){
                                    lessonUpdate |= oldLessons.get(oldLessons.indexOf(less)).update(less);
                                } else if(!newLessonList.contains(less)){
                                    newLessonList.add(less);
                                }
                                swimLess.add(new RelationCourseLess(swim.getId(), less));
                                lessList.add(less);
                            }
                            swim.setLessons(lessList);

                        }

                        swimTable.updateSwimCourses(oldSwimList);
                        swimTable.addSwimCourses(newSwimList);

                        if(subUpdate){
                            subscriptionTable.updateSubscriptions(oldSubscriptions);
                        }
                        subscriptionTable.addSubscriptions(newSubscriptionList);
                        relationTable.addCourseSubRelations(swimAbb);

                        if(lessonUpdate){
                            lessonTable.updateLessons(oldLessons);
                        }
                        lessonTable.addLessons(newLessonList);
                        relationTable.addCourseLessRelations(swimLess);

                        oldSwimList.addAll(newSwimList);
                        return oldSwimList;
                    }
                } catch(HttpException e){
                    Log.e(TAG,"HttpException in AquaFitnessCheckUpdate", e);
                } catch(JSONException e){
                    Log.e(TAG,"JSONException in AquaFitnessCheckUpdate", e);
                }

            } else {
                List<SwimCourse> swimList = swimTable.getAllSwimCourses();
                for(SwimCourse swim : swimList) {
                    swim.setLessons(relationTable.getAllLessonsOfSwimCourse(swim.getId()));
                    swim.setSubscriptions(relationTable.getAllSubscriptionsOfSwimCourse(swim.getId()));
                }
                return swimList;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<SwimCourse> course) {
            if(course != null){
                swimCourseList.setValue(course);
            }
        }

    }
}
