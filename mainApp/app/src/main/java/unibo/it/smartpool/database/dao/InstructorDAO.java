package unibo.it.smartpool.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import unibo.it.smartpool.database.entity.Instructor;

@Dao
public interface InstructorDAO {

    @Query("SELECT * FROM instructor")
    LiveData<List<Instructor>> getAllInstructors();

    @Query("SELECT * FROM instructor WHERE lastUpdate <= (SELECT MIN(lastUpdate) from instructor) LIMIT 1")
    Instructor getLeastUpdatedInstructor();

    @Insert
    void addAll(List<Instructor> instructors);

    @Update
    void updateAll(List<Instructor> instructors);

}
