package unibo.it.smartpool.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import unibo.it.smartpool.database.entity.Subscription;
import unibo.it.smartpool.database.entity.Lesson;
import unibo.it.smartpool.database.relation_entity.RelationCourseLess;
import unibo.it.smartpool.database.relation_entity.RelationCourseSub;
import unibo.it.smartpool.database.relation_entity.RelationFitnLess;
import unibo.it.smartpool.database.relation_entity.RelationFitnSub;
import unibo.it.smartpool.database.relation_entity.RelationFreeSwimSub;

@Dao
public interface RelationDAO {

    @Insert
    void addFitnLessRelations(final List<RelationFitnLess> relations);

    @Insert
    void addFitnSubRelations(final List<RelationFitnSub> relations);

    @Insert
    void addCourseLessRelations(final List<RelationCourseLess> relations);

    @Insert
    void addCourseSubRelations(final List<RelationCourseSub> relations);

    @Insert
    void addFreeSwimSubRelations(List<RelationFreeSwimSub> relations);

    @Query("DELETE FROM relationfitnsub WHERE fitnessRef = :fitnessReference")
    void deleteAllSubscriptionsOfAquaFitness(final int fitnessReference);

    @Query("DELETE FROM relationfitnless WHERE fitnessRef = :fitnessReference")
    void deleteAllLessonsOfAquaFitness(final int fitnessReference);

    @Query("DELETE FROM relationcoursesub WHERE courseRef = :courseRef")
    void deleteAllSubscriptionsOfSwimCourse(final int courseRef);

    @Query("DELETE FROM relationcourseless WHERE courseRef = :courseRef")
    void deleteAllLessonsOfSwimCourse(final int courseRef);

    @Query("DELETE FROM relationfreeswimsub")
    void deleteAllFreeSwimSubscriptions();

    @Query("SELECT les.* FROM lesson as les, relationfitnless rel WHERE rel.fitnessRef = :fitnessReference AND les.day = rel.day AND les.pool LIKE rel.pool AND les.time LIKE rel.time")
    List<Lesson> getAllLessonsOfAquaFitness(final int fitnessReference);

    @Query("SELECT sub.* FROM Subscription as sub, relationfitnsub rel WHERE rel.fitnessRef = :fitnessReference AND sub.id = rel.subscriptionRef")
    List<Subscription> getAllSubscriptionsOfAquaFitness(final int fitnessReference);

    @Query("SELECT les.* FROM lesson as les, relationcourseless rel WHERE rel.courseRef = :courseRef AND les.day = rel.day AND les.pool LIKE rel.pool AND les.time LIKE rel.time")
    List<Lesson> getAllLessonsOfSwimCourse(final int courseRef);

    @Query("SELECT sub.* FROM Subscription as sub, relationcoursesub rel WHERE rel.courseRef = :courseRef AND sub.id = rel.subscriptionRef")
    List<Subscription> getAllSubscriptionsOfSwimCourse(final int courseRef);

    @Query("SELECT sub.* FROM Subscription as sub, relationfreeswimsub rel WHERE rel.subReference LIKE sub.id")
    List<Subscription> getAllFreeSwimSubscriptions();

    @Query("SELECT * from relationfreeswimsub WHERE lastUpdate <= (SELECT MIN(lastUpdate) FROM relationfreeswimsub) LIMIT 1")
    RelationFreeSwimSub getLeastUpdateRelationFreeSwimSub();

}
