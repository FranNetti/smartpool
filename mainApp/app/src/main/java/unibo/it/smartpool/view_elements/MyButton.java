package unibo.it.smartpool.view_elements;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.view.View;
import android.widget.Toast;

import unibo.it.smartpool.R;

public class MyButton extends AppCompatImageButton {

    public MyButton(Context context ){
        super(context);
        this.setImageResource(android.R.drawable.ic_input_add);
        this.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        this.setColorFilter(getResources().getColor(android.R.color.white));
    }

}
