package unibo.it.smartpool.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import unibo.it.smartpool.InformationManager;
import unibo.it.smartpool.R;
import unibo.it.smartpool.activity.AlarmChooserActivity;
import unibo.it.smartpool.database.entity.AlarmNotification;
import unibo.it.smartpool.database.AppDatabase;
import unibo.it.smartpool.database.entity.SubAlarmNotification;
import unibo.it.smartpool.database.entity.UserSubscription;
import unibo.it.smartpool.view_elements.adapter.ClockAdapter;

public class SubscriptionNotificationFragment extends AlarmDealerFragment{

    private static final String USER_SUB = "user_sub_fr";

    public static SubscriptionNotificationFragment newInstance(final UserSubscription sub) {
        SubscriptionNotificationFragment fr = new SubscriptionNotificationFragment();
        Bundle args = new Bundle();
        args.putSerializable(USER_SUB, sub);
        fr.setArguments(args);
        return fr;
    }

    private UserSubscription sub;

    private Date expirationDate;

    public SubscriptionNotificationFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.sub = (UserSubscription) getArguments().getSerializable(USER_SUB);
            this.expirationDate = sub.getExpiration();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_subscription_notification, container, false);
        final InformationManager manager = ViewModelProviders.of(getActivity()).get(InformationManager.class);

        this.clock = view.findViewById(R.id.clock_list);
        final TextView subTitle = view.findViewById(R.id.sub_title);
        final TextView subType = view.findViewById(R.id.type);
        final TextView subStart = view.findViewById(R.id.start);
        final TextView subExpiration = view.findViewById(R.id.expiration);
        final TextView add = view.findViewById(R.id.clock_add);
        this.save = view.findViewById(R.id.sub_save);
        this.noAlarm = view.findViewById(R.id.no_alarm);

        /* subscription info setup */
        String type;
        String startText;
        String expirationText;
        subTitle.setText(sub.getReference());
        switch (sub.getSubscriptionType()){
            case MONTHLY:
                SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
                startText = format.format(sub.getStart());
                expirationText = format.format(sub.getExpiration());
                type = getContext().getString(R.string.subscription_type_month);
                break;
            default:
                type = "";
                startText = "";
                expirationText = "";
                break;
        }
        subType.setText(sub.getSubscriptionType().toString());
        subExpiration.setText(expirationText);
        subStart.setText(startText);
        subType.setText(type);

        /* button add setup */
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),AlarmChooserActivity.class);
                getActivity().startActivityForResult(intent, AlarmChooserActivity.ALARM_ACTIVITY_INTENT_CODE);
            }
        });

        /* list view setup */
        this.adp = new ClockAdapter(
                getContext(),
                android.R.layout.simple_list_item_1,
                alarmList
        );
        this.clock.setAdapter(adp);
        adp.setUpdateClockListener(this);

        manager.getAllAlarmsOf(sub).observe(this, new Observer<List<SubAlarmNotification>>() {
            @Override
            public void onChanged(@Nullable List<SubAlarmNotification> subscriptions) {
                if(subscriptions != null){
                    if(subscriptions.isEmpty()){
                        noAlarm.setVisibility(View.VISIBLE);
                        clock.setVisibility(View.GONE);
                    } else {
                        alarmList.clear();
                        alarmList.addAll(subscriptions);
                        updateAdpHeight();
                    }
                }
            }
        });

        /* button save setup */
        this.changeButtonStatus(this.save, false);
        this.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageAlarms(manager);
                Toast.makeText(getContext(), R.string.change_succ, Toast.LENGTH_SHORT).show();
                resetStatus();
            }
        });

        return view;
    }

    @Override
    public void onAlarmCreation(final Intent data){
        AlarmNotification newAlarm = (AlarmNotification) data.getSerializableExtra(AlarmChooserActivity.ALARM_NOT);
        if(newAlarm != null) {
            data.putExtra(AlarmChooserActivity.ALARM_NOT, new SubAlarmNotification(newAlarm, sub.getId()));
        }
        super.onAlarmCreation(data);
    }

    @Override
    public void onStateChanged(AlarmNotification alarm, boolean on) {
        alarm = new SubAlarmNotification(alarm, sub.getId());
        super.onStateChanged(alarm, on);
    }

    private void resetStatus(){
        changeButtonStatus(save, false);
        addAlarm.clear();
        updateAlarm.clear();
        deleteAlarm.clear();
    }

    private void manageAlarms(final InformationManager manager){
        if (!deleteAlarm.isEmpty()) {
            manager.manageSubscriptionAlarmOperation(AppDatabase.DbOperation.REMOVE, deleteAlarm, expirationDate);
        }
        if (!updateAlarm.isEmpty()) {
            manager.manageSubscriptionAlarmOperation(AppDatabase.DbOperation.UPDATE, updateAlarm, expirationDate);
        }
        if (!addAlarm.isEmpty()) {
            manager.manageSubscriptionAlarmOperation(AppDatabase.DbOperation.ADD, addAlarm,expirationDate);
        }
    }
}

