package unibo.it.smartpool.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import unibo.it.smartpool.InformationManager;
import unibo.it.smartpool.R;
import unibo.it.smartpool.activity.MainActivity;
import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.database.entity.Instructor;
import unibo.it.smartpool.database.entity.User;
import unibo.it.smartpool.utils.ParallelExecutor;
import unibo.it.smartpool.view_elements.adapter.InstructorAdapter;

public class InstructorsFragment extends Fragment implements InstructorAdapter.InstructorAdapterListener {

    public static InstructorsFragment newInstance() {
        return new InstructorsFragment();
    }

    public InstructorsFragment() {
    }

    private InformationManager manager;
    private InstructorAdapter adp;
    private User user;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager = ViewModelProviders.of(getActivity()).get(InformationManager.class);
        ParallelExecutor executor = new ParallelExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                user = manager.getUser();
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_instructor, container, false);
        final ListView listView = view.findViewById(R.id.instructor_list);
        final TextView emptyList = view.findViewById(R.id.empty_text);
        emptyList.setVisibility(View.GONE);

        final MainActivity main = (MainActivity)getActivity();

        final List<Instructor> instructorList = new ArrayList<>();
        this.manager.getInstructorList().observe(this, new Observer<List<Instructor>>() {
            @Override
            public void onChanged(@Nullable List<Instructor> instructors) {
                if(instructors != null){
                    if(instructors.isEmpty()){
                        main.noDataAvailable();
                    } else {
                        instructorList.clear();
                        instructorList.addAll(instructors);
                        adp.notifyDataSetChanged();
                    }
                }
            }
        });

        /* clock adapter and listview setup */
        this.adp = new InstructorAdapter(
                getContext(),
                android.R.layout.simple_list_item_1,
                instructorList,
                user.getEmail(),
                this
        );
        listView.setAdapter(adp);

       return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!ConnectionUtilities.isConnectionAvailable(getContext())){
            Toast.makeText(getContext(), getString(R.string.warning_text), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onAddressPressed(final String addr) {
        //query got from google apis
        String addressEncoded = "0,0?q=" + addr.replaceAll(" ", "+");
        Uri address = Uri.parse("geo:" + addressEncoded);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, address);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    @Override
    public void onInstructorSelected(Instructor instr) {
        if(user != null){
            manager.addFavourite(instr, user.getEmail());
            Toast.makeText(getContext(), getString(R.string.add_to_favorite), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onInstructorDeselected(Instructor instr) {
        if(user != null){
            manager.deleteFavourite(instr, user.getEmail());
            Toast.makeText(getContext(), getString(R.string.not_favorite), Toast.LENGTH_SHORT).show();
        }
    }
}

