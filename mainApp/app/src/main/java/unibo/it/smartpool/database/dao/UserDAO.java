package unibo.it.smartpool.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import unibo.it.smartpool.database.entity.User;

@Dao
public interface UserDAO {

    @Query("SELECT * FROM user WHERE email LIKE :email")
    LiveData<User> getObservableUser(final String email);

    @Query("SELECT * FROM user WHERE email LIKE :email")
    User getUser(final String email);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addUser(final User user);

    @Update
    void updateUser(final User user);
}
