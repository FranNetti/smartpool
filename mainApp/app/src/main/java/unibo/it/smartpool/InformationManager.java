package unibo.it.smartpool;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import unibo.it.smartpool.database.entity.AlarmNotification;
import unibo.it.smartpool.database.AppDatabase;
import unibo.it.smartpool.database.entity.AquaFitness;
import unibo.it.smartpool.database.entity.Card;
import unibo.it.smartpool.database.entity.FreeSwim;
import unibo.it.smartpool.database.entity.Instructor;
import unibo.it.smartpool.database.entity.NotificationMsg;
import unibo.it.smartpool.database.entity.OpeningTime;
import unibo.it.smartpool.database.entity.SubAlarmNotification;
import unibo.it.smartpool.database.entity.SwimCourse;
import unibo.it.smartpool.database.entity.User;
import unibo.it.smartpool.database.entity.UserSubscription;
import unibo.it.smartpool.repository.AlarmRepository;
import unibo.it.smartpool.repository.AquaFitnessRepository;
import unibo.it.smartpool.repository.FreeSwimRepository;
import unibo.it.smartpool.repository.InstructorRepository;
import unibo.it.smartpool.repository.OpeningTimesRepository;
import unibo.it.smartpool.repository.SwimCourseRepository;
import unibo.it.smartpool.repository.UserRepository;
import unibo.it.smartpool.service.NotificationService;
import unibo.it.smartpool.utils.ParallelExecutor;

/**
 * Class that contains every information used in the app
 */
public class InformationManager extends AndroidViewModel{

    private static final String SHARED_PREFERENCES_NAME = "info_sp";
    private static final String ALARM_ENABLE = "info_alarm_not";

    private Context context;
    private AlarmRepository alarmRepo;
    private UserRepository userRepo;
    private OpeningTimesRepository opRepo;
    private InstructorRepository insRepo;
    private AquaFitnessRepository aqftnRepo;
    private SwimCourseRepository swmRepo;
    private FreeSwimRepository freeSwimRepo;

    /**
     * Constructor.
     * @param application
     */
    public InformationManager(final Application application){
        super(application);
        this.context = application;
        this.alarmRepo = new AlarmRepository(application);
        this.userRepo = new UserRepository(application, alarmRepo);
        this.opRepo = new OpeningTimesRepository(application);
        this.insRepo = new InstructorRepository(application);
        this.aqftnRepo = new AquaFitnessRepository(application);
        this.swmRepo = new SwimCourseRepository(application);
        this.freeSwimRepo = new FreeSwimRepository(application);
        startAlarmNotification();
    }

    public LiveData<List<AlarmNotification>> getAllCardAlarms(){
        return this.alarmRepo.getAllCardAlarms();
    }

    public LiveData<List<Instructor>> getInstructorList(){
        return this.insRepo.getInstructorList();
    }

    public LiveData<List<Instructor>> getAllFavouritesOf(final String user){
        return this.insRepo.getFavouriteList(user);
    }

    public LiveData<List<AquaFitness>> getAquaFitnessList(){
        return this.aqftnRepo.getCourseList();
    }

    public LiveData<List<SwimCourse>> getSwimCourseList(){
        return this.swmRepo.getSwimCourseList();
    }

    public LiveData<FreeSwim> getFreeSwimCourse(){
        return this.freeSwimRepo.getFreeSwimCourse();
    }

    public LiveData<List<UserSubscription>> getAllSubscription(){
        return this.userRepo.getUserSubscriptions();
    }

    public LiveData<List<SubAlarmNotification>> getAllAlarmsOf(final UserSubscription sub){
        return this.alarmRepo.getSubAlarmList(sub);
    }

    public LiveData<User> getObservableUser(){
        return this.userRepo.getObservableUser();
    }

    public User getUser() {
        return this.userRepo.getUser();
    }

    public LiveData<Card> getUserCard(){
        return userRepo.getUserCard();
    }

    public LiveData<List<OpeningTime>> getAllOpeningTimes(){
        return this.opRepo.getOpeningTimes();
    }

    public void updateUser(final User user, final UserRepository.UpdateListener listener){
        this.userRepo.updateUser(user, listener);
    }

    public void addUser(final User user){
        this.userRepo.addUser(user);
    }

    public void setUser(final User user){
        this.userRepo.setUser(user);
    }

    public void deleteUserSubscriptions(final List<UserSubscription> subscriptions){
        userRepo.deleteUserSubscriptions(subscriptions);
    }

    public void updateSubscriptions(){
        this.userRepo.updateSubscriptions();
    }

    public void updateCard() {
        this.userRepo.updateCard();
    }

    public void addFavourite(final Instructor instructor, final String user){
        this.insRepo.addFavourite(instructor, user);
    }

    public void deleteFavourite(final Instructor instructor, final String user){
        this.insRepo.deleteFavourite(instructor, user);
    }

    /**
     * Do an operation with alarms
     * @param operation operation to do
     * @param alarmList list with all the alarms
     */
    public void manageCardAlarmOperation(final AppDatabase.DbOperation operation, final List<AlarmNotification> alarmList, final Date date){
        Runnable command = null;
        switch (operation){
            case ADD:
                command = new Runnable() {
                    @Override
                    public void run() {
                        List<Long> list = alarmRepo.addCardAlarms(alarmList);
                        if(list.size() > 0) {
                            for (int x = 0; x < list.size(); x++) {
                                alarmList.get(x).setUid(list.get(x).intValue());
                            }
                            final List<NotificationMsg> notList = createNotificationsFrom(alarmList, date, NotificationMsg.CARD_TYPE, true);
                            alarmRepo.addNotifications(notList);
                        }
                    }
                };
                break;
            case UPDATE:
                command = new Runnable() {
                    @Override
                    public void run() {
                        alarmRepo.updateCardAlarms(alarmList);
                        final List<NotificationMsg> notList = createNotificationsFrom(alarmList, date, NotificationMsg.CARD_TYPE, true);
                        alarmRepo.updateCardNotifications(notList, alarmList);
                    }
                };
                break;
            case REMOVE:
                command = new Runnable() {
                    @Override
                    public void run() {
                        alarmRepo.removeCardAlarms(alarmList);
                        alarmRepo.removeNotificationsOfCardAlarm(alarmList);
                    }
                };
                break;
            default:
                break;
        }
        if(command != null){
            new ParallelExecutor().execute(command);
        }
    }

    /**
     * Do an operation with alarms
     * @param operation operation to do
     * @param alarmList list with all the alarms
     */
    public void manageSubscriptionAlarmOperation(final AppDatabase.DbOperation operation, final List<AlarmNotification> alarmList, final Date date){
        final List<SubAlarmNotification> alarmNotificationList = new ArrayList<>();
        for(AlarmNotification alarm : alarmList){
            alarmNotificationList.add((SubAlarmNotification)alarm);
        }
        Runnable command = null;
        switch (operation){
            case ADD:
                command = new Runnable() {
                    @Override
                    public void run() {
                        List<Long> list = alarmRepo.addSubscriptionAlarms(alarmNotificationList);
                        if(list.size() > 0) {
                            for(int x = 0; x < list.size(); x++){
                                alarmList.get(x).setUid(list.get(x).intValue());
                            }
                            final List<NotificationMsg> notList = createNotificationsFrom(alarmList, date, NotificationMsg.SUB_TYPE, true);
                            alarmRepo.addNotifications(notList);
                        }
                    }
                };
                break;
            case UPDATE:
                command = new Runnable() {
                    @Override
                    public void run() {
                        alarmRepo.updateSubscriptionAlarms(alarmNotificationList);
                        final List<NotificationMsg> notList = createNotificationsFrom(alarmList, date, NotificationMsg.SUB_TYPE, true);
                        alarmRepo.updateSubscriptionNotifications(notList, alarmNotificationList);
                    }
                };
                break;
            case REMOVE:
                command = new Runnable() {
                    @Override
                    public void run() {
                        alarmRepo.removeSubscriptionAlarms(alarmNotificationList);
                        alarmRepo.removeNotificationsOfSubAlarm(alarmNotificationList);
                    }
                };
                break;
            default:
                break;
        }
        if(command != null){
            new ParallelExecutor().execute(command);
        }
    }

    private List<NotificationMsg> createNotificationsFrom(final List<AlarmNotification> list, final Date date, final int notificationType, boolean considerStatus){
        List<NotificationMsg> returnList = new ArrayList<>();
        for(AlarmNotification alarm : list){
            if((considerStatus && alarm.isOn()) || !considerStatus) {
                final User user = userRepo.getUser();
                String msg = user.getFullName() + ", ";
                if(notificationType == NotificationMsg.CARD_TYPE){
                    msg += context.getString(R.string.not_card_text_1);
                } else {
                    msg += context.getString(R.string.not_sub_text_1);
                    msg += userRepo.getReferenceOfSubscription(((SubAlarmNotification)alarm).getSubReference());
                    msg += context.getString(R.string.not_sub_text_2);
                }
                int quantity = alarm.getQuantity();
                if (quantity == 0) {
                    msg += context.getString(R.string.not_card_text_today);
                } else {
                    msg += context.getString(R.string.not_card_text_2) + quantity + " ";
                    msg += quantity > 1 ? context.getString(alarm.getPeriod().getIdMessagePlural()) :
                            context.getString(alarm.getPeriod().getIdMessage());
                }

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int actualDay = calendar.get(Calendar.DAY_OF_YEAR);
                int alarmDay = actualDay - alarm.getNumberOfDays();
                int year = calendar.get(Calendar.YEAR);
                while (alarmDay < 0) {
                    year--;
                    alarmDay += 365;
                }
                calendar.set(Calendar.DAY_OF_YEAR, alarmDay);
                calendar.set(Calendar.YEAR, year);

                NotificationMsg not = new NotificationMsg(
                        notificationType,
                        calendar.getTime(),
                        msg,
                        alarm.getUid()
                );
                returnList.add(not);
            }
        }
        return returnList;
    }

    private void startAlarmNotification(){
        final SharedPreferences pref = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        final boolean enable = pref.getBoolean(ALARM_ENABLE, false);
        if(!enable){
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, 13);

            Intent intent = new Intent(context, NotificationService.class);
            PendingIntent alarmIntent = PendingIntent.getService(context, 0, intent, 0);

            alarmManager.setInexactRepeating(AlarmManager.RTC, calendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY, alarmIntent);

            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(ALARM_ENABLE, true);
            editor.apply();
        }
    }
}
