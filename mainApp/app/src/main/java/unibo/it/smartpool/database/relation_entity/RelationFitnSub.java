package unibo.it.smartpool.database.relation_entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;

import unibo.it.smartpool.database.entity.AquaFitness;
import unibo.it.smartpool.database.entity.Subscription;

@Entity(
        primaryKeys = {"fitnessRef", "subscriptionRef"},
        foreignKeys = {
                @ForeignKey(
                        entity = AquaFitness.class,
                        parentColumns = "id",
                        childColumns = "fitnessRef",
                        onDelete = ForeignKey.CASCADE
                ),
                @ForeignKey(
                        entity = Subscription.class,
                        parentColumns = {"id"},
                        childColumns = {"subscriptionRef"},
                        onDelete = ForeignKey.CASCADE
                )
        },
        indices = @Index(
                value = {"subscriptionRef"},
                name = "fitn_sub_index"
        )
)
public class RelationFitnSub {

    @ColumnInfo
    private int fitnessRef;
    @ColumnInfo
    private int subscriptionRef;

    public RelationFitnSub(final int fitnessRef, final int subscriptionRef){
        this.fitnessRef = fitnessRef;
        this.subscriptionRef = subscriptionRef;
    }

    public int getFitnessRef() {
        return fitnessRef;
    }

    public void setFitnessRef(int fitnessRef) {
        this.fitnessRef = fitnessRef;
    }

    public int getSubscriptionRef() {
        return subscriptionRef;
    }

    public void setSubscriptionRef(int subscriptionRef) {
        this.subscriptionRef = subscriptionRef;
    }
}
