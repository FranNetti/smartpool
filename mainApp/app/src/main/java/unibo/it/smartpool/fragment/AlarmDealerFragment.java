package unibo.it.smartpool.fragment;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import unibo.it.smartpool.activity.AlarmChooserActivity;
import unibo.it.smartpool.database.entity.AlarmNotification;
import unibo.it.smartpool.view_elements.adapter.ClockAdapter;

abstract public class AlarmDealerFragment extends Fragment implements AlarmUserFragment {

    protected List<AlarmNotification> alarmList = new ArrayList<>();
    protected List<AlarmNotification> deleteAlarm = new ArrayList<>();
    protected List<AlarmNotification> updateAlarm = new ArrayList<>();
    protected List<AlarmNotification> addAlarm = new ArrayList<>();

    protected ListView clock;
    protected TextView noAlarm;
    protected Button save;
    protected ClockAdapter adp;

    private boolean hasToUpdate = false;
    private int elemPosToUpdate;

    @Override
    public void onAlarmCreation(final Intent data){
        /*
        if it's an update, check if the alarm has to be deleted
        if not then add the new alarm
        when adding a new element to one of the add/update/delete list, check if the same
        element isn't in another of those lists.
         */
        AlarmNotification newAlarm = (AlarmNotification) data.getSerializableExtra(AlarmChooserActivity.ALARM_NOT);
        if(hasToUpdate){
            boolean delete = data.getBooleanExtra(AlarmChooserActivity.ALARM_DEL, false);
            if(!delete && newAlarm != null && !this.alarmList.contains(newAlarm)){
                AlarmNotification old = this.alarmList.get(elemPosToUpdate);
                updateAlarm(old, newAlarm);
            } else if (delete || this.alarmList.contains(newAlarm)){
                AlarmNotification alarmRemoved = this.alarmList.remove(elemPosToUpdate);
                if(addAlarm.contains(alarmRemoved)){
                    addAlarm.remove(alarmRemoved);
                } else{
                    if(updateAlarm.contains(alarmRemoved)){
                        updateAlarm.remove(alarmRemoved);
                    }
                    deleteAlarm.add(alarmRemoved);
                }
            }
            updateAdapterView();
            if(alarmList.isEmpty()){
                noAlarm.setVisibility(View.VISIBLE);
                clock.setVisibility(View.GONE);
            }
            hasToUpdate = false;
        } else {
            if (newAlarm != null && !this.alarmList.contains(newAlarm)) {
                this.alarmList.add(newAlarm);
                this.addAlarm.add(newAlarm);
                updateAdapterView();
            }
        }
    }

    @Override
    public void onRequestUpdate(final AlarmNotification alarm) {
        hasToUpdate = true;
        elemPosToUpdate = this.alarmList.indexOf(alarm);
        Intent intent = new Intent(getContext(), AlarmChooserActivity.class);
        intent.putExtra(AlarmChooserActivity.UPDATE_ACTIVITY, true);
        getActivity().startActivityForResult(intent, AlarmChooserActivity.ALARM_ACTIVITY_INTENT_CODE);
    }

    @Override
    public void onStateChanged(AlarmNotification alarm, boolean on) {
        int index = alarmList.indexOf(alarm);
        AlarmNotification alarmToUpdate = alarmList.get(index);
        alarm.setOn(on);
        updateAlarm(alarmToUpdate, alarm);
        if (!this.save.isEnabled()) {
            this.changeButtonStatus(this.save, true);
        }
    }

    protected void updateAdpHeight(){
        this.noAlarm.setVisibility(View.GONE);
        this.clock.setVisibility(View.VISIBLE);
        this.adp.notifyDataSetChanged();
        this.clock.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                adp.getTotalHeight()
        ));
    }

    protected void changeButtonStatus(final Button btn, final boolean on){
        btn.setEnabled(on);
        btn.setVisibility(on ? View.VISIBLE : View.GONE);
    }

    /* Method to update an alarm, checks if it's not contained in any of the other lists */
    private void updateAlarm(final AlarmNotification oldAlarm, final AlarmNotification newAlarm){
        if(addAlarm.contains(oldAlarm)){
            int index = addAlarm.indexOf(oldAlarm);
            addAlarm.get(index).updateNotification(newAlarm);
        } else if(updateAlarm.contains(oldAlarm)) {
            int index = updateAlarm.indexOf(oldAlarm);
            updateAlarm.get(index).updateNotification(newAlarm);
        } else {
            oldAlarm.updateNotification(newAlarm);
            updateAlarm.add(oldAlarm);
        }
    }

    /* Method to update the adapter view; it updates the height and it enables the save buttons */
    private void updateAdapterView() {
        updateAdpHeight();
        if (!this.save.isEnabled()) {
            changeButtonStatus(this.save, true);
        } else if (this.addAlarm.isEmpty() && updateAlarm.isEmpty() && deleteAlarm.isEmpty()) {
            changeButtonStatus(this.save, false);
        }
    }

}
