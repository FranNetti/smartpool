package unibo.it.smartpool.repository;


import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.connection.HttpException;
import unibo.it.smartpool.database.AppDatabase;
import unibo.it.smartpool.database.entity.Favourite;
import unibo.it.smartpool.database.dao.FavouriteDAO;
import unibo.it.smartpool.database.entity.Instructor;
import unibo.it.smartpool.database.dao.InstructorDAO;
import unibo.it.smartpool.utils.ParallelExecutor;

public class InstructorRepository {

    private static final String TAG = "instr_repo_tag";

    private InstructorDAO instructorTable;
    private FavouriteDAO favouriteTable;
    private LiveData<List<Instructor>> instructorList;

    private ParallelExecutor executor = new ParallelExecutor();

    public InstructorRepository(final Context context){
        instructorTable = AppDatabase.getDatabase(context).instructorDAO();
        favouriteTable = AppDatabase.getDatabase(context).favouriteDAO();
        instructorList = instructorTable.getAllInstructors();
        getInstructorList();
    }


    public LiveData<List<Instructor>> getInstructorList() {
        executor.execute(new InstructorsCheckUpdate());
        return instructorList;
    }

    public LiveData<List<Instructor>> getFavouriteList(final String userEmail) {
        return favouriteTable.getAllFavouritesOfUser(userEmail);
    }

    public void addFavourite(final Instructor instructor, final String email){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                Favourite fav = new Favourite(email, instructor.getCf());
                favouriteTable.add(fav);
            }
        });
    }

    public void deleteFavourite(final Instructor instructor, final String email){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                Favourite fav = new Favourite(email, instructor.getCf());
                favouriteTable.delete(fav);
            }
        });
    }

    private class InstructorsCheckUpdate implements Runnable{

        @Override
        public void run() {
            Instructor instructor = instructorTable.getLeastUpdatedInstructor();
            if(instructor == null || instructor.hasToUpdate()) {
                final String url = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.INSTRUCTOR_PAGE;
                try{
                    String data = ConnectionUtilities.getDataFromUrl(url,null,false);
                    if(data != null && !data.isEmpty()){
                        List<Instructor> instrList = new ArrayList<>();
                        JSONArray array = new JSONArray(data);
                        for(int i = 0;i < array.length();i++){
                            JSONObject insJSON = array.getJSONObject(i);
                            instrList.add(new Instructor(insJSON));
                        }
                        if(instructor == null){
                            instructorTable.addAll(instrList);
                        } else {
                            instructorTable.updateAll(instrList);
                        }

                    }
                } catch(HttpException e){
                    Log.e(TAG,"HTTP Exception in getAllInstructors", e);
                } catch(JSONException e){
                    Log.e(TAG,"JSON Exception in getAllInstructors", e);
                }
            }
        }
   }
}
