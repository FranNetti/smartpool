package unibo.it.smartpool.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import unibo.it.smartpool.database.entity.AquaFitness;

@Dao
public interface AquaFitnessDAO {

    @Insert
    void addAquaFitness(final AquaFitness fitness);

    @Insert
    void addAquaFitnessCourses(final List<AquaFitness> fitness);

    @Query("SELECT * FROM aquafitness")
    List<AquaFitness> getAllCourses();

    @Query("SELECT * FROM aquafitness WHERE lastUpdate <= (SELECT MIN(lastUpdate) from aquafitness) LIMIT 1")
    AquaFitness getLeastUpdatedCourse();

    @Update
    void updateCourses(final List<AquaFitness> fitnesse);

}
