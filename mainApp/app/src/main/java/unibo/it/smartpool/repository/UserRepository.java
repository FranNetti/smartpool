package unibo.it.smartpool.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import unibo.it.smartpool.connection.ConnectionUtilities;;
import unibo.it.smartpool.connection.HttpException;
import unibo.it.smartpool.database.AppDatabase;
import unibo.it.smartpool.database.entity.Card;
import unibo.it.smartpool.database.dao.CardDAO;
import unibo.it.smartpool.database.entity.User;
import unibo.it.smartpool.database.dao.UserDAO;
import unibo.it.smartpool.database.entity.UserSubscription;
import unibo.it.smartpool.database.dao.UserSubscriptionDAO;
import unibo.it.smartpool.utils.ParallelExecutor;

public class UserRepository {

    public interface UpdateListener{
        void onUpdateComplete(boolean isUpdated);
    }

    public interface UserInformationListener {
        void onCardUpdate(Card newCard);
        void onSubscriptionsUpdate(List<UserSubscription> sub);
        void onSubscriptionsDeletion(List<UserSubscription> sub);
    }

    private static final String TAG = "user_repo_tag";

    private final Context context;
    private final UserInformationListener listener;
    private UserDAO userTable;
    private CardDAO cardTable;
    private UserSubscriptionDAO subscriptionTable;

    private LiveData<User> user;
    private LiveData<Card> card;
    private LiveData<List<UserSubscription>> subscriptions;
    private String email;

    private ParallelExecutor executor = new ParallelExecutor();

    public UserRepository(final Context context, final UserInformationListener listener){
        this.context = context;
        this.listener = listener;
        AppDatabase db = AppDatabase.getDatabase(context);
        userTable = db.userDao();
        cardTable = db.cardDAO();
        subscriptionTable = db.userSubscriptionDAO();
    }

    public LiveData<User> getObservableUser(){
        return this.user;
    }

    public User getUser(){
        return userTable.getUser(email);
    }

    public void setUser(final User user){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                User dbUser = userTable.getUser(user.getEmail());
                if(dbUser != null && !dbUser.equals(user)){
                    userTable.updateUser(user);
                }
            }
        });
        email = user.getEmail();
        this.user = userTable.getObservableUser(email);
        this.card = cardTable.getObservableCardFromUserEmail(email);
        this.subscriptions = subscriptionTable.getAllObservableSubscriptionOf(email);
    }

    public void updateUser(final User newUser, final UpdateListener listener){
        if(newUser.isUpdated(this.user.getValue())){
            new UserUpdateTask(newUser, listener).execute();
        } else {
            listener.onUpdateComplete(true);
        }
    }

    public void addUser(final User newUser){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                String user_email = newUser.getEmail();
                final String url_card = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.CARD_PAGE;
                final String url_sub = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.SUB_PAGE;
                try {
                    /* card creation */
                    Map<String, String> map = ConnectionUtilities.getCardMap(user_email);
                    String data = ConnectionUtilities.getDataFromUrl(url_card, map, true);
                    if(data != null && !data.isEmpty()){
                        JSONObject objData = new JSONObject(data);
                        final String number = objData.getString("numero");
                        final String expiration = objData.getString("scadenza");
                        final double credit = objData.getDouble("credito");
                        final String lastUpdate = Card.FORMATTER.format(Calendar.getInstance().getTime());
                        Card newCard = new Card(number, expiration, credit, user_email, lastUpdate);
                        cardTable.insertCard(newCard);
                    }
                    /* subscriptions creation */
                    map = ConnectionUtilities.getSubscriptionsMap(user_email);
                    data = ConnectionUtilities.getDataFromUrl(url_sub, map, true);
                    List<UserSubscription> subList = new ArrayList<>();
                    if(data != null && !data.isEmpty()) {
                        JSONArray subscriptions = new JSONArray(data);
                        for (int x = 0; x < subscriptions.length(); x++) {
                            JSONObject obj = subscriptions.getJSONObject(x);
                            subList.add(new UserSubscription(obj, user_email));
                        }
                        subscriptionTable.addSubscriptions(subList);
                    }
                    /* class variables initialization */
                    userTable.addUser(newUser);
                    email = user_email;
                    user = userTable.getObservableUser(user_email);
                    card = cardTable.getObservableCardFromUserEmail(user_email);
                    subscriptions = subscriptionTable.getAllObservableSubscriptionOf(user_email);
                } catch (HttpException e) {
                    Log.e(TAG, "Error in userRepository addUser, HttpCode: " + e.getCode(), e);
                } catch (JSONException e){
                    Log.e(TAG, "Error in userRepository addUser", e);
                }
            }
        });
    }

    public LiveData<Card> getUserCard(){
        executor.execute(new CardCheckUpdate());
        return card;
    }

    public LiveData<List<UserSubscription>> getUserSubscriptions(){
        executor.execute(new UserSubscriptionCheckUpdate());
        return subscriptions;
    }

    public void deleteUserSubscriptions(final List<UserSubscription> subscriptions){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                subscriptionTable.deleteSubscriptions(subscriptions);
                listener.onSubscriptionsDeletion(subscriptions);
            }
        });
    }

    public void updateSubscriptions(){
        executor.execute(new UserSubscriptionUpdate());
    }

    public void updateCard(){
        executor.execute(new UserCardUpdate());
    }

    public String getReferenceOfSubscription(final int subscriptionReference){
        return subscriptionTable.getReferenceOfSubscription(subscriptionReference);
    }

    private void updateSubscriptionsMethod(){
        final String url = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.SUB_PAGE;
        final Map<String, String> map = ConnectionUtilities.getSubscriptionsMap(email);
        try {
            final String data = ConnectionUtilities.getDataFromUrl(url, map, true);
            if (data != null && !data.isEmpty()) {
                boolean update = false;
                List<UserSubscription> oldSubList = subscriptionTable.getAllSubscriptionOf(email);
                List<UserSubscription> newSubList = new ArrayList<>();
                JSONArray subscriptions = new JSONArray(data);
                for (int x = 0; x < subscriptions.length(); x++) {
                    JSONObject obj = subscriptions.getJSONObject(x);
                    UserSubscription newSub = new UserSubscription(obj, email);
                    System.out.println(newSub);
                    if(oldSubList.contains(newSub)){
                        UserSubscription oldSub = oldSubList.get(oldSubList.indexOf(newSub));
                        update |= oldSub.updateUserSubscription(newSub);
                    } else {
                        newSubList.add(newSub);
                    }
                }

                subscriptionTable.updateSubscriptions(oldSubList);
                if(update){
                    listener.onSubscriptionsUpdate(oldSubList);
                }
                subscriptionTable.addSubscriptions(newSubList);

            }
        } catch (HttpException e) {
            Log.e(TAG, "Error in userRepository updateSubscriptionsMethod, HttpCode: " + e.getCode(), e);
        } catch (JSONException e) {
            Log.e(TAG, "Error in userRepository updateSubscriptionsMethod", e);
        }
    }

    private void updateCardMethod(final Card card) {
        final String url = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.CARD_PAGE;
        try {
            final Map<String, String> map = ConnectionUtilities.getCardMap(email);
            String data = ConnectionUtilities.getDataFromUrl(url, map, true);
            if (data != null && !data.isEmpty()) {
                JSONObject objData = new JSONObject(data);
                final String number = objData.getString("numero");
                final String expiration = objData.getString("scadenza");
                final int credit = objData.getInt("credito");
                final String lastUpdate = Card.FORMATTER.format(Calendar.getInstance().getTime());
                card.setCredit(credit);
                card.setExpiration(expiration);
                card.setNumber(number);
                card.setLastUpdate(lastUpdate);
                cardTable.updateCard(card);
                if (listener != null) {
                    listener.onCardUpdate(card);
                }
            }
        } catch (HttpException e) {
            Log.e(TAG, "Error in userRepository getUserCard, HttpCode: " + e.getCode(), e);
        } catch (JSONException e) {
            Log.e(TAG, "Error in userRepository getUserCard", e);
        }
    }

    /**
     * Task to send to the server the updated information
     */
    private class UserUpdateTask extends AsyncTask<Void, Void, Boolean> {

        private final User newUser;
        private final UpdateListener listener;

        public UserUpdateTask(final User newUser, final UpdateListener listener){
            this.newUser = newUser;
            this.listener = listener;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            final String oldEmail = new String(email);
            final String newEmail = newUser.getEmail();
            final Map<String, String> map = ConnectionUtilities.getUserUpdateMap(
                    this.newUser.getName(),
                    this.newUser.getSurname(),
                    this.newUser.getAddress(),
                    this.newUser.getNumber(),
                    this.newUser.getEmail(),
                    this.newUser.getPwd()
            );
            final String url = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.UPDATE_PAGE;
            try {
                if(ConnectionUtilities.isConnectionAvailable(context)) {
                    ConnectionUtilities.getDataFromUrl(url, map, true);
                    userTable.updateUser(newUser);
                    if(!oldEmail.equals(newEmail)) {
                        email = newEmail;
                        Card userCard = cardTable.getCardFromUserEmail(oldEmail);
                        userCard.setUserEmail(newEmail);
                        cardTable.updateCard(userCard);
                        subscriptionTable.updateUserSubscriptionsEmail(oldEmail, newEmail);
                        user = userTable.getObservableUser(newUser.getEmail());
                        card = cardTable.getObservableCardFromUserEmail(email);
                        subscriptions = subscriptionTable.getAllObservableSubscriptionOf(email);
                    }
                    return true;
                }
            } catch (Exception e) {
                Log.e(TAG, "error in userRepository update info", e);
                return false;
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (!success) {
                if(this.listener != null){
                    this.listener.onUpdateComplete(false);
                }
            } else {
                if(this.listener != null){
                    this.listener.onUpdateComplete(true);
                }
            }
        }
    }

    /**
     * Runnable for checking card updates online
     */
    private class CardCheckUpdate implements Runnable {

        @Override
        public void run() {
            final Card card = cardTable.getCardFromUserEmail(email);
            if(card.hasToUpdate()) {
                updateCardMethod(card);
            }
        }
    }

    /**
     * Runnable for checking subscriptions updates online
     */
    private class UserSubscriptionCheckUpdate implements Runnable {

        @Override
        public void run() {
            UserSubscription sub = subscriptionTable.getLeastUpdatedSubscriptionOf(email);
            if(sub == null || sub.hasToUpdate()) {
                updateSubscriptionsMethod();
            }
        }
    }

    /**
     * Runnable for updating subscriptions
     */
    private class UserSubscriptionUpdate implements Runnable {

        @Override
        public void run() {
            try {
                Thread.sleep(3000);
                updateSubscriptionsMethod();
            } catch (InterruptedException e){
                Log.e(TAG, "error in userSubscriptionUpdate", e);
            }
        }
    }

    /**
     * Runnable for updating subscriptions
     */
    private class UserCardUpdate implements Runnable {

        @Override
        public void run() {
            try {
                Thread.sleep(3000);
                final Card card = cardTable.getCardFromUserEmail(email);
                updateCardMethod(card);
            } catch (InterruptedException e){
                Log.e(TAG, "error in userSubscriptionUpdate", e);
            }
        }
    }
}
