package unibo.it.smartpool.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import unibo.it.smartpool.database.entity.Card;

@Dao
public interface CardDAO {

    @Query("SELECT * FROM card WHERE cardId = :id")
    LiveData<Card> getCardFromId(final int id);

    @Update
    void updateCard(final Card card);

    @Query("SELECT * FROM card WHERE user_email LIKE :email LIMIT 1")
    Card getCardFromUserEmail(final String email);

    @Query("SELECT * FROM card WHERE user_email LIKE :email LIMIT 1")
    LiveData<Card> getObservableCardFromUserEmail(final String email);

    @Insert
    void insertCard(final Card card);
}
