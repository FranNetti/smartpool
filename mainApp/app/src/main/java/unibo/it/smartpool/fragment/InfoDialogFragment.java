package unibo.it.smartpool.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import unibo.it.smartpool.R;
import unibo.it.smartpool.database.entity.Lesson;
import unibo.it.smartpool.view_elements.TimeRow;

public class InfoDialogFragment extends DialogFragment {

    public static InfoDialogFragment newInstance(final String instructors, final List<Lesson> lessons, final boolean isCourse){
        InfoDialogFragment f = new InfoDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(INSTRUCTOR_PARAM, instructors);
        bundle.putSerializable(LESSON_PARAM,(Serializable)lessons);
        bundle.putBoolean(COURSE_PARAM, isCourse);
        f.setArguments(bundle);
        return f;
    }

    private static final String INSTRUCTOR_PARAM = "instructor";
    private static final String LESSON_PARAM = "lesson";
    private static final String COURSE_PARAM = "course";

    String instList;
    ArrayList <Lesson> lessons;
    TableLayout table;
    TableRow row;
    View alertView;
    boolean course;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        Context dialogContext = builder.getContext();
        LayoutInflater inflater = getActivity().getLayoutInflater();

        Bundle bundle = getArguments();
        instList = bundle.getString(INSTRUCTOR_PARAM);
        lessons = (ArrayList<Lesson>) bundle.getSerializable(LESSON_PARAM);
        course = bundle.getBoolean(COURSE_PARAM);
        alertView = inflater.inflate(R.layout.info_dialog, null);
        row = alertView.findViewById(R.id.row_inst);
        table = alertView.findViewById(R.id.info_table);
        set();
        builder.setTitle(alertView.getContext().getString(R.string.course_dialog_title))
                .setView(alertView)
                .setPositiveButton(R.string.course_dialog_affermative, null);

        return builder.create();
    }

    public void set(){
        TextView t = new TextView(alertView.getContext());
        t.setText(instList);
        row.addView(t);

        for (Lesson l: lessons){
            String s = alertView.getContext().getString(l.getIntDay(l.getDay()));
            String pool = alertView.getContext().getString(R.string.pool);
            if(!course) {
                table.addView(new TimeRow(alertView.getContext(), s, l.getTime(), pool + "  " + l.getPool()));
            }else{
                table.addView(new TimeRow(alertView.getContext(), s, l.getTime(), ""));
            }
        }

    }


}
