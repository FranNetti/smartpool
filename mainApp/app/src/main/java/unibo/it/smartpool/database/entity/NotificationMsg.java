package unibo.it.smartpool.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Class that implements the table notificationMsg of the db
 */
@Entity(tableName = "notificationMsg")
public class NotificationMsg {

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final int CARD_TYPE = 1;
    public static final int SUB_TYPE = 2;
    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    @PrimaryKey
    private int uid;

    @ColumnInfo(name = "date")
    @NonNull
    private String date;

    @ColumnInfo(name = "msg")
    private String msg;

    @ColumnInfo(name = "type")
    private int type;

    @ColumnInfo(name = "alarm_ref")
    private int alarmRef;


    public NotificationMsg(final int uid, final int type, final String date, final String msg, final int alarmRef){
        this.uid = uid;
        this.date = date;
        this.msg = msg;
        this.type = type;
        this.alarmRef = alarmRef;
    }

    public NotificationMsg(final int type, final Date date, final String msg, final int alarmRef){
        this.date = FORMATTER.format(date);
        this.msg = msg;
        this.type = type;
        this.alarmRef = alarmRef;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(final String msg) {
        this.msg = msg;
    }

    public int getType() {
        return type;
    }

    public void setType(final int type) {
        this.type = type;
    }

    public int getAlarmRef() {
        return alarmRef;
    }

    public void setAlarmRef(int alarmRef) {
        this.alarmRef = alarmRef;
    }

    public Date getFormatDate(){
        try {
            return FORMATTER.parse(this.date);
        } catch (ParseException e){
            return Calendar.getInstance().getTime();
        }
    }

    public void setFormatDate(final Date date){
        this.date = FORMATTER.format(date);
    }
}
