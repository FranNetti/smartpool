package unibo.it.smartpool.view_elements.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import unibo.it.smartpool.R;
import unibo.it.smartpool.database.entity.UserSubscription;

/**
 * Class that implements a personalized version of an adapter for a list view
 */
public class SubscriptionAdapter extends ArrayAdapter<UserSubscription> {

    private boolean isEditable = false;

    public SubscriptionAdapter(Context context, int textViewResourceId, List<UserSubscription> objects) {
        super(context, textViewResourceId, objects);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.subscription_item, null);

        final UserSubscription item = getItem(position);
        final TextView subscription = (TextView) view.findViewById(R.id.subscription_text);
        final TextView expiration = (TextView) view.findViewById(R.id.expiration_text);
        final ImageView arrow = (ImageView) view.findViewById(R.id.sub_arrow);
        final TextView expired = (TextView) view.findViewById(R.id.sub_expired);
        final CheckBox checkBox = view.findViewById(R.id.sub_checkbox);

        /* subscription setup */
        subscription.setText(item.getReference());

        /* expiration and arrow setup */
        if(item.getSubscriptionType() == UserSubscription.SubscriptionType.MONTHLY && item.isExpired()){
            arrow.setVisibility(View.GONE);
        } else {
            expired.setVisibility(View.GONE);
        }

        /* expiration date setup */
        String text;
        switch (item.getSubscriptionType()){
            case SINGLE_ENTRANCE:
                final int remEntr = item.getNumRemainingEntrances();
                final int temp =  (remEntr > 1 || remEntr == 0)   ? R.string.entrance_remaining_plural : R.string.entrance_remaining;
                text = remEntr + " " + getContext().getString(temp);
                arrow.setVisibility(View.GONE);
                break;
            case MONTHLY:
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Context context = getContext();
                if(item.isStarted()){
                    text = context.getString(R.string.expriation_date_1) + format.format(item.getStart()) + context.getString(R.string.expriation_date_2) + format.format(item.getExpiration());
                } else {
                    text = context.getString(R.string.start_date_1) + format.format(item.getStart());
                }

                break;
            default:
                text = "";
                break;
        }
        expiration.setText(text);

        /* checkbox setup */
        if(this.isEditable && item.isExpired()){
            checkBox.setVisibility(View.VISIBLE);
            CheckBoxToggler toggler = new CheckBoxToggler(checkBox);
            expiration.setOnClickListener(toggler);
            subscription.setOnClickListener(toggler);
            expired.setOnClickListener(toggler);
        } else {
            checkBox.setVisibility(View.GONE);
        }

        return view;
    }

    public int getTotalHeight() {
        return this.getCount() * this.getSingleElementHeight();
    }

    public int getSingleElementHeight() {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getContext().getResources().getDisplayMetrics());
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

    private class CheckBoxToggler implements View.OnClickListener{

        private final CheckBox checkBox;

        CheckBoxToggler(final CheckBox checkBox){
            this.checkBox = checkBox;
        }

        @Override
        public void onClick(View v) {
            checkBox.toggle();
        }
    }
}
