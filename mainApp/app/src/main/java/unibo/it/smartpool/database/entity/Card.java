package unibo.it.smartpool.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Entity
public class Card extends UpdatableTable{

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    @PrimaryKey(autoGenerate = true)
    private int cardId;

    @ColumnInfo
    private String number;

    @ColumnInfo
    private String expiration;

    @ColumnInfo
    private double credit;

    @ColumnInfo
    private String lastUpdate;

    @ColumnInfo(name = "user_email")
    private String userEmail;

    @Ignore
    private Date expirationDate;

    public Card(final String number, final String expiration, final double credit, final String userEmail, final String lastUpdate){
        this.credit = credit;
        this.number = number;
        this.expiration = expiration;
        this.lastUpdate = lastUpdate;
        this.userEmail = userEmail;
        try {
            this.expirationDate = FORMATTER.parse(expiration);
        } catch (ParseException e){
            this.expirationDate = Calendar.getInstance().getTime();
        }
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
        try {
            this.expirationDate = FORMATTER.parse(expiration);
        } catch (ParseException e){
            this.expirationDate = Calendar.getInstance().getTime();
        }
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
        this.expiration = FORMATTER.format(expirationDate);
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public boolean hasToUpdate(){
        return super.hasToUpdate(FORMATTER, lastUpdate);
    }
}
