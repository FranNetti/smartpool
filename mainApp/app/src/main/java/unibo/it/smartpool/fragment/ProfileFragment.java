package unibo.it.smartpool.fragment;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

import unibo.it.smartpool.InformationManager;
import unibo.it.smartpool.R;
import unibo.it.smartpool.database.entity.User;

/**
 * Class for a fragment that shows the user info
 */
public class ProfileFragment extends Fragment {

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    private LiveData<User> user;
    private ProfileFragmentListener mListener;
    private TextView name;
    private TextView date;
    private TextView addr;
    private TextView mail;
    private TextView pwd;
    private TextView num;
    private ImageView profileImage;
    private SimpleDateFormat dateFormat;

    TableLayout view;

    public ProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        this.user = ViewModelProviders.of(getActivity()).get(InformationManager.class).getObservableUser();

        ImageButton btn = view.findViewById(R.id.change_info);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onButtonChangeInfoPressed();
                }
            }
        });

        this.name = view.findViewById(R.id.user_name);
        this.addr = view.findViewById(R.id.user_addr);
        this.num = view.findViewById(R.id.user_num);
        this.dateFormat = new SimpleDateFormat("dd MMMM YYYY", Locale.getDefault());
        this.date = view.findViewById(R.id.user_date);
        this.mail = view.findViewById(R.id.user_email);
        this.pwd = view.findViewById(R.id.user_pwd);
        this.profileImage = view.findViewById(R.id.user_img);

        this.user.observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if(user != null) {
                    name.setText(user.getFullName());
                    addr.setText(user.getAddress());
                    num.setText(user.getNumber());
                    date.setText(dateFormat.format(user.getBirthDate()));
                    mail.setText(user.getEmail());
                    pwd.setText(getPwdEncoded(user));
                    if(user.isMale()) {
                        profileImage.setImageResource(R.drawable.user_male);
                    } else {
                        profileImage.setImageResource(R.drawable.user_female);
                    }
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProfileFragmentListener) {
            mListener = (ProfileFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ProfileFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private String getPwdEncoded(final User user) {
        final String pwdText = user.getPwd();
        final int length = pwdText.length() - 2;
        StringBuilder str = new StringBuilder().append(pwdText.charAt(0));
        for (int x = 0; x < length; x++) {
            str.append("*");
        }
        str.append(pwdText.charAt(pwdText.length() - 1));
        return str.toString();
    }

    public interface ProfileFragmentListener {
        void onButtonChangeInfoPressed();
    }
}
