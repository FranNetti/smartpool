package unibo.it.smartpool.fragment;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;
import java.util.List;

import unibo.it.smartpool.InformationManager;
import unibo.it.smartpool.R;
import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.database.entity.OpeningTime;
import unibo.it.smartpool.view_elements.InfoTableRow;

/**
 * Class for a fragment that shows the pool info
 */
public class InfoFragment extends Fragment {

    public static InfoFragment newInstance() {
        return new InfoFragment();
    }

    private TextView addr;
    private TextView email;
    private TextView warningTv;
    private TableLayout scheduleTable;

    public InfoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_info, container, false);
        this.addr = view.findViewById(R.id.address);
        this.email = view.findViewById(R.id.email);
        this.scheduleTable = view.findViewById(R.id.schedule_table);
        setLinkText(addr, getContext().getString(R.string.pool_addr));
        setLinkText(email, getContext().getString(R.string.pool_email));
        this.warningTv = view.findViewById(R.id.warning_text);

        InformationManager manager = ViewModelProviders.of(getActivity()).get(InformationManager.class);
        LiveData<List<OpeningTime>> data = manager.getAllOpeningTimes();
        data.observe(this, new Observer<List<OpeningTime>>() {
            @Override
            public void onChanged(@Nullable List<OpeningTime> openingTimes) {
                final Context context = getContext();
                scheduleTable.removeAllViews();
                for(OpeningTime opening : openingTimes){
                    scheduleTable.addView(new InfoTableRow(context, opening.getDay(), opening.getHours()));
                }
            }
        });

        this.email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:" + email.getText().toString()));

                String title = getResources().getString(R.string.chooser_email_title);
                Intent chooser = Intent.createChooser(emailIntent, title);
                if(emailIntent.resolveActivity(getActivity().getPackageManager()) != null){
                    startActivity(chooser);
                }
            }
        });

        this.addr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //query got from google apis
                String addressEncoded = "0,0?q=" + addr.getText().toString().replaceAll(" ", "+");
                Uri address = Uri.parse("geo:" + addressEncoded);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, address);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean isOnline = ConnectionUtilities.isConnectionAvailable(getContext());
        this.warningTv.setVisibility(isOnline ? View.GONE : View.VISIBLE);
    }

    private void setLinkText(final TextView link, final String text) {
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        link.setText(content);
        link.setTextColor(getResources().getColor(R.color.colorAccent));
    }
}
