package unibo.it.smartpool.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import unibo.it.smartpool.database.entity.AlarmNotification;

/**
 * Interface to handle AlarmNotification table in the database
 */
@Dao
public interface AlarmNotDAO {

    @Query("SELECT * FROM AlarmNotification")
    LiveData<List<AlarmNotification>> getAllAlarms();

    @Query("SELECT * FROM AlarmNotification WHERE `on` = 1 ORDER BY quant DESC")
    List<AlarmNotification> getAllSwitchedOnAlarmNotification();

    @Query("UPDATE alarmNotification SET `on` = :on WHERE uid = :id ")
    void updateAlarmStatus(final int id, final boolean on);

    @Update
    void updateAlarm(AlarmNotification alarm);

    @Update
    void updateAll(AlarmNotification... alarms);

    @Update
    void updateAll(List<AlarmNotification> alarms);

    @Insert
    List<Long> insertAll(AlarmNotification... alarms);

    @Insert
    List<Long> insertAll(List<AlarmNotification> alarms);

    @Insert
    Long insert(AlarmNotification alarm);

    @Delete
    void delete(AlarmNotification alarm);

    @Delete
    void deleteAll(AlarmNotification... alarms);

    @Delete
    void deleteAll(List<AlarmNotification> alarms);
}
