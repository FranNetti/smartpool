package unibo.it.smartpool.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import unibo.it.smartpool.InformationManager;
import unibo.it.smartpool.R;
import unibo.it.smartpool.utils.ParallelExecutor;

import static android.nfc.NdefRecord.createMime;

/**
 * Class for a fragment that handles the NFC management to enter in the pool
 */
public class EntranceFragment extends Fragment implements NfcAdapter.CreateNdefMessageCallback {

    public static EntranceFragment newInstance() {
        return new EntranceFragment();
    }

    private static final String BEAM = "application/vnd.com.example.android.beam";
    private NfcAdapter nfcAdapter;
    private TextView textView;
    private String userEmail;
    private InformationManager manager;
    private boolean nfcPresent = true;

    public EntranceFragment() {
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.nfcAdapter = NfcAdapter.getDefaultAdapter(this.getContext());
        this.manager = ViewModelProviders.of(getActivity()).get(InformationManager.class);
        new ParallelExecutor().execute(new Runnable() {
            @Override
            public void run() {
                userEmail = manager.getUser().getEmail();
            }
        });
        this.nfcAdapter.setNdefPushMessageCallback(this, getActivity());
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_entrance, container, false);
        this.textView = (TextView) view.findViewById(R.id.nfc_text);
        if(this.nfcAdapter == null) {
            this.nfcPresent = false;
            this.textView.setText(R.string.nfc_absent);
        } else {
            this.on();
        }
        return view;
    }

    @Override
    public void onResume() {
        if(this.nfcPresent && this.nfcAdapter.isEnabled()){
            this.textView.setText(R.string.nfc_sending);
        } else {
            this.textView.setText(R.string.nfc_off);
        }
        super.onResume();
    }

    private void on(){
        // if the NFC is switched off, let the user turn on
        if (!this.nfcAdapter.isEnabled()) {
            Toast.makeText(this.getContext(), R.string.nfc_turn_on, Toast.LENGTH_LONG).show();
            startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
        }
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        final String text = this.userEmail != null ? this.userEmail : "";
        final NdefMessage ndefMessage =  new NdefMessage( new NdefRecord[] {
                createMime(BEAM, text.getBytes())
        });
        manager.updateSubscriptions();
        return ndefMessage;
    }
}
