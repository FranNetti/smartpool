package unibo.it.smartpool.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Locale;

import unibo.it.smartpool.InformationManager;
import unibo.it.smartpool.R;
import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.database.entity.User;
import unibo.it.smartpool.repository.UserRepository;

/**
 * Class for a fragment that allows the user to change infos.
 */
public class ChangeInfoFragment extends Fragment implements UserRepository.UpdateListener {

    public static ChangeInfoFragment newInstance() {
        return new ChangeInfoFragment();
    }

    private ChangeInfoFragmentListener mListener;
    private InformationManager manager;
    private User user;
    private TextView name;
    private TextView date;
    private ImageView profileImage;
    private EditText addr;
    private EditText num;
    private EditText mail;
    private EditText pwd;

    public ChangeInfoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_change_info, container, false);
        this.manager = ViewModelProviders.of(getActivity()).get(InformationManager.class);

        this.name = view.findViewById(R.id.change_user_name);
        this.date = view.findViewById(R.id.change_user_date);
        this.profileImage = view.findViewById(R.id.change_user_img);
        this.addr = view.findViewById(R.id.change_user_addr);
        this.num = view.findViewById(R.id.change_user_num);
        this.mail = view.findViewById(R.id.change_user_email);
        this.pwd = view.findViewById(R.id.change_user_pwd);

        final Button btn = view.findViewById(R.id.change_complete);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    final String addrText = addr.getText().toString();
                    final String numText = num.getText().toString();
                    final String mailText = mail.getText().toString();
                    final String pwdText = pwd.getText().toString();
                    if(addrText.isEmpty() || numText.isEmpty() || mailText.isEmpty() || pwdText.isEmpty()) {
                        Toast.makeText(getContext(), R.string.empty_fields, Toast.LENGTH_SHORT).show();
                    } else {
                        user.setAddress(addrText);
                        user.setNumber(numText);
                        user.setEmail(mailText);
                        user.setPwd(pwdText);
                        manager.updateUser(user, ChangeInfoFragment.this);
                    }
                }
            }
        });

        new SetUserFields().execute();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ChangeInfoFragmentListener) {
            mListener = (ChangeInfoFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onUpdateComplete(boolean isUpdated) {
        if(isUpdated){
            mListener.onChangesCompleted();
        } else {
            if(ConnectionUtilities.isConnectionAvailable(getContext())){
                ConnectionUtilities.alertConnectionError(getContext());
            } else {
                ConnectionUtilities.alertConnectionAbsence(getContext());
            }
        }
    }

    public interface ChangeInfoFragmentListener {
        void onChangesCompleted();
    }

    private class SetUserFields extends AsyncTask<Void, Void, User>{
        @Override
        protected User doInBackground(Void... voids) {
            return manager.getUser();
        }

        @Override
        protected void onPostExecute(User newUser) {
            user = newUser;
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM YYYY", Locale.getDefault());
            name.setText(newUser.getFullName());
            date.setText(dateFormat.format(newUser.getBirthDate()));
            if(newUser.isMale()) {
                profileImage.setImageResource(R.drawable.user_male);
            } else {
                profileImage.setImageResource(R.drawable.user_female);
            }
            addr.setText(newUser.getAddress());
            num.setText(newUser.getNumber());
            mail.setText(newUser.getEmail());
            pwd.setText(newUser.getPwd());
        }
    }
}
