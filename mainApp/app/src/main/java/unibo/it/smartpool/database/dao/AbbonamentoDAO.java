package unibo.it.smartpool.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import unibo.it.smartpool.database.entity.Subscription;

@Dao
public interface AbbonamentoDAO {

    @Insert
    void addSubscription(final Subscription subscription);

    @Insert
    void addSubscriptions(final List<Subscription> subscriptions);

    @Query("SELECT * FROM Subscription")
    List<Subscription> getAll();

    @Update
    void updateSubscriptions(final List<Subscription> subscriptions);

}
