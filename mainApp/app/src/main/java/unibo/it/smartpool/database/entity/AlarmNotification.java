package unibo.it.smartpool.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import unibo.it.smartpool.R;

/**
 * Class that implements the table alarmNotification of the db
 */
@Entity(tableName = "alarmNotification")
public class AlarmNotification implements Serializable{

    public static Calendar getNewDate(final Date date, final AlarmNotification alarm){
        Calendar returnCalendar = Calendar.getInstance();
        returnCalendar.setTime(date);
        int actualDay = returnCalendar.get(Calendar.DAY_OF_YEAR);
        int alarmDay = actualDay - alarm.getNumberOfDays();
        int year = returnCalendar.get(Calendar.YEAR);
        while (alarmDay < 0) {
            year--;
            alarmDay += 365;
        }
        returnCalendar.set(Calendar.DAY_OF_YEAR, alarmDay);
        returnCalendar.set(Calendar.YEAR, year);
        return returnCalendar;
    }

    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "quant")
    private int quantity;

    @ColumnInfo(name = "date_type")
    private String dateType;

    @ColumnInfo(name = "on")
    private boolean on;

    @Ignore
    private TimePeriod period;

    /**
     * Constructor.
     * @param quantity how much period before
     * @param dateType the time period
     * @param on if it's on
     */
    public AlarmNotification(final int quantity, final String dateType, final boolean on){
        this.quantity = quantity;
        this.dateType = dateType;
        this.on = on;
        this.period = TimePeriod.valueOf(dateType);
    }

    /**
     * Constructor.
     * @param quantity how much period before
     * @param period the time period
     * @param on if it's on
     */
    public AlarmNotification(final int quantity, final TimePeriod period, final boolean on){
        this.quantity = quantity;
        this.period = period;
        this.on = on;
        this.dateType = period.name();
    }

    /**
     * Constructor. It's supposed to be on.
     * @param quantity how much period before
     * @param period the time period
     */
    public AlarmNotification(final int quantity, final TimePeriod period){
        this(quantity, period, true);
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

    public TimePeriod getPeriod() {
        return period;
    }

    public int getNumberOfDays() {
        return quantity * period.getMultiplier();
    }

    public void updateNotification(final int quantity, final String period, final boolean on){
        this.quantity = quantity;
        this.dateType = period;
        this.on = on;
        this.period = TimePeriod.valueOf(period);
    }

    public void updateNotification(final int quantity, final TimePeriod period, final boolean on){
        this.quantity = quantity;
        this.period = period;
        this.on = on;
        this.dateType = period.name();
    }


    public void updateNotification(final AlarmNotification newAlarm){
        this.updateNotification(newAlarm.getQuantity(), newAlarm.getPeriod().name(), newAlarm.isOn());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlarmNotification that = (AlarmNotification) o;
        return quantity == that.quantity &&
                period == that.period;
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantity, period);
    }

    /**
     * Enum for the time period
     */
    public enum TimePeriod {
        DAY(R.string.day, R.string.day_plural, 1),
        WEEK(R.string.week, R.string.week_plural, 7),
        MONTH(R.string.month, R.string.month_plural, 30),
        YEAR(R.string.year, R.string.year_plural, 365);

        private final int printMessage;
        private final int printMessagePlural;
        private final int multiplier;
        private boolean on;

        TimePeriod(final int printMessage, final int printMessagePlural, final int multiplier){
            this.printMessage = printMessage;
            this.printMessagePlural = printMessagePlural;
            this.multiplier = multiplier;
        }

        public int getIdMessage(){
            return printMessage;
        }

        public int getIdMessagePlural() {
            return printMessagePlural;
        }

        public int getMultiplier() {
            return multiplier;
        }

    };
}
