package unibo.it.smartpool.view_elements.adapter;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;

import unibo.it.smartpool.R;
import unibo.it.smartpool.database.entity.Subscription;
import unibo.it.smartpool.database.entity.AquaFitness;
import unibo.it.smartpool.database.entity.Lesson;
import unibo.it.smartpool.fragment.InfoDialogFragment;
import unibo.it.smartpool.view_elements.MyButton;
import unibo.it.smartpool.view_elements.PriceTableRow;

public class CourseAdapter extends ArrayAdapter<AquaFitness> {
    private ImageView coursePic;
    private TextView courseTitle;
    private TextView description;
    private TableLayout table;
    private ImageButton infoButton;
    private CurseAdapterListener listener;
    private int id;


    public CourseAdapter(Context context, int textViewResourceId, CurseAdapterListener listener, List<AquaFitness> objects) {
        super(context, textViewResourceId, objects);
        id = 0;
        this.listener = listener;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.item, null);
        final AquaFitness item = getItem(position);
        componentSet(view);
        courseTitle.setText(item.getName());
        infoButton = new ImageButton(getContext());
        TableRow t1 = view.findViewById(R.id.row_title);
        t1.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        t1.addView(infoButton);
        description.setText(item.getDescription());
        final String instructors = item.getInstrList();
        final List<Lesson> lessons = item.getLessons();
        final List<Subscription> list = item.getSubscriptions();
        for(final Subscription abb:list){
            MyButton button = new MyButton(getContext());
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onSubscriptionSelected(item,abb);
                }
            });
            table.addView(new PriceTableRow(getContext(), abb.getPrice(), abb.getType()+" "+abb.getLabel(), button));
        }
        infoButton.setId(id++);

        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //CoursesInfoFragment fragment = CoursesInfoFragment.newInstance();
                DialogFragment newFragment = InfoDialogFragment.newInstance(instructors, lessons, false);
                FragmentManager fm = ((AppCompatActivity)getContext()).getFragmentManager();
                newFragment.show(fm, "bundle");
            }
        });

        infoButton.setImageResource(R.drawable.ic_info_outline_black_24dp);
        infoButton.setBackgroundColor(0x00000000);
        return view;
    }

    private void componentSet(View view1){
        courseTitle = view1.findViewById(R.id.coursetitle);
        coursePic = new ImageView(getContext());
        description = view1.findViewById(R.id.coursedescription);
        coursePic.setImageResource(R.drawable.aquafitness);
        LinearLayout img = view1.findViewById(R.id.imgCorso);
        img.addView(coursePic);
        table = (TableLayout) view1.findViewById(R.id.table);
        courseTitle.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        description.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        courseTitle.setTextSize(22);
        courseTitle.setTypeface(null, Typeface.BOLD);

    }

    public interface CurseAdapterListener {
     void onSubscriptionSelected(final AquaFitness item, final Subscription sub);
    }
}
