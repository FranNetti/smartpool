package unibo.it.smartpool.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import unibo.it.smartpool.database.dao.AlarmNotDAO;
import unibo.it.smartpool.database.entity.AlarmNotification;
import unibo.it.smartpool.database.AppDatabase;
import unibo.it.smartpool.database.entity.Card;
import unibo.it.smartpool.database.dao.NotificationDAO;
import unibo.it.smartpool.database.entity.NotificationMsg;
import unibo.it.smartpool.database.entity.SubAlarmNotification;
import unibo.it.smartpool.database.dao.SubAlarmNotificationDAO;
import unibo.it.smartpool.database.entity.UserSubscription;
import unibo.it.smartpool.utils.ParallelExecutor;

public class AlarmRepository implements UserRepository.UserInformationListener {

    private NotificationDAO notificationDAO;
    private AlarmNotDAO cardAlarmNotDAO;
    private SubAlarmNotificationDAO subAlarmNotDAO;
    private ParallelExecutor executor = new ParallelExecutor();

    private LiveData<List<AlarmNotification>> cardAlarmList = new MutableLiveData<>();

    public AlarmRepository(final Context context){
        AppDatabase db = AppDatabase.getDatabase(context);
        this.notificationDAO = db.notificationDAO();
        this.cardAlarmNotDAO = db.alarmNotDAO();
        this.subAlarmNotDAO = db.subAlarmNotificationDAO();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                cardAlarmList = cardAlarmNotDAO.getAllAlarms();
            }
        });
    }

    public LiveData<List<AlarmNotification>> getAllCardAlarms(){
        return cardAlarmList;
    }

    public LiveData<List<SubAlarmNotification>> getSubAlarmList(final UserSubscription sub) {
        return subAlarmNotDAO.getAllObservableAlarmsOfSubscription(sub.getId());
    }

    public List<Long> addCardAlarms(final List<AlarmNotification> alarms){
        return cardAlarmNotDAO.insertAll(alarms);
    }

    public void removeCardAlarms(final List<AlarmNotification> alarms){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                cardAlarmNotDAO.deleteAll(alarms);
            }
        });
    }

    public void updateCardAlarms(final List<AlarmNotification> alarms){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                cardAlarmNotDAO.updateAll(alarms);
            }
        });
    }

    public List<Long> addSubscriptionAlarms(final List<SubAlarmNotification> alarms){
        return subAlarmNotDAO.insertAll(alarms);
    }

    public void removeSubscriptionAlarms(final List<SubAlarmNotification> alarms){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                subAlarmNotDAO.deleteAll(alarms);
            }
        });
    }

    public void updateSubscriptionAlarms(final List<SubAlarmNotification> alarms){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                subAlarmNotDAO.updateAll(alarms);
            }
        });
    }

    public void addNotifications(final List<NotificationMsg> notif){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                notificationDAO.insertAll(notif);
            }
        });
    }

    public void updateCardNotifications(final List<NotificationMsg> notifications, final List<AlarmNotification> alarms){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                removeNotificationsOfCardAlarm(alarms);
                addNotifications(notifications);
            }
        });
    }

    public void removeNotificationsOfCardAlarm(List<AlarmNotification> alarmList){
        List<Integer> alarmUid = new ArrayList<>();
        for(AlarmNotification alarm : alarmList){
            alarmUid.add(alarm.getUid());
        }
        notificationDAO.deleteAllNotificationOfAlarms(alarmUid);
    }

    public void updateSubscriptionNotifications(final List<NotificationMsg> notifications, final List<SubAlarmNotification> alarms){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                removeNotificationsOfSubAlarm(alarms);
                addNotifications(notifications);
            }
        });
    }

    public void removeNotificationsOfSubAlarm(List<SubAlarmNotification> alarmList){
        List<Integer> alarmUid = new ArrayList<>();
        for(SubAlarmNotification alarm : alarmList){
            alarmUid.add(alarm.getUid());
        }
        notificationDAO.deleteAllNotificationOfAlarms(alarmUid);
    }

    @Override
    public void onCardUpdate(final Card newCard) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                final Date newExpiration = newCard.getExpirationDate();
                List<AlarmNotification> alarms = cardAlarmNotDAO.getAllSwitchedOnAlarmNotification();
                List<Integer> alarmsUid = new ArrayList<>();
                for(AlarmNotification alarm : alarms){
                    alarmsUid.add(alarm.getUid());
                }
                List<NotificationMsg> oldNotList = notificationDAO.getAllNotificationOfType(NotificationMsg.CARD_TYPE);
                for(NotificationMsg notification : oldNotList){
                    AlarmNotification alarm = alarms.get(alarmsUid.indexOf(notification.getAlarmRef()));
                    Calendar newDate = AlarmNotification.getNewDate(newExpiration, alarm);
                    notification.setFormatDate(newDate.getTime());
                }
                notificationDAO.updateNotifications(oldNotList);
            }
        });
    }

    @Override
    public void onSubscriptionsUpdate(final List<UserSubscription> subscriptions) {
       executor.execute(new Runnable() {
            @Override
            public void run() {
                for(UserSubscription sub : subscriptions) {
                    final Date newExpiration = sub.getExpiration();
                    List<AlarmNotification> alarms = subAlarmNotDAO.getAllSwitchedOnAlarmNotificationOfSubscription(sub.getId());
                    List<Integer> alarmsUid = new ArrayList<>();
                    for(AlarmNotification alarm : alarms){
                        alarmsUid.add(alarm.getUid());
                    }
                    List<NotificationMsg> oldNotList = notificationDAO.getAllNotificationOfAlarms(alarmsUid);
                    for(NotificationMsg notification : oldNotList){
                        AlarmNotification alarm = alarms.get(alarmsUid.indexOf(notification.getAlarmRef()));
                        Calendar newDate = AlarmNotification.getNewDate(newExpiration, alarm);
                        notification.setFormatDate(newDate.getTime());
                    }
                    if(oldNotList.size() > 0) {
                        System.out.println(oldNotList.get(0).getDate());
                    }
                    notificationDAO.updateNotifications(oldNotList);
                }
            }
        });
    }

    @Override
    public void onSubscriptionsDeletion(final List<UserSubscription> subscriptions) {
        for(UserSubscription sub : subscriptions){
            subAlarmNotDAO.deleteAlarm(sub.getId());
        }
    }
}
