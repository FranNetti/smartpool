package unibo.it.smartpool.view_elements;

import android.content.Context;
import android.util.TypedValue;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Class that implements a single row of a table
 */
public class InfoTableRow extends TableRow {

    public InfoTableRow(final Context context, final String header, final String text) {
        super(context);
        this.setLayoutParams(new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT
        ));

        TextView headerView = new TextView(context);
        TextView textView = new TextView(context);
        headerView.setLayoutParams(new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT,
                1f
        ));
        textView.setLayoutParams(new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT,
                1f
        ));

        headerView.setText(header.toUpperCase());
        headerView.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
        textView.setText(text);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);

        this.addView(headerView);
        this.addView(textView);
    }
}
