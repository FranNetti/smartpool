package unibo.it.smartpool.view_elements;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.lang.reflect.Type;

public class PriceTableRow extends TableRow {

    public PriceTableRow (Context context, String price, String type, MyButton button){
        super(context);

        TextView priceView = new TextView(context);
        TextView typeView = new TextView(context);

        priceView.setLayoutParams(new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT,
              1f
        ));

        typeView.setLayoutParams(new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT,
                1f
        ));

        button.setLayoutParams(new TableRow.LayoutParams(
                50,
                100,
                1f

        ));

       typeView.setText(type);
       typeView.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
       typeView.setGravity(Gravity.CENTER_HORIZONTAL);
       priceView.setGravity(Gravity.CENTER_HORIZONTAL);
       priceView.setText(price);
       priceView.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
       this.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
       this.setWeightSum(3);
       this.addView(typeView);
       this.addView(priceView);
       this.addView(button);
        TableLayout.LayoutParams lp = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                        TableLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0,10,40,10);
        this.setLayoutParams(lp);

    }
}
