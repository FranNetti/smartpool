package unibo.it.smartpool.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import unibo.it.smartpool.R;
import unibo.it.smartpool.database.entity.User;

/**
 * Class that manages an activity that requires a login before starting
 **/
public abstract class LoggedActivity extends AppCompatActivity {

    /* code for identifying the LoginActivity */
    private static final int LOG_REQ_CODE = 1;

    private boolean logged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!this.logged) {
            this.login();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOG_REQ_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    this.logged = true;
                    User user = (User)data.getSerializableExtra(LoginActivity.USER_DATA);
                    boolean newUser = data.getBooleanExtra(LoginActivity.USER_CREATE, false);
                    onLoginCompleted(user, newUser);
                    break;
                case Activity.RESULT_CANCELED:
                    finish();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                    .setTitle(R.string.exit_alert_title)
                    .setMessage(R.string.exit_alert_msg)
                    .setPositiveButton(R.string.exit_affermative, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.exit_negative, null)
                    .show();
    }

    /**
     * Abstract method to inform the subclasses that the login is completed
     * @param user the user that logged
     * @param newUser if the user it's using this app for the first time
     */
    public abstract void onLoginCompleted(final User user, final boolean newUser);

    private void login() {
        Intent intent = new Intent(this,LoginActivity.class);
        startActivityForResult(intent, LOG_REQ_CODE);
    }

}
