package unibo.it.smartpool.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import unibo.it.smartpool.R;

/**
 * Class for a fragment that shows an empty screen
 */
public class EmptyFragment extends Fragment {

    public interface EmptyFragmentListener {
        void refresh();
    }

    public static EmptyFragment newInstance() {
        return new EmptyFragment();
    }

    public EmptyFragment() {
    }

    private EmptyFragmentListener listener;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_empty, container, false);

        ImageButton btn = view.findViewById(R.id.btn_refresh);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.refresh();
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EmptyFragmentListener) {
            listener = (EmptyFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement EmptyFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}