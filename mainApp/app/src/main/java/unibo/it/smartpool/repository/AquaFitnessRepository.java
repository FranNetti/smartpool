package unibo.it.smartpool.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import unibo.it.smartpool.connection.ConnectionUtilities;
import unibo.it.smartpool.connection.HttpException;
import unibo.it.smartpool.database.entity.Subscription;
import unibo.it.smartpool.database.dao.AbbonamentoDAO;
import unibo.it.smartpool.database.AppDatabase;
import unibo.it.smartpool.database.entity.AquaFitness;
import unibo.it.smartpool.database.dao.AquaFitnessDAO;
import unibo.it.smartpool.database.dao.LessonDAO;
import unibo.it.smartpool.database.dao.RelationDAO;
import unibo.it.smartpool.database.relation_entity.RelationFitnLess;
import unibo.it.smartpool.database.relation_entity.RelationFitnSub;
import unibo.it.smartpool.database.entity.Lesson;

public class AquaFitnessRepository {

    private static final String TAG = "aquFitn_repo_tag";

    private AquaFitnessDAO aquaTable;
    private LessonDAO lessonTable;
    private AbbonamentoDAO subscriptionTable;
    private RelationDAO relationTable;

    private MutableLiveData<List<AquaFitness>> courseList = new MutableLiveData<>();

    public AquaFitnessRepository(final Context context){
        this.aquaTable = AppDatabase.getDatabase(context).aquaFitnessDAO();
        this.lessonTable = AppDatabase.getDatabase(context).lessonDAO();
        this.subscriptionTable = AppDatabase.getDatabase(context).abbonamentoDAO();
        this.relationTable = AppDatabase.getDatabase(context).relationDAO();
        new AquaFitnessCheckUpdate().execute();
    }

    public LiveData<List<AquaFitness>> getCourseList() {
        new AquaFitnessCheckUpdate().execute();
        return courseList;
    }

    private class AquaFitnessCheckUpdate extends AsyncTask<Void,Void, List<AquaFitness>> {

        @Override
        protected List<AquaFitness> doInBackground(Void... voids) {
            AquaFitness old = aquaTable.getLeastUpdatedCourse();
            if(old == null || old.hasToUpdate()){
                boolean subUpdate = false;
                boolean lessonUpdate = false;
                List<AquaFitness> oldFitnessList = aquaTable.getAllCourses();
                List<Subscription> oldSubscriptions = subscriptionTable.getAll();
                List<Lesson> oldLessons = lessonTable.getAll();

                final String url = ConnectionUtilities.SERVER_HTTP_URL + ConnectionUtilities.AQUAFITNESS_PAGE;
                try{
                    String data = ConnectionUtilities.getDataFromUrl(url,null,false);
                    if(data != null && !data.isEmpty()){
                        List<RelationFitnSub> acqFitnAbb = new ArrayList<>();
                        List<RelationFitnLess> acqFitnLess = new ArrayList<>();
                        List<AquaFitness> fitnessList = new ArrayList<>();
                        List<Subscription> subscriptionList = new ArrayList<>();
                        List<Lesson> lessonList = new ArrayList<>();
                        JSONArray array = new JSONArray(data);
                        for(int i = 0;i < array.length();i++){
                            JSONObject obj = array.getJSONObject(i);

                            /* update/create aquafitness obj */
                            AquaFitness fitness = new AquaFitness(obj);
                            if(oldFitnessList.contains(fitness)){
                                fitness = oldFitnessList.get(oldFitnessList.indexOf(fitness));
                                fitness.update(fitness);
                                relationTable.deleteAllLessonsOfAquaFitness(fitness.getId());
                                relationTable.deleteAllSubscriptionsOfAquaFitness(fitness.getId());
                            } else {
                                fitnessList.add(fitness);
                            }

                            /* update/create subscriptions */
                            JSONArray subscriptions = obj.getJSONArray("fare");
                            List<Subscription> subList = new ArrayList<>();
                            for (int x = 0; x < subscriptions.length(); x++){
                                Subscription abb = new Subscription(subscriptions.getJSONObject(x));
                                if(oldSubscriptions.contains(abb)){
                                    subUpdate |= oldSubscriptions.get(oldSubscriptions.indexOf(abb)).update(abb);
                                } else if(!subscriptionList.contains(abb)){
                                    subscriptionList.add(abb);
                                }
                                acqFitnAbb.add(new RelationFitnSub(fitness.getId(), abb.getId()));
                                subList.add(abb);
                            }
                            fitness.setSubscriptions(subList);

                            /* create lessons */
                            JSONArray lessons = obj.getJSONArray("lezioni");
                            List<Lesson> lessList = new ArrayList<>();
                            for (int x = 0; x < lessons.length(); x++){
                                Lesson less = new Lesson(lessons.getJSONObject(x));
                                if(oldLessons.contains(less)){
                                    lessonUpdate |= oldLessons.get(oldLessons.indexOf(less)).update(less);
                                } else if(!lessonList.contains(less)){
                                    lessonList.add(less);
                                }
                                acqFitnLess.add(new RelationFitnLess(fitness.getId(), less));
                                lessList.add(less);
                            }
                            fitness.setLessons(lessList);

                        }

                        aquaTable.updateCourses(oldFitnessList);
                        aquaTable.addAquaFitnessCourses(fitnessList);

                        if(subUpdate){
                            subscriptionTable.updateSubscriptions(oldSubscriptions);
                        }
                        subscriptionTable.addSubscriptions(subscriptionList);
                        relationTable.addFitnSubRelations(acqFitnAbb);

                        if(lessonUpdate){
                            lessonTable.updateLessons(oldLessons);
                        }
                        lessonTable.addLessons(lessonList);
                        relationTable.addFitnLessRelations(acqFitnLess);

                        oldFitnessList.addAll(fitnessList);
                        return oldFitnessList;
                    }
                } catch(HttpException e){
                    Log.e(TAG,"HttpException in AquaFitnessCheckUpdate", e);
                } catch(JSONException e){
                    Log.e(TAG,"JSONException in AquaFitnessCheckUpdate", e);
                }

            } else {
                List<AquaFitness> fitnessList = aquaTable.getAllCourses();
                for(AquaFitness aqua : fitnessList) {
                    aqua.setLessons(relationTable.getAllLessonsOfAquaFitness(aqua.getId()));
                    aqua.setSubscriptions(relationTable.getAllSubscriptionsOfAquaFitness(aqua.getId()));
                }
                return fitnessList;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<AquaFitness> course) {
            if(course != null){
                courseList.setValue(course);
            }
        }

    }
}
