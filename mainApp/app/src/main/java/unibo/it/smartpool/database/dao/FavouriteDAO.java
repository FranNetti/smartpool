package unibo.it.smartpool.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import unibo.it.smartpool.database.entity.Favourite;
import unibo.it.smartpool.database.entity.Instructor;

@Dao
public interface FavouriteDAO {

    @Query("SELECT instr.* FROM favourite as fav, instructor as instr WHERE instr.cf LIKE fav.instructorFavourite AND fav.user LIKE :user")
    LiveData<List<Instructor>> getAllFavouritesOfUser(final String user);

    @Delete
    void delete(final Favourite favourite);

    @Insert
    void add(final Favourite favourite);

    @Query("SELECT DISTINCT COUNT(instructorFavourite) FROM favourite WHERE user LIKE :userEmail AND instructorFavourite LIKE :instructorReference")
    int isAFavourite(final String userEmail, final String instructorReference);
}
