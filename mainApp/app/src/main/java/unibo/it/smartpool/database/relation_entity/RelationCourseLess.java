package unibo.it.smartpool.database.relation_entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import unibo.it.smartpool.database.entity.Lesson;
import unibo.it.smartpool.database.entity.SwimCourse;

@Entity(
        primaryKeys = {"courseRef", "day", "time", "pool"},
        foreignKeys = {
                @ForeignKey(
                        entity = SwimCourse.class,
                        parentColumns = "id",
                        childColumns = "courseRef",
                        onDelete = ForeignKey.CASCADE
                ),
                @ForeignKey(
                        entity = Lesson.class,
                        parentColumns = {"day", "time", "pool"},
                        childColumns = {"day", "time", "pool"},
                        onDelete = ForeignKey.CASCADE
                )
        },
        indices = @Index(
                value = {"day", "time", "pool"},
                name = "course_less_index"
        )
)
public class RelationCourseLess {

    @ColumnInfo
    private int courseRef;
    @ColumnInfo
    private int day;
    @ColumnInfo
    @NonNull
    private String time;
    @ColumnInfo
    @NonNull
    private String pool;

    public RelationCourseLess(final int courseRef, final int day, final String time, final String pool){
        this.day = day;
        this.courseRef = courseRef;
        this.time = time;
        this.pool = pool;
    }

    public RelationCourseLess(final int fitnessRef, final Lesson lesson){
        this(fitnessRef, lesson.getDay(), lesson.getTime(), lesson.getPool());
    }

    public int getCourseRef() {
        return courseRef;
    }

    public void setCourseRef(int courseRef) {
        this.courseRef = courseRef;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @NonNull
    public String getTime() {
        return time;
    }

    public void setTime(@NonNull String time) {
        this.time = time;
    }

    @NonNull
    public String getPool() {
        return pool;
    }

    public void setPool(@NonNull String pool) {
        this.pool = pool;
    }
}
