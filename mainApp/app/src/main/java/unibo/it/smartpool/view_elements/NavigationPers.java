package unibo.it.smartpool.view_elements;

import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;

import java.util.Stack;

import unibo.it.smartpool.utils.MenuState;

/**
 * Class that handles the state management of the app
 */
public class NavigationPers implements NavigationView.OnNavigationItemSelectedListener {

    public interface NavigationPersListener {
        void onStateChanged(MenuState state);
    }

    private final NavigationView navigationView;
    private final DrawerLayout drawer;
    private final NavigationPersListener listener;
    private final Stack<MenuState> menuStateList;
    private final MenuState startState;

    public NavigationPers(final NavigationView nav, final DrawerLayout drawer, final NavigationPersListener list, final MenuState startState) {
        this.navigationView = nav;
        this.drawer = drawer;
        this.listener = list;
        this.menuStateList = new Stack<>();
        this.startState = startState;
        this.menuStateList.add(startState);
        this.changeMenuElementState(startState, true);
        this.navigationView.setNavigationItemSelectedListener(this);
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        return this.navigationItemSelectedById(id);
    }

    public void onBackPressed() {
        if(this.canReturnBack()) {
            MenuState checkedState = this.menuStateList.pop();
            MenuState currentState = this.menuStateList.pop();
            this.changeMenuElementState(currentState, true);
            this.changeMenuElementState(checkedState, false);
            this.navigationItemSelectedById(currentState.getItemId());
        }
    }

    public boolean canReturnBack() {
        return this.menuStateList.size() > 1;
    }

    public MenuState getCurrentMenuState() {
        return this.menuStateList.peek();
    }

    private boolean navigationItemSelectedById(final int id) {
        this.drawer.closeDrawer(GravityCompat.START);

        if(this.listener != null) {
            if(this.menuStateList.empty()) {
                this.listener.onStateChanged(this.startState);
                this.menuStateList.push(this.startState);
            } else {
                MenuState oldMenuState = this.menuStateList.peek();
                if (oldMenuState.getItemId() != id) {
                    MenuState newMenuState = this.startState;
                    for (MenuState elem : MenuState.values()) {
                        if (elem.getItemId() == id) {
                            newMenuState = elem;
                        }
                    }
                    this.changeMenuElementState(newMenuState, true);
                    this.changeMenuElementState(oldMenuState, false);
                    if(this.menuStateList.contains(newMenuState)) {
                        this.menuStateList.remove(newMenuState);
                    }
                    this.menuStateList.push(newMenuState);
                    this.listener.onStateChanged(newMenuState);
                }
            }
        }
        return true;
    }

    private void changeMenuElementState(MenuState menu, boolean checked) {
        this.navigationView.getMenu()
                .findItem(menu.getItemId())
                .setChecked(checked);
    }

}
