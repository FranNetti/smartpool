package unibo.it.smartpool.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import unibo.it.smartpool.database.entity.SwimCourse;

@Dao
public interface SwimCourseDAO {

    @Query("SELECT * FROM swimcourse WHERE lastUpdate <= (SELECT MIN(lastUpdate) FROM swimcourse) LIMIT 1")
    SwimCourse getLeastUpdatedSwimCourse();

    @Query("SELECT * FROM swimcourse")
    List<SwimCourse> getAllSwimCourses();

    @Insert
    void addSwimCourses(final List<SwimCourse> courses);

    @Update
    void updateSwimCourses(final List<SwimCourse> courses);

}
