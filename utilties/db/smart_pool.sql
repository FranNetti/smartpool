-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Ott 01, 2018 alle 00:24
-- Versione del server: 10.1.31-MariaDB
-- Versione PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smart_pool`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `abbonamento`
--

CREATE TABLE `abbonamento` (
  `id` int(11) NOT NULL,
  `mensile` int(11) DEFAULT NULL,
  `ingresso` int(11) DEFAULT NULL,
  `prezzo` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `abbonamento`
--

INSERT INTO `abbonamento` (`id`, `mensile`, `ingresso`, `prezzo`) VALUES
(1, NULL, 1, 10),
(2, NULL, 5, 45),
(3, NULL, 10, 80),
(4, 1, NULL, 100),
(5, 5, NULL, 300);

-- --------------------------------------------------------

--
-- Struttura della tabella `abbonamento_utente`
--

CREATE TABLE `abbonamento_utente` (
  `id` int(11) NOT NULL,
  `codAbbonamento` int(11) NOT NULL,
  `codUtente` varchar(16) NOT NULL,
  `dataInizio` date DEFAULT NULL,
  `dataFine` date DEFAULT NULL,
  `numIngressiRim` int(11) DEFAULT NULL,
  `nuotoLibero` tinyint(1) NOT NULL,
  `codCorsoNuoto` int(11) DEFAULT NULL,
  `codCorsoAtt` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `abbonamento_utente`
--

INSERT INTO `abbonamento_utente` (`id`, `codAbbonamento`, `codUtente`, `dataInizio`, `dataFine`, `numIngressiRim`, `nuotoLibero`, `codCorsoNuoto`, `codCorsoAtt`) VALUES
(1, 5, 'GRNFNC96D27I872P', '2018-05-01', '2018-10-01', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `abb_acqua_fitn`
--

CREATE TABLE `abb_acqua_fitn` (
  `codAbbonamento` int(11) NOT NULL,
  `codCorsoAcqFitn` int(11) NOT NULL,
  `codCorsoAtt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `corso_acqua_fitness`
--

CREATE TABLE `corso_acqua_fitness` (
  `id` int(11) NOT NULL,
  `nome` varchar(20) DEFAULT NULL,
  `descrizione` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `corso_acqua_fitness`
--

INSERT INTO `corso_acqua_fitness` (`id`, `nome`, `descrizione`) VALUES
(1, 'Acqua Fitness', 'Ginnastica in acqua '),
(2, 'AcquaBike', 'Ginnastica con bici'),
(3, 'Zumba', 'Attività motoria supportata dalla musica in acqua');

-- --------------------------------------------------------

--
-- Struttura della tabella `corso_att_acq_fitn`
--

CREATE TABLE `corso_att_acq_fitn` (
  `id` int(11) NOT NULL,
  `codCorsoAcqFitn` int(11) NOT NULL,
  `istruttore` varchar(16) NOT NULL,
  `dataInizio` date NOT NULL,
  `dataFine` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `corso_att_acq_fitn`
--

INSERT INTO `corso_att_acq_fitn` (`id`, `codCorsoAcqFitn`, `istruttore`, `dataInizio`, `dataFine`) VALUES
(1, 2, '123456789', '2018-09-01', '2019-02-28'),
(2, 1, 'GLTMTT96T05C265J', '2018-09-01', '2019-02-28'),
(4, 1, 'GLTMTT96T05C265J', '2019-03-01', '2019-07-31');

-- --------------------------------------------------------

--
-- Struttura della tabella `corso_att_nuoto`
--

CREATE TABLE `corso_att_nuoto` (
  `id` int(11) NOT NULL,
  `codCorsoNuoto` int(11) NOT NULL,
  `istruttore` varchar(16) NOT NULL,
  `dataInizio` date NOT NULL,
  `dataFine` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `corso_att_nuoto`
--

INSERT INTO `corso_att_nuoto` (`id`, `codCorsoNuoto`, `istruttore`, `dataInizio`, `dataFine`) VALUES
(1, 1, '987654321', '2018-09-20', '2018-11-20'),
(2, 2, '357159258', '2018-09-20', '2018-11-20'),
(3, 3, 'GLTMTT96T05C265J', '2018-09-20', '2018-11-20');

-- --------------------------------------------------------

--
-- Struttura della tabella `corso_nuoto`
--

CREATE TABLE `corso_nuoto` (
  `id` int(11) NOT NULL,
  `livello` int(11) NOT NULL,
  `vasca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `corso_nuoto`
--

INSERT INTO `corso_nuoto` (`id`, `livello`, `vasca`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `istruttore`
--

CREATE TABLE `istruttore` (
  `cf` varchar(16) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `cognome` varchar(20) NOT NULL,
  `sesso` varchar(1) DEFAULT NULL,
  `indirizzo` varchar(50) NOT NULL,
  `numTelefono` varchar(10) NOT NULL,
  `idPiscina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `istruttore`
--

INSERT INTO `istruttore` (`cf`, `nome`, `cognome`, `sesso`, `indirizzo`, `numTelefono`, `idPiscina`) VALUES
('123456789', 'Elena', 'Morelli', 'F', 'Via Zolino 4/e', '3492447002', 1),
('357159258', 'Francesco', 'Monte', 'M', 'Viale Marconi, 20', '3356214208', 3),
('987654321', 'Francesca', 'Castelli', 'F', 'Via Villa 3', '3480032639', 0),
('GLTMTT96T05C265J', 'Matteo', 'Galeotti', 'M', 'Viale D\'Agostino 12', '3664363974', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `lezione_acq_fitn`
--

CREATE TABLE `lezione_acq_fitn` (
  `giorno` int(11) NOT NULL,
  `orario` time NOT NULL,
  `vasca` int(11) NOT NULL,
  `codCorsoatt` int(11) NOT NULL,
  `codCorsoAcqFitn` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `lezione_acq_fitn`
--

INSERT INTO `lezione_acq_fitn` (`giorno`, `orario`, `vasca`, `codCorsoatt`, `codCorsoAcqFitn`) VALUES
(1, '19:00:00', 3, 2, 1),
(2, '18:00:00', 2, 1, 2),
(4, '09:00:00', 3, 2, 1),
(6, '17:00:00', 2, 1, 2),
(7, '08:00:00', 2, 2, 1),
(7, '10:00:00', 2, 1, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `lezione_nuoto`
--

CREATE TABLE `lezione_nuoto` (
  `giorno` int(11) NOT NULL,
  `orario` time NOT NULL,
  `codCorsoNuoto` int(11) NOT NULL,
  `codCorsoAtt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `lezione_nuoto`
--

INSERT INTO `lezione_nuoto` (`giorno`, `orario`, `codCorsoNuoto`, `codCorsoAtt`) VALUES
(3, '18:00:00', 1, 1),
(3, '18:00:00', 2, 2),
(4, '18:30:00', 3, 3),
(5, '18:00:00', 2, 2),
(5, '19:00:00', 1, 1),
(5, '19:00:00', 3, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `tessera`
--

CREATE TABLE `tessera` (
  `numero` varchar(15) NOT NULL,
  `scadenza` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `tessera`
--

INSERT INTO `tessera` (`numero`, `scadenza`) VALUES
('0000758122', '2018-12-31'),
('0000758123', '2018-12-31');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `cf` varchar(16) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `cognome` varchar(20) NOT NULL,
  `sesso` char(1) NOT NULL,
  `dataNascita` date NOT NULL,
  `indirizzo` varchar(255) NOT NULL,
  `numTelefono` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pwd` varchar(20) NOT NULL,
  `credito` int(11) NOT NULL,
  `qrCode` varchar(50) DEFAULT NULL,
  `tessera` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`cf`, `nome`, `cognome`, `sesso`, `dataNascita`, `indirizzo`, `numTelefono`, `email`, `pwd`, `credito`, `qrCode`, `tessera`) VALUES
('GRNFNC96D27I872P', 'Francesco', 'Grandinetti', 'M', '1996-04-27', 'via Zaccherini 25', '3474864891', 'francesco.grandinett@gmail.com', 'iosonoio', 100, NULL, '0000758122'),
('MRLLNE96C59A944I', 'Elena', 'Morelli', 'F', '1996-03-19', 'via Tal dei Tali', '3492447002', 'mora19396@gmail.com', 'jappoegatti', 150, NULL, '0000758123');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `abbonamento`
--
ALTER TABLE `abbonamento`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `abbonamento_utente`
--
ALTER TABLE `abbonamento_utente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codUtente` (`codUtente`),
  ADD KEY `codAbbonamento` (`codAbbonamento`),
  ADD KEY `codCorsoAtt` (`codCorsoAtt`,`codCorsoNuoto`),
  ADD KEY `abbonamento_utente_ibfk_3` (`codCorsoNuoto`,`codCorsoAtt`);

--
-- Indici per le tabelle `abb_acqua_fitn`
--
ALTER TABLE `abb_acqua_fitn`
  ADD PRIMARY KEY (`codAbbonamento`,`codCorsoAcqFitn`,`codCorsoAtt`),
  ADD KEY `codCorsoAcqFitn` (`codCorsoAcqFitn`,`codCorsoAtt`);

--
-- Indici per le tabelle `corso_acqua_fitness`
--
ALTER TABLE `corso_acqua_fitness`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `corso_att_acq_fitn`
--
ALTER TABLE `corso_att_acq_fitn`
  ADD PRIMARY KEY (`id`,`codCorsoAcqFitn`),
  ADD KEY `istruttore` (`istruttore`),
  ADD KEY `idCorsoAcqFitn` (`codCorsoAcqFitn`);

--
-- Indici per le tabelle `corso_att_nuoto`
--
ALTER TABLE `corso_att_nuoto`
  ADD PRIMARY KEY (`id`,`codCorsoNuoto`),
  ADD KEY `istruttore` (`istruttore`),
  ADD KEY `codCorsoNuoto` (`codCorsoNuoto`);

--
-- Indici per le tabelle `corso_nuoto`
--
ALTER TABLE `corso_nuoto`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `istruttore`
--
ALTER TABLE `istruttore`
  ADD PRIMARY KEY (`cf`),
  ADD UNIQUE KEY `idPiscina` (`idPiscina`);

--
-- Indici per le tabelle `lezione_acq_fitn`
--
ALTER TABLE `lezione_acq_fitn`
  ADD PRIMARY KEY (`giorno`,`orario`,`codCorsoatt`,`codCorsoAcqFitn`),
  ADD KEY `codCorsoAcqFitn` (`codCorsoAcqFitn`,`codCorsoatt`);

--
-- Indici per le tabelle `lezione_nuoto`
--
ALTER TABLE `lezione_nuoto`
  ADD PRIMARY KEY (`giorno`,`orario`,`codCorsoNuoto`,`codCorsoAtt`),
  ADD KEY `codCorsoAtt` (`codCorsoAtt`,`codCorsoNuoto`);

--
-- Indici per le tabelle `tessera`
--
ALTER TABLE `tessera`
  ADD PRIMARY KEY (`numero`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`cf`),
  ADD KEY `tessera` (`tessera`);

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `abbonamento_utente`
--
ALTER TABLE `abbonamento_utente`
  ADD CONSTRAINT `abbonamento_utente_ibfk_1` FOREIGN KEY (`codUtente`) REFERENCES `utente` (`cf`),
  ADD CONSTRAINT `abbonamento_utente_ibfk_2` FOREIGN KEY (`codAbbonamento`) REFERENCES `abbonamento` (`id`),
  ADD CONSTRAINT `abbonamento_utente_ibfk_3` FOREIGN KEY (`codCorsoNuoto`,`codCorsoAtt`) REFERENCES `corso_att_nuoto` (`codCorsoNuoto`, `id`);

--
-- Limiti per la tabella `abb_acqua_fitn`
--
ALTER TABLE `abb_acqua_fitn`
  ADD CONSTRAINT `abb_acqua_fitn_ibfk_1` FOREIGN KEY (`codAbbonamento`) REFERENCES `abbonamento_utente` (`id`),
  ADD CONSTRAINT `abb_acqua_fitn_ibfk_2` FOREIGN KEY (`codCorsoAcqFitn`,`codCorsoAtt`) REFERENCES `corso_att_acq_fitn` (`codCorsoAcqFitn`, `id`);

--
-- Limiti per la tabella `corso_att_acq_fitn`
--
ALTER TABLE `corso_att_acq_fitn`
  ADD CONSTRAINT `corso_att_acq_fitn_ibfk_1` FOREIGN KEY (`istruttore`) REFERENCES `istruttore` (`cf`),
  ADD CONSTRAINT `corso_att_acq_fitn_ibfk_2` FOREIGN KEY (`codCorsoAcqFitn`) REFERENCES `corso_acqua_fitness` (`id`);

--
-- Limiti per la tabella `corso_att_nuoto`
--
ALTER TABLE `corso_att_nuoto`
  ADD CONSTRAINT `corso_att_nuoto_ibfk_1` FOREIGN KEY (`istruttore`) REFERENCES `istruttore` (`cf`),
  ADD CONSTRAINT `corso_att_nuoto_ibfk_2` FOREIGN KEY (`codCorsoNuoto`) REFERENCES `corso_nuoto` (`id`);

--
-- Limiti per la tabella `lezione_acq_fitn`
--
ALTER TABLE `lezione_acq_fitn`
  ADD CONSTRAINT `lezione_acq_fitn_ibfk_1` FOREIGN KEY (`codCorsoAcqFitn`,`codCorsoatt`) REFERENCES `corso_att_acq_fitn` (`codCorsoAcqFitn`, `id`);

--
-- Limiti per la tabella `lezione_nuoto`
--
ALTER TABLE `lezione_nuoto`
  ADD CONSTRAINT `lezione_nuoto_ibfk_1` FOREIGN KEY (`codCorsoAtt`,`codCorsoNuoto`) REFERENCES `corso_att_nuoto` (`id`, `codCorsoNuoto`);

--
-- Limiti per la tabella `utente`
--
ALTER TABLE `utente`
  ADD CONSTRAINT `utente_ibfk_1` FOREIGN KEY (`tessera`) REFERENCES `tessera` (`numero`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
