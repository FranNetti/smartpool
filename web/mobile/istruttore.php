<?php

  include 'utils/db_connect.php';
  $istruttore= array();

  class Corso
  {
    public $idCorso;
    public $nomeCorso;
    function __construct($id,$nomeCorso)
    {
      $this->idCorso = $id;
      $this->nomeCorso = $nomeCorso;
    }
  }


  class Istruttore {
    public $cf;
    public $nome;
    public $cognome;
    public $indirizzo;
    public $numTelefono;
    public $sesso;
    public $corsi;

    function __construct($cf, $nome,$cognome,$indirizzo,$numTelefono, $sesso, $corsi) {
      $this->cf = $cf;
      $this->nome = $nome;
      $this->cognome = $cognome;
      $this->indirizzo = $indirizzo;
      $this->numTelefono = $numTelefono;
      $this->sesso = $sesso;
      $this->corsi = $corsi;
    }
  }

 $conn = connectToDatabase();
 $query_sql="SELECT DISTINCT i.cf as cf,i.nome as nome,i.cognome as cognome,i.indirizzo as indirizzo, i.numTelefono as numTelefono, i.sesso as sesso, c.id as idCorso,  c.nome as nomeCorso
              FROM istruttore i, corso_att_acq_fitn corsAtt,corso_acqua_fitness c
              WHERE i.cf = corsAtt.istruttore
              AND corsAtt.codCorsoAcqFitn = c.id
              and corsAtt.dataFine >= CURDATE()
              and corsAtt.dataInizio <= CURDATE()";
    $result = $conn->query($query_sql);
       if ($result->num_rows > 0) {
         $result = $conn->query($query_sql);
           if ($result->num_rows > 0) {
             while ($row = $result->fetch_assoc()) {
               $corso = new Corso($row["idCorso"],$row["nomeCorso"]);
               if (!array_key_exists($row["cf"],$istruttore)){
                 $myIstruttore =  new Istruttore($row["cf"],$row["nome"],$row["cognome"],$row["indirizzo"],$row["numTelefono"],$row["sesso"],array());
                 $istruttore[$row["cf"]]=$myIstruttore;
               }
                 array_push($istruttore[$row["cf"]]->corsi,$corso);
               }
              }
            }

  $query_sql1="SELECT i.cf as cf,i.nome as nome,i.cognome as cognome,i.indirizzo as indirizzo, i.numTelefono as numTelefono, i.sesso as sesso, c.id as idCorso, c.livello as nomeCorso
              FROM istruttore i, corso_att_nuoto corsAtt,corso_nuoto c
              WHERE i.cf = corsAtt.istruttore
              AND corsAtt.codCorsoNuoto = c.id
              and corsAtt.dataFine >= CURDATE()
              and corsAtt.dataInizio <= CURDATE()";
    $result = $conn->query($query_sql1);
       if ($result->num_rows > 0) {
         $result = $conn->query($query_sql1);
           if ($result->num_rows > 0) {
             while ($row = $result->fetch_assoc()) {
               $corso = new Corso($row["idCorso"],"Corso di nuoto livello " . $row["nomeCorso"]);
               if (!array_key_exists($row["cf"],$istruttore)){
                 $myIstruttore =  new Istruttore($row["cf"],$row["nome"],$row["cognome"],$row["indirizzo"],$row["numTelefono"],$row["sesso"],array());
                 $istruttore[$row["cf"]]=$myIstruttore;
               }
                 array_push($istruttore[$row["cf"]]->corsi,$corso);
            }
          }
      }
   $conn->close();
   $array = array();
   $i=0;
   foreach ($istruttore as $key => $value) {
     $array[$i++]=$istruttore[$key];
   }
   echo json_encode($array);
?>
