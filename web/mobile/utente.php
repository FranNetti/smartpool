<?php

  include 'utils/db_connect.php';

  $utente = array("utente" => "");

  if(isset($_POST['user'], $_POST['pwd'])) {
     $user = $_POST['user'];
     $password = $_POST['pwd'];
     $mysqli = connectToDatabase();
     if ($res = $mysqli->prepare("SELECT * FROM utente WHERE email = ? LIMIT 1")) {
       $res->bind_param('s', $user);
       $res->execute();
       $result = $res->get_result();
       if($result->num_rows == 1) {
         $row = $result->fetch_assoc();
         if($row["pwd"] === $password) {
           $array = array();
           $array["cf"] = $row["cf"];
           $array["nome"] = $row["nome"];
           $array["cognome"] = $row["cognome"];
           $array["indirizzo"] = $row["indirizzo"];
           $array["numTelefono"] = $row["numTelefono"];
           $array["sesso"] = $row["sesso"];
           $array["dataNascita"] = $row["dataNascita"];
           $utente["utente"] = $array;
         } else {
           http_response_code(400); //bad request
           die();
         }
       } else {
         http_response_code(404); //not found
         die();
       }
       $res->close();
     }
     $mysqli->close();
     echo json_encode($utente);
  } else {

?>

<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <title>Utente</title>
  </head>
  <body>
    <form action="utente.php" method="post">
      <fieldset>
        <legend>Credenziali</legend>
        <label for="email">Email: </label>
        <input type="email" id="email" name="user" required>
        <label for="pwd">Password: </label>
        <input type="password" name="pwd" id="pwd" required>
      </fieldset>
      <input type="submit" value="Login">
      <input type="reset" value="Resetta campi">
    </form>
  </body>
</html>

<?php
}
 ?>
