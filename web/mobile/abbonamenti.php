<?php

  $SINGLE_ENTRY_SUB = 1;
  $MONTHLY_SUB = 2;


  include 'utils/db_connect.php';
  $utente = array();
  date_default_timezone_set('Europe/Rome');
  $currentDate = date('Y-m-d', time());

  if(isset($_POST['user'])) {
     $user = $_POST['user'];
     $mysqli = connectToDatabase();
     $sql = "SELECT abb.id, abb.dataInizio, abb.dataFine, abb.numIngressiRim, abb.nuotoLibero, a.mensile, c.livello, ff.nome
             FROM (abbonamento_utente abb, utente u, abbonamento a)
             LEFT JOIN corso_nuoto AS c ON c.id = abb.codCorsoNuoto
             LEFT JOIN abb_acqua_fitn AS f ON f.codAbbonamento = abb.id
             LEFT JOIN corso_acqua_fitness AS ff ON ff.id = f.codCorsoAcqFitn
             WHERE u.email = ?
             AND u.cf = abb.codUtente
             AND abb.codAbbonamento = a.id
             GROUP BY abb.id";
     if ($res = $mysqli->prepare($sql)) {
       $res->bind_param('s', $user);
       $res->execute();
       $result = $res->get_result();
       if($result->num_rows >= 1) {
         while($row = $result->fetch_assoc()){
           if($currentDate < $row["dataFine"] || ($row["numIngressiRim"] != null && $row["numIngressiRim"] >= 0)) {
             $array = array();
             $array["id"] = $row["id"];
             $nuotoLibero = $row["nuotoLibero"] == 1;
             if($row["mensile"]){
               $array["tipoAbbonamento"] = $MONTHLY_SUB;
             } else {
               $array["tipoAbbonamento"] = $SINGLE_ENTRY_SUB;
             }
             $array["scadenza"] = $row["dataFine"];
             $array["dataInizio"] = $row["dataInizio"];
             $array["numIngressi"] = $row["numIngressiRim"];
             if($row["livello"]){
               $array["corso"] = "Corso di nuoto livello " . $row["livello"];
             } else if($row["nome"]){
               $array["corso"] = "Corso di " . $row["nome"];
             } else if($nuotoLibero){
               $array["corso"] = "Nuoto libero";
             } else {
               $array["corso"] = null;
             }
             array_push($utente, $array);
             if($row["numIngressiRim"] == 0 && $array["tipoAbbonamento"] == $SINGLE_ENTRY_SUB){
               $sql = "UPDATE abbonamento_utente
                       SET numIngressiRim = -1
                       WHERE id = " . $row["id"];
               $mysqli->query($sql);
             }
           }
         }
       }
       $res->close();
     } else if($mysqli->error){
       echo $mysqli->error . "<br/>";
     }
     $mysqli->close();
     echo json_encode($utente);
  } else {

?>

<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <title>Utente</title>
  </head>
  <body>
    <form action="abbonamenti.php" method="post">
      <fieldset>
        <legend>Credenziali</legend>
        <label for="email">Email: </label>
        <input type="email" id="email" name="user" required>
      </fieldset>
      <input type="submit" value="Login">
      <input type="reset" value="Resetta campi">
    </form>
  </body>
</html>

<?php
}
 ?>
