<?php

  include 'utils/db_connect.php';

  $risposta = array();

  if(isset($_POST['email'])) {
    $email = $_POST["email"];
    $mysqli = connectToDatabase();
    if ($res = $mysqli->prepare("SELECT u.email, u.credito, t.* FROM tessera t, utente u WHERE u.email = ? AND t.numero = u.tessera LIMIT 1")) {
      $res->bind_param('s', $email);
      $res->execute();
      $result = $res->get_result();
      if($result->num_rows == 1) {
        $row = $result->fetch_assoc();
        $risposta["numero"] = $row["numero"];
        $risposta["scadenza"] = $row["scadenza"];
        $risposta["credito"] = $row["credito"];
      } else {
        http_response_code(404); //not found
        die();
      }
      $res->close();
    }
    $mysqli->close();
    echo json_encode($risposta);
  } else {
    http_response_code(400);
    die();
  }
?>
