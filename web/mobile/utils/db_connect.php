<?php

  /**
   * Effettua la connessione al database; ritorna l'oggetto mysqli con cui interagire.
   **/
  function connectToDatabase() {
    $host = "localhost"; // E' il server a cui ti vuoi connettere.
    $user = "root"; // E' l'utente con cui ti collegherai al DB.
    $pwd = ""; // Password di accesso al DB.
    $database = "smart_pool"; // Nome del database.
    return new mysqli($host, $user, $pwd, $database);
  }

  function getId($mysqli, $email){
    if ($res = $mysqli->prepare("SELECT cf FROM utente WHERE email = ?")) {
      $res->bind_param('s', $email); // esegue il bind del parametro '$email'.
      $res->execute(); // esegue la query appena creata.
      $res->store_result();
      $res->bind_result($cf); // recupera il risultato della query e lo memorizza nelle relative variabili.
      $res->fetch();
      if(is_null($cf)){
        return false;
      }
      return $cf;
    } else {
      return false;
    }
  }

  function getMaxId($mysqli){
    if($res = $mysqli->prepare("SELECT MAX(Id) FROM abbonamento_utente")){
      $res->execute(); // Esegue la query creata.
      $res->store_result();
      $res->bind_result($id); // recupera le variabili dal risultato ottenuto.
      $res->fetch();
      return $id;
    }else {
      return false;
    }
  }

  function getAccountBalance($mysqli,$email){
    if ($res = $mysqli->prepare("SELECT credito FROM utente WHERE email = ?")) {
      $res->bind_param('s', $email); // esegue il bind del parametro '$email'.
      $res->execute(); // esegue la query appena creata.
      $res->store_result();
      $res->bind_result($credito); // recupera il risultato della query e lo memorizza nelle relative variabili.
      $res->fetch();
      return $credito;
    } else {
      return false;
    }
  }

  function updateAccountBalance($mysqli,$cf,$newBalance){
      $sql = "UPDATE utente SET credito=".$newBalance." WHERE cf='".$cf."'";
      if($mysqli->query($sql) === TRUE){
      return true;
      }else{
      return false;
    }
  }

  function hasAlreadyEntrance($mysqli,$id,$idAbb,$cf,$value){

    $sql = "SELECT FROM abbonamento_utente abbUser,abb_acqua_fitn AbbAcq WHERE abbUser.id = AbbAcq.codAbbonamento";
    if($res = $mysqli->prepare("SELECT MAX(Id) FROM abbonamento_utente WHERE")){
      $res->execute(); // Esegue la query creata.
      $res->store_result();
      $res->bind_result($id); // recupera le variabili dal risultato ottenuto.
      $res->fetch();
      return $id;
    }else {
      return false;
    }
  }
 ?>
