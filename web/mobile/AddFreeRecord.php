<?php
include 'utils/db_connect.php';
class Data {
  public $id;
  public $dataInizio;
  public $dataFine;

  function __construct($id, $dataInizio, $dataFine) {
    $this->id = $id;
    $this->dataInizio = $dataInizio;
    $this->dataFine = $dataFine;
  }
}

if (isset($_POST["codAbbonamento"], $_POST["email"], $_POST["label"]
,$_POST["value"],$_POST["price"])){

    $idAbb = $_POST["codAbbonamento"];
    $email = $_POST["email"];
    $label = $_POST["label"];
    $value = $_POST["value"];
    $price = $_POST["price"];
    $mysqli = connectToDatabase();

    $credito = getAccountBalance($mysqli,$email);
    if($credito == false) {
      http_response_code(501);
      die();
    }
    $newBalance = $credito-$price;
    if(($newBalance) < 0){
      http_response_code(400);
      die();
    }else{
    $cf = getId($mysqli,$email);
    if($cf !== false) {
      $update = updateAccountBalance($mysqli,$cf,$newBalance);
    if($update === false){
      http_response_code(502);
      die();
    }
      $id = getMaxId($mysqli);
      if($id !== false){
        $id ++;
        if(strpos($label, 'ingr')!== false){
          $sql = "SELECT numIngressiRim, id
                  FROM abbonamento_utente
                  WHERE codUtente = ?
                  AND dataFine IS NULL
                  AND nuotoLibero = 1";
          if ($res = $mysqli->prepare($sql)) {
            $res->bind_param('s', $cf);
            $res->execute();
            $result = $res->get_result();
            if($result->num_rows >= 1) {
              $row = $result->fetch_assoc();
              $ingressi = $row["numIngressiRim"];
              $idUpdate = $row["id"];
              $ingressi += $value;
              $sql = "UPDATE abbonamento_utente SET numIngressiRim = $ingressi WHERE id = $idUpdate";
              if (!$mysqli->query($sql)){
                http_response_code(500);
                die();
              }
            }else {
              $sql = "INSERT INTO abbonamento_utente (id, codAbbonamento, dataFine, codUtente, numIngressiRim, nuotoLibero, codCorsoNuoto, codCorsoAtt)
                      VALUES ('$id','$idAbb',NULL,'$cf','$value','1',NULL,NULL)";
                if (!$mysqli->query($sql) === TRUE) {
                  http_response_code(500);
                  die();
                }
            }
          }
        }else{
        $sql = "SELECT id, dataFine
                FROM abbonamento_utente
                WHERE codUtente = ?
                AND codCorsoNuoto IS NULL
                AND codCorsoAtt  IS NULL
                AND nuotoLibero = 1
                AND numIngressiRim IS NULL
                and CURDATE() BETWEEN dataInizio AND dataFine";
        if ($res = $mysqli->prepare($sql)) {
          $res->bind_param('s', $cf);
          $res->execute();
          $result = $res->get_result();
          if($result->num_rows >= 1) {
            $row = $result->fetch_assoc();
            $dataScadenza = $row["dataFine"];
            $idUpdate = $row["id"];
            $dataScadenza = date("Y-m-d", strtotime($dataScadenza."+". $value."month"));
            $sql = "UPDATE abbonamento_utente SET dataFine = '$dataScadenza' WHERE id = '$idUpdate'";
            if (!$mysqli->query($sql)){
              http_response_code(500);
              die();
            }
              $dataScadenza = date_create($dataScadenza);
          }else{
          $corsi = array();
          $data = date("Y/m/d");
          $date = date("Y-m-d", strtotime($data."+". $value."month"));
          $dataScadenza = date_create($date);
          $dataString = $dataScadenza->format('Y-m-d');
          $dataStringToday = date("Y-m-d");
          $sql = "INSERT INTO abbonamento_utente (id, codAbbonamento, codUtente, dataInizio, dataFine, numIngressiRim, nuotoLibero, codCorsoNuoto, codCorsoAtt)
                  VALUES ('$id','$idAbb','$cf','$dataStringToday','$dataString',NULL,'1',NULL,NULL)";
                  if (!$mysqli->query($sql) === TRUE) {
                    http_response_code(500);
                    die();
                }
            }
          } else {
            $mysqli->close();
            http_response_code(507);
            die();
        }
      }
    }else{
    $mysqli->close();
    http_response_code(500);
    die();
    }
    }
  }
}else{
?>
<html>
  <form action="AddFreeRecord.php" method="post" class="row">
    <input type="text" name="codAbbonamento" value="3">
    <input type="text" name="email" value="mora19396@gmail.com">
    <input type="text" name="value" value="1">
    <input type="text" name="label" value="mensile">
    <input type="text" name="price" value="10">
    <input type="submit" name="submit">
  </form>
</html>
<?php
http_response_code(401);
die();
} ?>
