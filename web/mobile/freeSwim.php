<?php
include 'utils/db_connect.php';
$swimCourse = array();
/**
 *
 */
class Nuoto
{
  public $nome;
  public $fare;

  function __construct($nome,$abbonamento)
  {
    $this->nome = $nome;
    $this->fare = $abbonamento;

  }
}

class Abbonamento
{
  public $id;
  public $tipo;
  public $prezzo;
  public $label;

  function __construct($id, $ingresso,$mensile,$prezzo)
  {
    $this->id = $id;
    if($ingresso != 0){
      $this->tipo = $ingresso;
      if($ingresso == "1"){
        $this->label = "ingresso";
      }else{
        $this->label = "ingressi";
    }
    }else{
      $this->tipo = $mensile;
      if($mensile == "1"){
        $this->label = "mese";
      }else{
        $this->label = "mesi";
      }
    }
    $this->prezzo = $prezzo;
  }
}

$conn = connectToDatabase();
$query_sql="SELECT id, ingresso, mensile, prezzo
             FROM abbonamento";

   $result = $conn->query($query_sql);
      if ($result->num_rows > 0) {
        $result = $conn->query($query_sql);
          if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $abb = new Abbonamento($row["id"], $row["ingresso"],$row["mensile"],$row["prezzo"]);
                $abbonamento[]=$abb;
              }
             }
           }

  $swimCourse = new Nuoto("Nuoto Libero",$abbonamento);
  $conn->close();
  echo json_encode($swimCourse);
?>
