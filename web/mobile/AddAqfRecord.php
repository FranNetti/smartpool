<?php
include 'utils/db_connect.php';
  class Data {
    public $id;
    public $dataInizio;
    public $dataFine;

    function __construct($id, $dataInizio, $dataFine) {
      $this->id = $id;
      $this->dataInizio = $dataInizio;
      $this->dataFine = $dataFine;
    }
  }

  if (isset($_POST["codAbbonamento"], $_POST["email"], $_POST["label"],$_POST["value"],
  $_POST["corsoId"],$_POST["tipoId"],$_POST["price"])){

    $idAbb = $_POST["codAbbonamento"];
    $email = $_POST["email"];
    $idCorso = $_POST["corsoId"];
    $label = $_POST["label"];
    $value = $_POST["value"];
    $idTipo = $_POST["tipoId"];
    $price = $_POST["price"];
    $mysqli = connectToDatabase();

    $credito = getAccountBalance($mysqli,$email);
    if($credito === false) {
      http_response_code(501);
      die();
    }
    $newBalance = $credito-$price;
    if(($newBalance) < 0){
      http_response_code(400);
      die();
    } else {
      $cf = getId($mysqli,$email);
      if($cf !== false) {
          $update = updateAccountBalance($mysqli,$cf,$newBalance);
          if($update === false){
            http_response_code(502);
            die();
          }
          $id = getMaxId($mysqli);
          if($id !== false){
          $id ++;
          if(strpos($label, 'ingr')!== false){
            $sql = "SELECT u.numIngressiRim, u.id
                    FROM abbonamento_utente u, abb_acqua_fitn f
                    WHERE u.id = f.codAbbonamento
                    AND u.codUtente = ?
                    AND f.codCorsoAcqFitn = ?
                    AND f.codCorsoAtt = ? ";
            if ($res = $mysqli->prepare($sql)) {
              $res->bind_param('sii', $cf, $idTipo, $idCorso);
              $res->execute();
              $result = $res->get_result();
              if($result->num_rows >= 1) {
                $row = $result->fetch_assoc();
                $ingressi = $row["numIngressiRim"];
                $idUpdate = $row["id"];
                $ingressi += $value;
                $sql = "UPDATE abbonamento_utente SET numIngressiRim = $ingressi WHERE id = $idUpdate";
                if (!$mysqli->query($sql)){
                  http_response_code(500);
                  die();
                }
              } else {
                $sql = "INSERT INTO abbonamento_utente (id, codAbbonamento, dataFine, codUtente, numIngressiRim, nuotoLibero, codCorsoNuoto, codCorsoAtt)
                        VALUES ('$id','$idAbb',NULL,'$cf','$value','0',NULL,NULL)";
                if (!$mysqli->query($sql) === TRUE) {
                  http_response_code(503);
                  die();
                }
                $sql1 = "INSERT INTO abb_acqua_fitn(codAbbonamento, codCorsoAcqFitn, codCorsoAtt)
                          VALUES ('$id','$idTipo','$idCorso')";
                if (!$mysqli->query($sql1) === TRUE) {
                  http_response_code(504);
                  die();
                }
              }
            }
          }else{

            $corsi = array();
            $data = date("Y/m/d");
            $arrayCorsi = array();
            $x=0;

            $res = "SELECT id, dataFine, dataInizio FROM corso_att_acq_fitn
                    WHERE dataFine >= CURDATE()
                    AND codCorsoAcqFitn ='$idTipo'
                    ORDER BY  dataInizio asc";

            $result = $mysqli->query($res);
            if ($result->num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                $dt = new Data($row["id"],$row["dataInizio"],$row["dataFine"]);
                $corsi[] = $dt;
              }
            }
            $currentCourse = $corsi[$x];

            $sql = "SELECT u.id, u.dataFine
                    FROM abbonamento_utente u, abb_acqua_fitn f
                    WHERE u.id = f.codAbbonamento
                    AND u.codUtente = ?
                    AND f.codCorsoAcqFitn = ?
                    AND f.codCorsoAtt = ?
                    AND numIngressiRim IS NULL";
            if ($res = $mysqli->prepare($sql)) {
              $res->bind_param('sii', $cf, $idTipo, $idCorso);
              $res->execute();
              $result = $res->get_result();
              if($result->num_rows >= 1) {
                $row = $result->fetch_assoc();
                $dataScadenza = $row["dataFine"];
                $idUpdate = $row["id"];
                $dataScadenza = date("Y-m-d", strtotime($dataScadenza."+". $value."month"));
                $sql = "UPDATE abbonamento_utente SET dataFine = '$dataScadenza' WHERE id = '$idUpdate'";
                if (!$mysqli->query($sql)){
                  http_response_code(500);
                  die();
                }
                $dataScadenza = date_create($dataScadenza);
              } else {
                $date = date("Y-m-d", strtotime($data."+". $value."month"));
                $dataScadenza = date_create($date);
                array_push($arrayCorsi,$currentCourse->id);
                $dataString = $dataScadenza->format('Y-m-d');
                $dataStringToday = date("Y-m-d");
                $sql = "INSERT INTO abbonamento_utente (id, codAbbonamento, codUtente, dataInizio, dataFine, numIngressiRim, nuotoLibero, codCorsoNuoto, codCorsoAtt)
                        VALUES ('$id','$idAbb','$cf','$dataStringToday','$dataString',NULL,'0',NULL,NULL)";
                if (!$mysqli->query($sql) === TRUE) {
                  http_response_code(505);
                  die();
                }
              }

              $dataFine = date_create($currentCourse->dataFine);
              $interval = $dataScadenza->diff($dataFine);
              $diff = $interval->format('%R%a');

              while($diff < 0){
                $dataFine = $corsi[++$x]->dataFine;
                $dataFine = date_create($dataFine);
                array_push($arrayCorsi,$corsi[$x]->id);
                $interval = $dataScadenza->diff($dataFine);
                $diff = $interval->format('%R%a');
              }
              /*$dataString = $dataScadenza->format('Y-m-d');
              $sql = "INSERT INTO abbonamento_utente (id, codAbbonamento, codUtente, dataFine, numIngressiRim, nuotoLibero, codCorsoNuoto, codCorsoAtt)
                      VALUES ('$id','$idAbb','$cf','$dataString',NULL,'0',NULL,NULL)";
              if (!$mysqli->query($sql) === TRUE) {
                http_response_code(505);
                die();
              }*/
              for ($i=0; $i < count($arrayCorsi); $i++) {
                $var = $arrayCorsi[$i];
                $sql1 = "INSERT INTO abb_acqua_fitn(codAbbonamento, codCorsoAcqFitn, codCorsoAtt)
                          VALUES ('$id','$idTipo','$var')";
                if (!$mysqli->query($sql1) === TRUE) {
                  http_response_code(506);
                  die();
                }
              }
            } else {
              $mysqli->close();
              http_response_code(507);
              die();
            }
          }
        } else {
          $mysqli->close();
          http_response_code(508);
          die();
        }
      } else {
        $mysqli->close();
        http_response_code(509);
        die();
      }
    }
  } else {
?>
<html>
  <form action="AddAqfRecord.php" method="post" class="row">
    <label for="codAbbonamento">Abbonamento: </label>
    <input type="text" name="codAbbonamento" value="3"> <br/>
    <label for="email">Email: </label>
    <input type="text" name="email" value="mora19396@gmail.com"> <br/>
    <label for="email">ingressi/mesi: </label>
    <input type="text" name="value" value="1"> <br/>
    <label for="email">corso attuale: </label>
    <input type="text" name="corsoId" value="2"> <br/>
    <label for="email">mesile/ingressi: </label>
    <input type="text" name="label" value="mensile"> <br/>
    <label for="email">corso: </label>
    <input type="text" name="tipoId" value="1"> <br/>
    <label for="email">prezzo: </label>
    <input type="text" name="price" value="10"> <br/>
    <input type="submit" name="submit">
  </form>
</html>
<?php
  http_response_code(401);
  die();
  } ?>
