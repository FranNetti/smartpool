<?php
include 'utils/db_connect.php';
$corso = array();

class Lezione
{
  public $giorno;
  public $ora;

  function __construct($giorno, $ora)
  {
    $this->giorno = $giorno;
    $this->ora = $ora;
  }
}

class Abbonamento
{
  public $id;
  public $tipo;
  public $prezzo;
  public $label;
  function __construct($id,$mensile,$prezzo)
  {
      $this->tipo = $mensile;
      if($mensile == "1"){
        $this->label = "mese";
      }else{
        $this->label = "mesi";
      }
    $this->prezzo = $prezzo;
    $this->id = $id;
  }
}

class NuotoCorso{
  public $id;
  public $nome;
  public $vasca;
  public $labelLivello;
  public $labelVasca;
  public $idCorsoAtt;
  public $fare;
  public $istruttori;
  public $lezioni;

  function __construct($id,$nome,$vasca,$idCorsoAtt,$array,$istruttori,$lezioni)
  {
    $this->id = $id;
    $this->nome = $nome;
    $this->vasca = $vasca;
    $this->labelLivello = "Livello";
    $this->labelVasca = "Vasca";
    $this->idCorsoAtt = $idCorsoAtt;
    $this->fare = $array;
    $this->istruttori = $istruttori;
    $this->lezioni = $lezioni;
  }

  function addIstruttore($istr){
    $count = count($this->istruttori);
    $ok = false;
    for ($i=0; $i < $count ; $i++) {
       $tmp = $this->istruttori[$i];
       $ok |= $tmp->nome === $istr->nome && $tmp->cognome === $istr->cognome;
    }
    if(!$ok){
      array_push($this->istruttori, $istr);
    }
  }
}

class Istruttore {
  public $nome;
  public $cognome;
  function __construct($nome, $cognome){
    $this->nome = $nome;
    $this->cognome =$cognome;
  }
}

$conn = connectToDatabase();
$query_sql="SELECT id,mensile, prezzo
             FROM abbonamento a
             where ingresso IS NULL";

        $result = $conn->query($query_sql);
          if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $abb = new Abbonamento($row["id"],$row["mensile"],$row["prezzo"]);
                $abbonamento[]=$abb;
              }
             }
    $query_sql1="SELECT c.id, c.livello as livello, c.vasca as vasca, corsAtt.id as idCorsoAtt, i.cf as cf, i.nome as nomeIst, i.cognome as cognomeIst, l.orario as ora, l.giorno as giorno
                 FROM corso_nuoto c, istruttore i, lezione_nuoto l, corso_att_nuoto corsAtt
                  WHERE i.cf = corsAtt.istruttore
                    AND corsAtt.codCorsoNuoto = c.id
                    and corsAtt.id = l.codCorsoatt
                    and l.codCorsoNuoto = c.id
                    and corsAtt.dataInizio <= CURDATE()
                    and corsAtt.dataFine >= CURDATE()";

    $result = $conn->query($query_sql1);
       if ($result->num_rows > 0) {
         $result = $conn->query($query_sql1);
           if ($result->num_rows > 0) {
             while ($row = $result->fetch_assoc()) {
               $lezione = new Lezione($row["giorno"], $row["ora"]);
               $istruttore = new Istruttore($row["nomeIst"],$row["cognomeIst"]);
                if (!array_key_exists($row["id"],$corso)){
                 $myCorso = new NuotoCorso($row["id"],$row["livello"],$row["vasca"],$row["idCorsoAtt"],$abbonamento, array(), array());
                 $corso[$row["id"]]=$myCorso;
                }
                $corso[$row["id"]]->addIstruttore($istruttore);
                array_push($corso[$row["id"]]->lezioni,$lezione);
               }
              }
            }
    $conn->close();
    $array = array();
    $i=0;
    foreach ($corso as $key => $value) {
      $array[$i++]=$corso[$key];
    }
  echo json_encode($array);
?>
