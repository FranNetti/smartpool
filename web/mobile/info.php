<?php

  $indirizzo = "via delle Piscine 15, Imola (BO)";
  $telefono = "0542-684258";
  $mail = "info@smartpool.it";
  $numVasche = 3;
  $vasche = array();
  array_push($vasche, "25x12.50, 6 corsie profondità 120-160 cm", "acquafitness profondità media 102 cm", "ludica profondità media 80 cm");
  $descrizione = "La nostra struttura è composta da " . $numVasche .
                  " vasche, ciascuna con profondità diversa in modo tale da accontentare clienti di tutte le età.\n" .
                  "Siamo aperti tutti i giorni con orario continuato più un servizio bar nelle serate dei weekend per " .
                  "intrattenervi con deliziosi aperitivi e musica.";
  $orari = array();
  $orari["lunedì"] = "08:00-20:00";
  $orari["martedì"] = "08:00-20:00";
  $orari["mercoledì"] = "08:00-20:00";
  $orari["giovedì"] = "08:00-20:00";
  $orari["venerdì"] = "08:00-20:00";
  $orari["sabato"] = "08:00-22:00";
  $orari["domenica"] = "08:00-22:00";

  $info = array();
  $info["descrizione"] = $descrizione;
  $info["orari"] = $orari;
  $info["indirizzo"] = $indirizzo;
  $info["numVasche"] = $numVasche;
  $info["numero"] = $telefono;
  $info["mail"] = $mail;
  $info["vasche"] = $vasche;

  echo json_encode($info);
 ?>
