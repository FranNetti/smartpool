<?php
include 'utils/db_connect.php';
$corso = array();

class Lezione
{
  public $giorno;
  public $ora;
  public $vasca;

  function __construct($giorno, $ora, $vasca)
  {
    $this->giorno = $giorno;
    $this->ora = $ora;
    $this->vasca = $vasca;
  }
}
class Abbonamento
{
  public $id;
  public $tipo;
  public $prezzo;
  public $label;

  function __construct($id, $ingresso, $mensile, $prezzo)
  {
    if($ingresso != 0){
      $this->tipo = $ingresso;
      if($ingresso == "1"){
        $this->label = "ingresso";
      }else{
        $this->label = "ingressi";
    }
    }else{
      $this->tipo = $mensile;
      if($mensile == "1"){
        $this->label = "mese";
      }else{
        $this->label = "mesi";
      }
    }
    $this->prezzo = $prezzo;
    $this->id = $id;
  }
}

class AcquaCorso{
  public $id;
  public $nome;
  public $desc;
  public $idCorsoAtt;
  public $fare;
  public $istruttori;
  public $lezioni;


  function __construct($id,$nome,$descrizione,$idCorsoAtt,$array,$istruttori,$lezioni)
  {
    $this->id = $id;
    $this->nome = $nome;
    $this->desc = $descrizione;
    $this->idCorsoAtt = $idCorsoAtt;
    $this->fare = $array;
    $this->istruttori = $istruttori;
    $this->lezioni = $lezioni;

  }

  function addIstruttore($istr){
    $count = count($this->istruttori);
    $ok = false;
    for ($i=0; $i < $count ; $i++) {
       $tmp = $this->istruttori[$i];
       $ok |= $tmp->nome === $istr->nome && $tmp->cognome === $istr->cognome;
    }
    if(!$ok){
      array_push($this->istruttori, $istr);
    }
  }
}

 class Istruttore {
   public $nome;
   public $cognome;
   function __construct($nome, $cognome){
     $this->nome = $nome;
     $this->cognome =$cognome;
   }
}

$conn = connectToDatabase();
$query_sql="SELECT id, ingresso, mensile, prezzo
             FROM abbonamento";

   $result = $conn->query($query_sql);
      if ($result->num_rows > 0) {
        $result = $conn->query($query_sql);
          if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $abb = new Abbonamento($row["id"],$row["ingresso"],$row["mensile"],$row["prezzo"]);
                $abbonamento[]=$abb;
              }
             }
           }
  $query_sql1=" SELECT DISTINCT c.id as id, c.nome as nome, c.descrizione as descrizione, corsAtt.id as idCorsoAttuale, i.cf as cf, i.nome as nomeIst, i.cognome as cognomeIst, l.orario as ora, l.giorno as giorno, l.vasca as vasca
                FROM istruttore i, lezione_acq_fitn l, corso_att_acq_fitn corsAtt, corso_acqua_fitness c
                WHERE i.cf = corsAtt.istruttore
                AND corsAtt.codCorsoAcqFitn = c.id
                and corsAtt.id = l.codCorsoatt
                and l.codCorsoAcqFitn = c.id
                and corsAtt.dataInizio <= CURDATE()
                and corsAtt.dataFine >= CURDATE()";


         $result = $conn->query($query_sql1);
           if ($result->num_rows > 0) {
             while ($row = $result->fetch_assoc()) {
               $lezione = new Lezione($row["giorno"], $row["ora"], $row["vasca"]);
               $istruttore = new Istruttore($row["nomeIst"],$row["cognomeIst"]);
               if (!array_key_exists($row["id"],$corso)){
                 $myCorso =  new AcquaCorso($row["id"],$row["nome"],$row["descrizione"],$row["idCorsoAttuale"],$abbonamento, array(), array());
                 $corso[$row["id"]]=$myCorso;
               }
               $corso[$row["id"]]->addIstruttore($istruttore);
                 array_push($corso[$row["id"]]->lezioni,$lezione);

               }
            }
    $conn->close();
    $array = array();
    $i=0;
    foreach ($corso as $key => $value) {
      $array[$i++]=$corso[$key];
    }
    echo json_encode($array);
 ?>
