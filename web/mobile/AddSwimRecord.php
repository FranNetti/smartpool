<?php
include 'utils/db_connect.php';
class Data {
  public $id;
  public $dataInizio;
  public $dataFine;

  function __construct($id, $dataInizio, $dataFine) {
    $this->id = $id;
    $this->dataInizio = $dataInizio;
    $this->dataFine = $dataFine;
  }
}

if (isset($_POST["codAbbonamento"], $_POST["email"],$_POST["value"],
  $_POST["corsoId"],$_POST["tipoId"],$_POST["price"])){
    $idAbb = $_POST["codAbbonamento"];
    $email = $_POST["email"];
    $idCorso = $_POST["corsoId"];
    $value = $_POST["value"];
    $idTipo = $_POST["tipoId"];
    $price = $_POST["price"];
    $mysqli = connectToDatabase();

    $credito = getAccountBalance($mysqli,$email);
    if($credito == false) {
      http_response_code(501);
      die();
    }
    $newBalance = $credito-$price;
    if(($newBalance) < 0){
      http_response_code(400);
      die();
    }else{
    $cf = getId($mysqli,$email);
    if($cf != false) {
      $update = updateAccountBalance($mysqli,$cf,$newBalance);
    if($update == false){
      http_response_code(502);
      die();
    }
      $id = getMaxId($mysqli);
      if($id != false) {

        $corsi = array();
        $data = date("Y/m/d");
        $arrayCorsi = array();
        $x=0;

        $res = "SELECT id, dataFine, dataInizio FROM corso_att_nuoto
        WHERE dataFine >= CURDATE()
        AND codCorsoNuoto ='$idTipo'
        ORDER BY  dataInizio asc";

         $result = $mysqli->query($res);
           if ($result->num_rows > 0) {
             while ($row = $result->fetch_assoc()) {
                $dt = new Data($row["id"],$row["dataInizio"],$row["dataFine"]);
                $corsi[] = $dt;
             }
        }
        $currentCourse = $corsi[$x];
        $sql = "SELECT id, dataFine
                FROM abbonamento_utente
                WHERE codUtente = ?
                AND codCorsoNuoto= ?
                AND codCorsoAtt = ?
                AND numIngressiRim IS NULL";
        if ($res = $mysqli->prepare($sql)) {
          $res->bind_param('sii', $cf, $idTipo, $idCorso);
          $res->execute();
          $result = $res->get_result();
          if($result->num_rows >= 1) {
            $row = $result->fetch_assoc();
            $dataScadenza = $row["dataFine"];
            $idUpdate = $row["id"];
            $dataScadenza = date("Y-m-d", strtotime($dataScadenza."+". $value."month"));
            $sql = "UPDATE abbonamento_utente SET dataFine = '$dataScadenza' WHERE id = '$idUpdate'";
            if (!$mysqli->query($sql)){
              http_response_code(505);
              die();
            }
            $dataScadenza = date_create($dataScadenza);
          } else {
            $currentCourse = $corsi[$x];
            $dataStringToday = date("Y-m-d");
            $dataFine = date_create($currentCourse->dataFine);
            $date = date("Y-m-d", strtotime($data."+". $value."month"));
            $dataScadenza = date_create($date);
            $interval = $dataScadenza->diff($dataFine);
            $diff = $interval->format('%R%a');
            array_push($arrayCorsi,$currentCourse->id);
            while($diff < 0){
              $dataFine = $corsi[++$x]->dataFine;
              $dataFine = date_create($dataFine);
              array_push($arrayCorsi,$corsi[$x]->id);
              $interval = $dataScadenza->diff($dataFine);
              $diff = $interval->format('%R%a');
            }
            $dataString = $dataScadenza->format('Y-m-d');
            print_r($arrayCorsi);
            for ($i=0; $i < count($arrayCorsi); $i++) {
              $id ++;
              $var = $arrayCorsi[$i];
              $sql = "INSERT INTO abbonamento_utente (id, codAbbonamento, codUtente, dataInizio, dataFine, numIngressiRim, nuotoLibero, codCorsoNuoto, codCorsoAtt)
                      VALUES ('$id','$idAbb','$cf','$dataStringToday','$dataString',NULL,'0','$idTipo','$var')";
                      if (!$mysqli->query($sql) === TRUE) {
                        http_response_code(506);
                        die();
              }
            }
          }
        }

      /*  for ($i=0; $i < count($arrayCorsi); $i++) {
          $id ++;
          $var = $arrayCorsi[$i];
          $sql = "INSERT INTO abbonamento_utente (id, codAbbonamento, codUtente, dataFine, numIngressiRim, nuotoLibero, codCorsoNuoto, codCorsoAtt)
                  VALUES ('$id','$idAbb','$cf','$dataString',NULL,'0','$idTipo','$var')";
                  if (!$mysqli->query($sql) === TRUE) {
                    http_response_code(503);
                    die();
                }
        }*/
    } else {
      $mysqli->close();
      http_response_code(507);
      die();
    }
  } else {
    $mysqli->close();
    http_response_code(508);
    die();
  }
}
}else{
?>
<html>
  <form action="AddSwimRecord.php" method="post" class="row">
    <input type="text" name="codAbbonamento" value="1">
    <input type="text" name="email" value="mora19396@gmail.com">
    <input type="text" name="value" value="1">
    <input type="text" name="corsoId" value="3">
    <input type="text" name="tipoId" value="3">
    <input type="text" name="price" value="10">
    <input type="submit" name="submit">
  </form>
</html>
<?php
http_response_code(401);
die();} ?>
