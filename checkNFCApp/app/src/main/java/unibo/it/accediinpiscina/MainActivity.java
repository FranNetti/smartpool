package unibo.it.accediinpiscina;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{

    private static final String TECHNOLOGY = "application/vnd.com.example.android.beam";

    private NfcAdapter nfcAdapter;
    private TextView textView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.textView = (TextView) findViewById(R.id.mail_utente);
        this.nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if(this.nfcAdapter == null) {
            Toast.makeText(this, R.string.nfc_absent , Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(this.nfcAdapter != null) {
            this.on();
            startNFCDispatch(this, this.nfcAdapter);
        }

    }

    @Override
    protected void onPause(){
        super.onPause();
        if(this.nfcAdapter != null) {
            stopNFCDispatch(this, this.nfcAdapter);
        }

    }

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        if (intent != null && NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if(rawMsgs != null) {
                NdefMessage msg = (NdefMessage) rawMsgs[0];
                String text = new String(msg.getRecords()[0].getPayload());
                if (!text.isEmpty()) {
                    this.textView.setText(text);
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                /**/
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            textView.setText("");
                        }
                    }.execute();
                }
            }
        }
    }

    private void startNFCDispatch(final Activity act, final NfcAdapter adapter){
        final Context ctx = act.getApplicationContext();
        final Intent i = new Intent(ctx, act.getClass());
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent pi = PendingIntent.getActivity(ctx, 0, i, 0);
        final IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndef.addDataType(TECHNOLOGY);
            ndef.addCategory(Intent.CATEGORY_DEFAULT);
        }
        catch (IntentFilter.MalformedMimeTypeException e) {
            /**/
        }
        final IntentFilter[] filters = new IntentFilter[] {ndef};
        final String[][] techList = new String[][]{};
        adapter.enableForegroundDispatch(act, pi, filters, techList);
    }

    private void stopNFCDispatch(final Activity a, final NfcAdapter adapter){
        adapter.disableForegroundDispatch(a);
    }

    private void on(){
        if (!this.nfcAdapter.isEnabled()) {
            startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
        }
    }
}
